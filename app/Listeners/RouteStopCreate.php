<?php

namespace App\Listeners;

use App\Events\RouteSaved;
use App\Http\Models\RouteStop;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class RouteStopCreate
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RouteSaved  $event
     * @return void
     */
    public function handle(RouteSaved $event)
    {
        $routeStopInput = $event->routeStop;

        $routeStop = new RouteStop;

        $routeStop->port_id = $routeStopInput['port'];
        $routeStop->eta = $routeStopInput['estimasi'];
        $routeStop->route_id = $event->routeId;

        $routeStop->save();
    }
}
