<?php

namespace App\Listeners;

use App\Events\RecipeSaved;
use App\Http\Models\IngredientRecipe;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class IngredientRecipeCreate
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RecipeSaved  $event
     * @return void
     */
    public function handle(RecipeSaved $event)
    {
        $ingredientRecipeInput = $event->ingredientRecipe;

        $ingredientRecipe = new IngredientRecipe;

        $ingredientRecipe->ingredient_id = $ingredientRecipeInput['bahan'];
        $ingredientRecipe->ingredient_recipe_qty = $ingredientRecipeInput['jumlah'];
        $ingredientRecipe->uom_id = $ingredientRecipeInput['satuan'];
        $ingredientRecipe->recipe_id = $event->recipeId;

        $ingredientRecipe->save();
    }
}
