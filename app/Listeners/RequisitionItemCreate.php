<?php

namespace App\Listeners;

use App\Events\RequisitionSaved;
use App\Http\Models\RequisitionItem;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class RequisitionItemCreate
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RequisitionSaved  $event
     * @return void
     */
    public function handle(RequisitionSaved $event)
    {
        $requisitionItemInput = $event->requisitionItem;

        $requisitionItem = new RequisitionItem;

        $requisitionItem->ingredient_id = $requisitionItemInput['ingredient'];
        $requisitionItem->quantity = $requisitionItemInput['quantity'];
        $requisitionItem->price = $requisitionItemInput['price'];
        $requisitionItem->uom_id = $requisitionItemInput['uom'];
        $requisitionItem->requisition_id = $event->requisitionId;

        $requisitionItem->save();
    }
}
