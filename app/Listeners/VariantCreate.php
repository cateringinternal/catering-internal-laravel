<?php

namespace App\Listeners;

use App\Events\IngredientSaved;
use App\Http\Models\Variant;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class VariantCreate
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  IngredientSaved  $event
     * @return void
     */
    public function handle(IngredientSaved $event)
    {
        $variant_input = $event->variant;

        $variants = new Variant;

        $variants->variation = $variant_input['variantName'];
        $variants->description = $variant_input['description'];
        $variants->ingredient_id = $event->ingredientId;

        $variants->save();
    }
}
