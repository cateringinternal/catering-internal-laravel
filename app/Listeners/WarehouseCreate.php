<?php

namespace App\Listeners;

use App\Events\CruiseSaved;
use App\Http\Models\Warehouse;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class WarehouseCreate
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CruiseSaved  $event
     * @return void
     */
    public function handle(CruiseSaved $event)
    {
        $warehouse_input = $event->warehouse;

        $warehouse = new Warehouse;

        $warehouse->name = $warehouse_input['warehouseName'];
        $warehouse->type = $warehouse_input['warehouseType'];
        $warehouse->cruise_id = $event->cruiseId;

        $warehouse->save();
    }
}
