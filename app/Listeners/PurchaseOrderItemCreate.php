<?php

namespace App\Listeners;

use App\Events\PurchaseOrderSaved;
use App\Http\Models\PurchaseOrderItem;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class PurchaseOrderItemCreate
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PurchaseOrderSaved  $event
     * @return void
     */
    public function handle(PurchaseOrderSaved $event)
    {
        $purchaseOrderItemInput = $event->purchaseOrderItem;

        $purchaseOrderItem = new PurchaseOrderItem;

        $purchaseOrderItem->ingredient_id = $purchaseOrderItemInput['ingredient'];
        $purchaseOrderItem->quantity = $purchaseOrderItemInput['quantity'];
        $purchaseOrderItem->price = $purchaseOrderItemInput['price'];
        $purchaseOrderItem->uom_id = $purchaseOrderItemInput['uom'];
        $purchaseOrderItem->purchase_order_id = $event->purchaseOrderId;

        $purchaseOrderItem->save();
    }
}
