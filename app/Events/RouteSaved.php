<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class RouteSaved
{
    use InteractsWithSockets, SerializesModels;

    public $routeStop, $routeId;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($routeStop, $routeId)
    {
        $this->routeStop = $routeStop;
        $this->routeId = $routeId;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
