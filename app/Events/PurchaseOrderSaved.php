<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class PurchaseOrderSaved
{
    use InteractsWithSockets, SerializesModels;

    public $purchaseOrderItem, $purchaseOrderId;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($purchaseOrderItem, $purchaseOrderId)
    {
        $this->purchaseOrderItem = $purchaseOrderItem;
        $this->purchaseOrderId = $purchaseOrderId;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
