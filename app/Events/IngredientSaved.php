<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class IngredientSaved
{
    use InteractsWithSockets, SerializesModels;

    public $variant, $ingredientId;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($variants, $ingredientId)
    {
        $this->variant = $variants;
        $this->ingredientId = $ingredientId;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
