<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class RecipeSaved
{
    use InteractsWithSockets, SerializesModels;

    public $ingredientRecipe, $recipeId;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($ingredientRecipe, $recipeId)
    {
        $this->ingredientRecipe = $ingredientRecipe;
        $this->recipeId = $recipeId;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
