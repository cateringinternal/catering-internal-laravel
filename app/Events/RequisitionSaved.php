<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class RequisitionSaved
{
    use InteractsWithSockets, SerializesModels;

    public $requisitionItem, $requisitionId;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($requisitionItem, $requisitionId)
    {
        $this->requisitionItem = $requisitionItem;
        $this->requisitionId = $requisitionId;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
