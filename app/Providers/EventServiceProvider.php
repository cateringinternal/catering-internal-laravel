<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\IngredientSaved' => [
            'App\Listeners\VariantCreate',
        ],
        'App\Events\RecipeSaved' => [
            'App\Listeners\IngredientRecipeCreate',
        ],
        'App\Events\CruiseSaved' => [
            'App\Listeners\WarehouseCreate',
        ],
        'App\Events\RouteSaved' => [
            'App\Listeners\RouteStopCreate',
        ],
        'App\Events\RequisitionSaved' => [
            'App\Listeners\RequisitionItemCreate',
        ],
        'App\Events\PurchaseOrderSaved' => [
            'App\Listeners\PurchaseOrderItemCreate',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
