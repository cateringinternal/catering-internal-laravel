<?php

namespace App\Providers;

use App\Http\Models\Cruise;
use App\Http\Models\Ingredient;
use App\Http\Models\PurchaseOrder;
use App\Http\Models\Recipe;
use App\Http\Models\Requisition;
use App\Http\Models\Route;
use App\Http\Models\User;
use App\Observers\CruiseObserver;
use App\Observers\IngredientObserver;
use App\Observers\PurchaseOrderObserver;
use App\Observers\RecipeObserver;
use App\Observers\RequisitionObserver;
use App\Observers\RouteObserver;
use App\Observers\UserObserver;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //observe user
        User::observe(UserObserver::class);
        //observe ingredient
        Ingredient::observe(IngredientObserver::class);
        //observe recipe
        Recipe::observe(RecipeObserver::class);
        //observe cruise
        Cruise::observe(CruiseObserver::class);
        //observe route
        Route::observe(RouteObserver::class);
        //observe requisition
        Requisition::observe(RequisitionObserver::class);
        //observe purchase order
        PurchaseOrder::observe(PurchaseOrderObserver::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
