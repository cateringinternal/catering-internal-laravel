<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 16/10/16
 * Time: 14:19
 */

namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;

class WastePerMeal extends Model
{
    protected $table = 'waste_per_meal';
}