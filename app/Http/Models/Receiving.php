<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 09/10/16
 * Time: 8:57
 */

namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;

class Receiving extends Model
{
    protected $table = 'receiving';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function receiving_item()
    {
        return $this->hasMany('App\Http\Models\ReceivingItem');
    }
}