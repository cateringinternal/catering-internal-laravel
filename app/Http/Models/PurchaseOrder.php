<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 07/10/16
 * Time: 22:32
 */

namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;

class PurchaseOrder extends Model
{
    protected $table = 'purchase_order';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function purchase_order_item()
    {
        return $this->hasMany('App\Http\Models\PurchaseOrderItem');
    }
}