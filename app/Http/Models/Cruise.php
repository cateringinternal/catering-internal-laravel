<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 22/09/16
 * Time: 0:52
 */

namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;

class Cruise extends Model
{
    /**
     * table name
     *
     * @var string
     */
    protected $table = 'cruise';

    /**
     * cruise has many warehouses
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function warehouse()
    {
        return $this->hasMany('App\Http\Models\Warehouse');
    }
}