<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 21/10/16
 * Time: 6:44
 */

namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;

class Variant extends Model
{
    protected $table = 'variation';

    /**
     * variant belongs to ingredient
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ingredient()
    {
        return $this->belongsTo('App\Http\Models\Ingredient');
    }
}