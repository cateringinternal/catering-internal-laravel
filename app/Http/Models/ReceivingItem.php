<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 09/10/16
 * Time: 8:58
 */

namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;

class ReceivingItem extends Model
{
    protected $table = 'receiving_item';
}