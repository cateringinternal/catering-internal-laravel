<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 05/10/16
 * Time: 7:13
 */

namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;

class Requisition extends Model
{
    protected $table = 'requisition';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function vendor()
    {
        return $this->belongsTo('App\Http\Models\Vendor');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function voyage_planning()
    {
        return $this->belongsTo('App\Http\Models\VoyagePlanning');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function requisition_item()
    {
        return $this->hasMany('App\Http\Models\RequisitionItem');
    }
}