<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 16/10/16
 * Time: 21:17
 */

namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $table = 'employee';
}