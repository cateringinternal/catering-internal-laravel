<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 15/10/16
 * Time: 13:23
 */

namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;

class InventoryOut extends Model
{
    protected $table = 'inventory_out';
}