<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 24/10/16
 * Time: 5:24
 */

namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;

class RouteStop extends Model
{
    protected $table = 'route_stop';
}