<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 23/10/16
 * Time: 21:18
 */

namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;

class IngredientRecipe extends Model
{
    protected $table = 'ingredient_receipt';
}