<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 16/10/16
 * Time: 13:49
 */

namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;

class Waste extends Model
{
    protected $table = 'waste';
}