<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 13/10/16
 * Time: 6:11
 */

namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;

class Inventory extends Model
{
    protected $table = 'inventory';
}