<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 09/10/16
 * Time: 8:56
 */

namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $table = 'invoice';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function receiving()
    {
        return $this->belongsTo('App\Http\Models\Receiving');
    }
}