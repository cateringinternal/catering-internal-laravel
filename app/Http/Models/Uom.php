<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 20/10/16
 * Time: 7:09
 */

namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;

class Uom extends Model
{
    protected $table = 'uom';
}