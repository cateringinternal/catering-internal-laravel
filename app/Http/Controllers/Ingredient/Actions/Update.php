<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 22/09/16
 * Time: 12:39
 */

namespace App\Http\Controllers\Ingredient\Actions;


use App\Events\IngredientSaved;
use App\Http\Controllers\Ingredient\Requests\IngredientRequest;
use App\Http\Models\Ingredient;
use App\Http\Models\Variant;
use Illuminate\Contracts\Encryption\DecryptException;

trait Update
{
    /**
     * update ingredient data by ID
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getUpdate($id){

        try {

            $decrypted = decrypt($id);

            if ($ingredient = $this->ingredient->where('id', $decrypted)->first()) {

                $data = [
                    'ingredient' => $ingredient,
                    'categories' => $this->category,
                    'uoms' => $this->uom
                ];

                return view('ingredients.actions.update', $data);

            }

            return view('errors.404');

        } catch (DecryptException $e) {

            return $e;

        }

    }

    /**
     * post ingredient data request into database
     *
     * @param IngredientRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postUpdate(IngredientRequest $request, $id)
    {
        $ingredient_input = $request->except(['_token', 'submit', '_method']);

        Variant::where('ingredient_id', $id)->delete();

        $ingredient = Ingredient::find($id);

        $ingredient->name = $ingredient_input['name'];
        $ingredient->category_id = $ingredient_input['category_id'];
        $ingredient->description = $ingredient_input['description'];

        $ingredient->save();

        foreach ($ingredient_input['variant'] as $variant) {
            event(new IngredientSaved($variant, $ingredient->id));
        }

        return redirect('ingredient')->with('success', 'Data Updated!');
    }
}