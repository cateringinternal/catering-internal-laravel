<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 22/09/16
 * Time: 12:35
 */

namespace App\Http\Controllers\Ingredient\Actions;


use App\Http\Models\Ingredient;
use Illuminate\Contracts\Encryption\DecryptException;

trait Delete
{

    /**
     * delete ingredient data by ID
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getDelete($id){

        try {

            $decrypted = decrypt($id);

            if ($ingredient = Ingredient::find($decrypted)) {

                $ingredient->delete();

                return redirect('ingredient')->with('success', 'Data Deleted!');

            }

            return view('errors.404');

        } catch (DecryptException $e) {

            return $e;

        }

    }
}