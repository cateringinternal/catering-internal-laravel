<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 16/10/16
 * Time: 21:19
 */

namespace App\Http\Controllers\Employee;


use App\Http\Controllers\Controller;
use App\Http\Models\Employee;
use App\Http\Models\User;

class EmployeeController extends Controller
{
    /**
     * @var \Illuminate\Database\Eloquent\Collection|static[]
     */
    private $user, $employee;

    use Actions\Create, Actions\Delete, Actions\Update;

    /**
     * EmployeeController constructor.
     */
    public function __construct()
    {
        $this->employee = Employee::all();
        $this->user = User::all();
    }

    /**
     * show employee data
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex()
    {
        $data = [
            'employees' => $this->employee,
            'users' => $this->user
        ];

        return view('employees.index', $data);
    }
}