<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 17/10/16
 * Time: 7:04
 */

namespace App\Http\Controllers\Employee\Actions;


use App\Http\Controllers\Employee\Requests\EmployeeRequest;
use App\Http\Models\Employee;

trait Create
{
    /**
     * create employee data
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCreate()
    {
        $data = [
            'users' => $this->user,
        ];

        return view('employees.actions.create', $data);
    }

    /**
     * post employee data created into database
     *
     * @param EmployeeRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postCreate(EmployeeRequest $request)
    {
        $employees_input = $request->except(['_token', 'submit']);

        $employees = new Employee;

        $employees->name = $employees_input['name'];
        $employees->email = $employees_input['email'];
        $employees->phone = $employees_input['phone'];
        $employees->sijil_number = $employees_input['sijil_number'];
        $employees->nrp_number = $employees_input['nrp_number'];
        $employees->certificate = $employees_input['certificate'];
        $employees->bk_number_crew = $employees_input['bk_number_crew'];
        $employees->effective_date = $employees_input['effective_date'];
        $employees->user_id = $employees_input['user'];

        $employees->save();

        return redirect('employee')->with('success' , 'Data Recorded!');
    }
}