<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 17/10/16
 * Time: 7:21
 */

namespace App\Http\Controllers\Employee\Actions;


use App\Http\Models\Employee;
use Illuminate\Contracts\Encryption\DecryptException;

trait Delete
{
    /**
     * delete employee data by ID
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getDelete($id)
    {
        try {

            $decrypted = decrypt($id);

            if ($employee = Employee::find($decrypted)) {

                $employee->delete();

                return redirect('employee')->with('success' , 'Data Deleted!');

            }

            return view('errors.404');

        } catch (DecryptException $e) {

            return $e;

        }
    }
}