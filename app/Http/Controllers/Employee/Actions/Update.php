<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 17/10/16
 * Time: 7:23
 */

namespace App\Http\Controllers\Employee\Actions;


use App\Http\Controllers\Employee\Requests\EmployeeRequest;
use App\Http\Models\Employee;
use Illuminate\Contracts\Encryption\DecryptException;

trait Update
{
    /**
     * update employee data into database
     *
     * @param $id
     * @return bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getUpdate($id)
    {

        try {

            $decrypted = decrypt($id);

            if ($employee = $this->employee->where('id', $decrypted)->first()) {

                $data = [
                    'users' => $this->user,
                    'employee' => $employee
                ];

                return view('employees.actions.update', $data);

            }

            return view('errors.404');

        } catch (DecryptException $e) {

            return $e;

        }
    }

    /**
     * post employee data updated into database
     *
     * @param EmployeeRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postUpdate(EmployeeRequest $request, $id)
    {
        $employees_input = $request->except(['_token', 'submit', '_method']);

        $employees = Employee::find($id);

        $employees->name = $employees_input['name'];
        $employees->email = $employees_input['email'];
        $employees->phone = $employees_input['phone'];
        $employees->sijil_number = $employees_input['sijil_number'];
        $employees->nrp_number = $employees_input['nrp_number'];
        $employees->certificate = $employees_input['certificate'];
        $employees->bk_number_crew = $employees_input['bk_number_crew'];
        $employees->effective_date = $employees_input['effective_date'];
        $employees->user_id = $employees_input['user'];

        $employees->save();

        return redirect('employee')->with('success' , 'Data Updated!');
    }
}