<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 17/10/16
 * Time: 7:13
 */

namespace App\Http\Controllers\Employee\Requests;


use Illuminate\Foundation\Http\FormRequest;

class EmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required|numeric',
            'sijil_number' => 'required|numeric',
            'nrp_number' => 'required|numeric',
            'certificate' => 'required',
            'bk_number_crew' => 'required',
            'effective_date' => 'required',
            'user' => 'required',
        ];
    }
}