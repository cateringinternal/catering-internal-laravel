<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 20/10/16
 * Time: 7:10
 */

namespace App\Http\Controllers\Uom;


use App\Http\Controllers\Controller;
use App\Http\Models\Uom;

class UomController extends Controller
{
    /**
     * @var \Illuminate\Database\Eloquent\Collection|static[]
     */
    private $uom;

    use Actions\Create, Actions\Delete, Actions\Update;

    /**
     * UomController constructor.
     */
    public function __construct()
    {
        $this->uom = Uom::all();
    }

    /**
     * show uom data
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex()
    {
        $data = [
            'uoms' => $this->uom
        ];

        return view('uoms.index', $data);
    }
}