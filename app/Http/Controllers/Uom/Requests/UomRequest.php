<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 20/10/16
 * Time: 7:19
 */

namespace App\Http\Controllers\Uom\Requests;


use Illuminate\Foundation\Http\FormRequest;

class UomRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'name' => 'required',
            'type' => 'required',
            'multiplier' => 'required|numeric',
            'description' => 'required',
        ];
    }
}