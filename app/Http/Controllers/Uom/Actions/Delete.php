<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 20/10/16
 * Time: 7:25
 */

namespace App\Http\Controllers\Uom\Actions;


use App\Http\Models\Uom;
use Illuminate\Contracts\Encryption\DecryptException;

trait Delete
{
    /**
     * delete uom data by ID
     *
     * @param $id
     * @return bool|\Exception|DecryptException|\Illuminate\Http\RedirectResponse
     */
    public function getDelete($id)
    {
        try {

            $decrypted = decrypt($id);

            if ($uom = Uom::find($decrypted)) {

                $uom->delete();

                return redirect('uom')->with('success', 'Data Deleted');

            }

            return false;

        } catch (DecryptException $e) {

            return $e;

        }
    }
}