<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 20/10/16
 * Time: 7:15
 */

namespace App\Http\Controllers\Uom\Actions;


use App\Http\Controllers\Uom\Requests\UomRequest;
use App\Http\Models\Uom;

trait Create
{
    /**
     * create uom data
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCreate()
    {
        return view('uoms.actions.create');
    }

    /**
     * post uom data created into database
     *
     * @param UomRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postCreate(UomRequest $request)
    {
        $uom_input = $request->except(['_token', 'submit']);

        $uoms = new Uom;

        $uoms->uom = $uom_input['name'];
        $uoms->type = $uom_input['type'];
        $uoms->multiplier = $uom_input['multiplier'];
        $uoms->description = $uom_input['description'];

        $uoms->save();

        return redirect('uom')->with('success' , 'Data Recorded!');
    }
}