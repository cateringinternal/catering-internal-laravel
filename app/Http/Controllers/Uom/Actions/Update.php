<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 20/10/16
 * Time: 7:27
 */

namespace App\Http\Controllers\Uom\Actions;


use App\Http\Controllers\Uom\Requests\UomRequest;
use App\Http\Models\Uom;
use Illuminate\Contracts\Encryption\DecryptException;

trait Update
{
    /**
     * update uom data by ID
     *
     * @param $id
     * @return bool|\Exception|DecryptException|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getUpdate($id)
    {
        try {

            $decrypted = decrypt($id);

            if ($uom = Uom::find($decrypted)) {

                $data = [
                    'uom' => $uom
                ];

                return view('uoms.actions.update', $data);

            }

            return false;

        } catch (DecryptException $e) {

            return $e;

        }
    }

    /**
     * post uom data updated into database
     *
     * @param UomRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postUpdate(UomRequest $request, $id)
    {
        $uom_input = $request->except(['_token', 'submit']);

        $uoms = Uom::find($id);

        $uoms->uom = $uom_input['name'];
        $uoms->type = $uom_input['type'];
        $uoms->multiplier = $uom_input['multiplier'];
        $uoms->description = $uom_input['description'];

        $uoms->save();

        return redirect('uom')->with('success' , 'Data Updated!');
    }
}