<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 16/10/16
 * Time: 13:52
 */

namespace App\Http\Controllers\Waste;


use App\Http\Controllers\Controller;
use App\Http\Models\VoyagePlanning;
use App\Http\Models\Waste;

class WasteController extends Controller
{
    /**
     * @var \Illuminate\Database\Eloquent\Collection|static[]
     */
    private $waste, $voyage_planning;

    use Actions\Create, Actions\Delete, Actions\Update;

    /**
     * WasteController constructor.
     */
    public function __construct()
    {
        $this->waste = Waste::all();
        $this->voyage_planning = VoyagePlanning::all();
    }

    /**
     * show waste data
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex()
    {
        $data = [
            'wastes' => $this->waste,
            'voyage_plannings' => $this->voyage_planning
        ];

        return view('wastes.index', $data);
    }
}