<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 16/10/16
 * Time: 14:06
 */

namespace App\Http\Controllers\Waste\Requests;


use Illuminate\Foundation\Http\FormRequest;

class WasteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'date' => 'required',
            'voyage_planning' => 'required',
            'weight' => 'required|numeric',
            'waste_type' => 'required',
        ];
    }
}