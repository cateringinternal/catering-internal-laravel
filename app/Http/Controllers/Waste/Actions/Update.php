<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 16/10/16
 * Time: 14:15
 */

namespace App\Http\Controllers\Waste\Actions;


use App\Http\Controllers\Waste\Requests\WasteRequest;
use App\Http\Models\Waste;
use Illuminate\Contracts\Encryption\DecryptException;

trait Update
{
    /**
     * update waste data
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getUpdate($id)
    {
        try {

            $decrypted = decrypt($id);

            if ($waste = $this->waste->where('id', $decrypted)->first()) {

                $data = [
                    'waste' => $waste,
                    'voyage_plannings' => $this->voyage_planning
                ];

                return view('wastes.actions.update', $data);
            }

            return view('errors.404');

        } catch (DecryptException $e) {

            return $e;

        }
    }

    /**
     * post waste data updated into database
     *
     * @param WasteRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postUpdate(WasteRequest $request, $id)
    {
        $waste_input = $request->except(['_token', 'submit', '_method']);

        $wastes = Waste::find($id);

        $wastes->date = $waste_input['date'];
        $wastes->voyage_id = $waste_input['voyage_planning'];
        $wastes->weight = $waste_input['weight'];
        $wastes->waste_type = $waste_input['waste_type'];

        $wastes->save();

        return redirect('waste')->with('success' , 'Data Updated!');
    }

}