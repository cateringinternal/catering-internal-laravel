<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 16/10/16
 * Time: 14:12
 */

namespace App\Http\Controllers\Waste\Actions;


use App\Http\Models\Waste;
use Illuminate\Contracts\Encryption\DecryptException;

trait Delete
{
    /**
     * delete waste data
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getDelete($id)
    {
        try {

            $decrypted = decrypt($id);

            if ($waste = Waste::find($decrypted)) {

                $waste->delete();

                return redirect('waste')->with('success' , 'Data Deleted!');

            }

            return view('errors.404');

        } catch (DecryptException $e) {

            return $e;

        }
    }

}