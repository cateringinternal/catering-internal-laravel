<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 16/10/16
 * Time: 14:02
 */

namespace App\Http\Controllers\Waste\Actions;


use App\Http\Controllers\Waste\Requests\WasteRequest;
use App\Http\Models\Waste;

trait Create
{
    /**
     * create voyage planning data
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCreate()
    {
        $data = [
            'voyage_plannings' => $this->voyage_planning,
        ];

        return view('wastes.actions.create', $data);
    }

    /**
     * post waste data created into database
     *
     * @param WasteRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postCreate(WasteRequest $request)
    {
        $waste_input = $request->except(['_token', 'submit']);

        $wastes = new Waste;

        $wastes->date = $waste_input['date'];
        $wastes->voyage_id = $waste_input['voyage_planning'];
        $wastes->weight = $waste_input['weight'];
        $wastes->waste_type = $waste_input['waste_type'];

        $wastes->save();

        return redirect('waste')->with('success' , 'Data Recorded!');
    }
}