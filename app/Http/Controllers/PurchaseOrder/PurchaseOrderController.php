<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 07/10/16
 * Time: 22:35
 */

namespace App\Http\Controllers\PurchaseOrder;


use App\Http\Controllers\Controller;
use App\Http\Models\Ingredient;
use App\Http\Models\PurchaseOrder;
use App\Http\Models\PurchaseOrderItem;
use App\Http\Models\Requisition;
use App\Http\Models\RequisitionItem;
use App\Http\Models\Uom;
use App\Http\Models\Vendor;

class PurchaseOrderController extends Controller
{
    /**
     * @var \Illuminate\Database\Eloquent\Collection|static[]
     */
    private $purchase_order, $requisition, $vendor, $purchaseOrderItem, $requisitionItem,
    $ingredient, $uom;

    use Actions\Create, Actions\Delete, Actions\Update, Actions\Printer;

    /**
     * PurchaseOrderController constructor.
     */
    public function __construct()
    {
        $this->purchase_order = PurchaseOrder::all();
        $this->requisition = Requisition::all();
        $this->vendor = Vendor::all();
        $this->purchaseOrderItem = PurchaseOrderItem::all();
        $this->requisitionItem = RequisitionItem::all();
        $this->ingredient = Ingredient::all();
        $this->uom = Uom::all();
    }

    /**
     * show PO data
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex()
    {
        $data = [
            'purchase_orders' => $this->purchase_order,
            'requisitions' => $this->requisition,
            'vendors' => $this->vendor,
            'purchaseOrderItems' => $this->purchaseOrderItem,
            'requisitionItems' => $this->requisitionItem,
            'ingredients' => $this->ingredient,
            'uoms' => $this->uom
        ];

        return view('purchase-orders.index', $data);
    }
}