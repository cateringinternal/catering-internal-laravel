<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 07/10/16
 * Time: 22:54
 */

namespace App\Http\Controllers\PurchaseOrder\Requests;


use Illuminate\Foundation\Http\FormRequest;

class PurchaseOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'requisition' => 'required',
            'date' => 'required|date',
            'total_amount' => 'required|numeric',
            'vendor' => 'required',
            'payment_method' => 'required',
        ];
    }
}