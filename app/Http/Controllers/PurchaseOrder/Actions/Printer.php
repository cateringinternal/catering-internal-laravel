<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 08/10/16
 * Time: 16:47
 */

namespace App\Http\Controllers\PurchaseOrder\Actions;


use App\Http\Models\Ingredient;
use App\Http\Models\PurchaseOrder;
use App\Http\Models\PurchaseOrderItem;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\App;

trait Printer
{
    /**
     * print Purchase Order
     *
     * @param $id
     * @return mixed
     */
    public function getPrint($id)
    {
        try {

            $decrypted = decrypt($id);

            if ($purchase_order = $this->purchase_order->where('id', $decrypted)->first()) {

                $purchase_order_item = $purchase_order->purchase_order_item;

                $data = [
                    'purchase_order' => $purchase_order,
                    'purchase_order_items' => $purchase_order_item,
                    'ingredients' => $this->ingredient,
                    'uoms' => $this->uom
                ];

                $pdf = App::make('dompdf.wrapper');
                $pdf->loadView('purchase-orders.actions.print', $data);
                return $pdf->stream();;

            }

            return view('errors.404');

        } catch (DecryptException $e) {

            return $e;

        }
    }
}