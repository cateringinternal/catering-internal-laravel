<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 07/10/16
 * Time: 22:50
 */

namespace App\Http\Controllers\PurchaseOrder\Actions;


use App\Events\PurchaseOrderSaved;
use App\Http\Controllers\PurchaseOrder\Requests\PurchaseOrderRequest;
use App\Http\Models\PurchaseOrder;

trait Create
{
    /**
     * create PO
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCreate()
    {
        $data = [
            'requisitions' => $this->requisition,
            'vendors' => $this->vendor,
            'requisitionItems' => $this->requisitionItem,
            'ingredients' => $this->ingredient,
            'uoms' => $this->uom
        ];

        return view('purchase-orders.actions.create', $data);
    }

    /**
     * post purchase order data into database
     *
     * @param PurchaseOrderRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postCreate(PurchaseOrderRequest $request)
    {
        $purchase_order_input = $request->except(['_token', 'submit']);

        $purchase_order = new PurchaseOrder;

        $purchase_order->requisition_id = $purchase_order_input['requisition'];
        $purchase_order->date = $purchase_order_input['date'];
        $purchase_order->total_amount = $purchase_order_input['total_amount'];
        $purchase_order->vendor_id = $purchase_order_input['vendor'];
        $purchase_order->payment_method = $purchase_order_input['payment_method'];

        $purchase_order->save();

        foreach ($purchase_order_input['purchaseOrderItem'] as $purchaseOrderItem) {
            event(new PurchaseOrderSaved($purchaseOrderItem, $purchase_order->id));
        }

        $getLastPurchaseOrder = PurchaseOrder::find($purchase_order->id);
        $getLastPurchaseOrder->code = 'PU000' . '' . $purchase_order->id;
        $getLastPurchaseOrder->save();

        return redirect('purchase-order')->with('success' , 'Data Recorded!');
    }
}