<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 08/10/16
 * Time: 5:58
 */

namespace App\Http\Controllers\PurchaseOrder\Actions;


use App\Events\PurchaseOrderSaved;
use App\Http\Controllers\PurchaseOrder\Requests\PurchaseOrderRequest;
use App\Http\Models\PurchaseOrder;
use App\Http\Models\PurchaseOrderItem;
use Illuminate\Contracts\Encryption\DecryptException;

trait Update
{
    /**
     * update purchase order data
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getUpdate($id)
    {
        try {

            $decrypted = decrypt($id);

            if ($purchase_order = $this->purchase_order->where('id', $decrypted)->first()) {

                $data = [
                    'purchase_order' => $purchase_order,
                    'requisitions' => $this->requisition,
                    'vendors' => $this->vendor,
                    'requisitionItems' => $this->requisitionItem,
                    'ingredients' => $this->ingredient,
                    'uoms' => $this->uom
                ];

                return view('purchase-orders.actions.update', $data);

            }

            return view('errors.404');

        } catch (DecryptException $e) {

            return $e;

        }
    }

    /**
     * post purchase order data into database
     *
     * @param PurchaseOrderRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postUpdate(PurchaseOrderRequest $request, $id)
    {
        $purchase_order_input = $request->except(['_token', 'submit', '_method']);

        PurchaseOrderItem::where('purchase_order_id', $id)->delete();

        $purchase_order = PurchaseOrder::find($id);

        $purchase_order->requisition_id = $purchase_order_input['requisition'];
        $purchase_order->date = $purchase_order_input['date'];
        $purchase_order->total_amount = $purchase_order_input['total_amount'];
        $purchase_order->vendor_id = $purchase_order_input['vendor'];
        $purchase_order->payment_method = $purchase_order_input['payment_method'];

        $purchase_order->save();

        foreach ($purchase_order_input['purchaseOrderItem'] as $purchaseOrderItem) {
            event(new PurchaseOrderSaved($purchaseOrderItem, $purchase_order->id));
        }

        return redirect('purchase-order')->with('success' , 'Data Updated!');
    }
}