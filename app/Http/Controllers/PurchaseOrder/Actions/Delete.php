<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 08/10/16
 * Time: 5:55
 */

namespace App\Http\Controllers\PurchaseOrder\Actions;


use App\Http\Models\PurchaseOrder;
use Illuminate\Contracts\Encryption\DecryptException;

trait Delete
{
    /**
     * delete purchase order data
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getDelete($id)
    {
        try {

            $decrypted = decrypt($id);

            if ($purchase_order = PurchaseOrder::find($decrypted)) {

                $purchase_order->delete();

                return redirect('purchase-order')->with('success' , 'Data Deleted!');

            }

            return view('errors.404');

        } catch (DecryptException $e) {

            return $e;

        }
    }
}