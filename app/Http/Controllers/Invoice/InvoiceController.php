<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 09/10/16
 * Time: 9:04
 */

namespace App\Http\Controllers\Invoice;


use App\Http\Controllers\Controller;
use App\Http\Models\Ingredient;
use App\Http\Models\Invoice;
use App\Http\Models\Receiving;
use App\Http\Models\ReceivingItem;
use App\Http\Models\Uom;
use App\Http\Models\Vendor;

class InvoiceController extends Controller
{
    /**
     * @var \Illuminate\Database\Eloquent\Collection|static[]
     */
    private $invoice, $receiving, $receiving_item, $vendor, $ingredient, $uom;

    use Actions\Create, Actions\Delete, Actions\Update;

    /**
     * InvoiceController constructor.
     */
    public function __construct()
    {
        $this->invoice = Invoice::all();
        $this->receiving = Receiving::all();
        $this->receiving_item = ReceivingItem::all();
        $this->vendor = Vendor::all();
        $this->ingredient = Ingredient::all();
        $this->uom = Uom::all();
    }

    /**
     * show invoice data
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex()
    {
        $data = [
            'vendors' => $this->vendor,
            'invoices' => $this->invoice,
            'receivings' => $this->receiving,
            'receivingItems' => $this->receiving_item,
            'ingredients' => $this->ingredient,
            'uoms' => $this->uom
        ];

        return view('invoices.index', $data);
    }
}