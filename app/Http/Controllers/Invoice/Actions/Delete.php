<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 09/10/16
 * Time: 20:57
 */

namespace App\Http\Controllers\Invoice\Actions;


use App\Http\Models\Invoice;
use Illuminate\Contracts\Encryption\DecryptException;

trait Delete
{
    /**
     * delete invoice data by ID
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getDelete($id)
    {
        try {

            $decrypted = decrypt($id);

            if ($invoice = Invoice::find($decrypted)) {

                $invoice->delete();

                return redirect('invoice')->with('success', 'Data Deleted!');

            }

            return view('errors.404');

        } catch (DecryptException $e) {

            return $e;

        }
    }
}