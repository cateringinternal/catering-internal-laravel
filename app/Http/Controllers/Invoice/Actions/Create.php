<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 09/10/16
 * Time: 19:14
 */

namespace App\Http\Controllers\Invoice\Actions;


use App\Http\Controllers\Invoice\Requests\InvoiceRequest;
use App\Http\Models\Invoice;

trait Create
{
    /**
     * create invoice
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCreate()
    {
        $data = [
            'vendors' => $this->vendor,
            'receivings' => $this->receiving,
            'receivingItems' => $this->receiving_item,
            'ingredients' => $this->ingredient,
            'uoms' => $this->uom
        ];

        return view('invoices.actions.create', $data);
    }

    /**
     * post invoice create data into database
     *
     * @param InvoiceRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postCreate(InvoiceRequest $request)
    {
        $invoice_input = $request->except(['_token', 'submit']);

        $invoice = new Invoice;

        $invoice->date = $invoice_input['date'];
        $invoice->total_amount = $invoice_input['total_amount'];
        $invoice->vendor_id = $invoice_input['vendor'];
        $invoice->payment_type = $invoice_input['payment_type'];
        $invoice->payment_status = 'drafted';
        $invoice->issued_by = 1;
        $invoice->bank_account = $invoice_input['bank_account'];
        $invoice->receiving_id = $invoice_input['receiving'];

        $invoice->save();

        $getLastInvoice = Invoice::find($invoice->id);
        $getLastInvoice->code = 'IV000' . '' . $invoice->id;
        $getLastInvoice->save();

        return redirect('invoice')->with('success', 'Data Recorded!');
    }
}