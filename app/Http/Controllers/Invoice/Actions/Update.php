<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 09/10/16
 * Time: 20:59
 */

namespace App\Http\Controllers\Invoice\Actions;


use App\Http\Controllers\Invoice\Requests\InvoiceRequest;
use App\Http\Models\Invoice;
use Illuminate\Contracts\Encryption\DecryptException;

trait Update
{
    /**
     * update invoice by ID
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getUpdate($id)
    {
        try {

            $decrypted = decrypt($id);

            if ($invoice = $this->invoice->where('id', $decrypted)->first()) {

                $data = [
                    'invoice' => $invoice,
                    'vendors' => $this->vendor,
                    'receivings' => $this->receiving,
                    'receivingItems' => $this->receiving_item,
                    'ingredients' => $this->ingredient,
                    'uoms' => $this->uom

                ];

                return view('invoices.actions.update', $data);

            }

            return view('errors.404');

        } catch (DecryptException $e) {

            return $e;

        }
    }

    /**
     * update invoice data into database
     *
     * @param InvoiceRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postUpdate(InvoiceRequest $request, $id)
    {
        $invoice_input = $request->except(['_token', 'submit', '_method']);

        $invoice = Invoice::find($id);

        $invoice->date = $invoice_input['date'];
        $invoice->total_amount = $invoice_input['total_amount'];
        $invoice->vendor_id = $invoice_input['vendor'];
        $invoice->payment_type = $invoice_input['payment_type'];
        $invoice->payment_status = 'drafted';
        $invoice->issued_by = 1;
        $invoice->bank_account = $invoice_input['bank_account'];
        $invoice->receiving_id = $invoice_input['receiving'];

        $invoice->save();

        return redirect('invoice')->with('success', 'Data Updated!');
    }
}