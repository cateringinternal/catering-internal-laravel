<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 09/10/16
 * Time: 20:39
 */

namespace App\Http\Controllers\Invoice\Requests;


use Illuminate\Foundation\Http\FormRequest;

class InvoiceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'date' => 'required',
            'total_amount' => 'required|numeric',
            'vendor' => 'required',
            'payment_type' => 'required',
            'bank_account' => 'required',
            'receiving' => 'required',
        ];
    }
}