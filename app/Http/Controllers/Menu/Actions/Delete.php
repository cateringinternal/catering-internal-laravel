<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 23/09/16
 * Time: 6:05
 */

namespace App\Http\Controllers\Menu\Actions;


use App\Http\Models\Menu;
use Illuminate\Contracts\Encryption\DecryptException;

trait Delete
{
    /**
     * delete menu data by ID
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getDelete($id)
    {
        try {

            $decrypted = decrypt($id);

            if ($menu = Menu::find($decrypted)) {

                $menu->delete();

                return redirect('menu')->with('success' , 'Data Deleted!');

            }

            return view('errors.404');

        } catch (DecryptException $e) {

            return $e;

        }

    }
}