<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 28/09/16
 * Time: 22:01
 */

namespace App\Http\Controllers\MealPlanning\Actions;


use App\Http\Models\MealPlanning;
use Illuminate\Contracts\Encryption\DecryptException;

trait Delete
{
    /**
     * delete meal planning by ID
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getDelete($id)
    {
        try {

            $decrypted = decrypt($id);

            if ($meal_planning = MealPlanning::find($decrypted)) {

                $meal_planning->delete();

                return redirect('meal-planning')->with('success' , 'Data Deleted!');

            }

            return view('errors.404');

        } catch (DecryptException $e) {

            return $e;

        }
    }
}