<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 05/10/16
 * Time: 7:43
 */

namespace App\Http\Controllers\Requisition\Actions;


use App\Http\Models\Requisition;
use Illuminate\Contracts\Encryption\DecryptException;

trait Delete
{
    /**
     * delete requisition data by ID
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getDelete($id)
    {
        try {

            $decrypted = decrypt($id);

            if ($requisition = Requisition::find($decrypted)) {

                $requisition->delete();

                return redirect('requisition')->with('success' , 'Data Deleted!');

            }

            return view('errors.404');

        } catch (DecryptException $e) {

            return $e;

        }
    }
}