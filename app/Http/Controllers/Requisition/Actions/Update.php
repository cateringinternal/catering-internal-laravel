<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 05/10/16
 * Time: 7:45
 */

namespace App\Http\Controllers\Requisition\Actions;


use App\Events\RequisitionSaved;
use App\Http\Controllers\Requisition\Requests\RequisitionRequest;
use App\Http\Models\Requisition;
use App\Http\Models\RequisitionItem;
use Illuminate\Contracts\Encryption\DecryptException;

trait Update
{
    /**
     * update requisition data by ID
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getUpdate($id)
    {
        try {

            $decrypted = decrypt($id);

            if ($requisition = $this->requisition->where('id', $decrypted)->first()) {

                $data = [
                    'requisition' => $requisition,
                    'voyage_plannings' => $this->voyage_planning,
                    'vendors' => $this->vendor,
                    'ingredients' => $this->ingredient,
                    'uoms' => $this->uom
                ];

                return view('requisitions.actions.update', $data);

            }

            return view('errors.404');

        } catch (DecryptException $e) {

            return $e;

        }
    }

    /**
     * post requisition update data into database
     *
     * @param RequisitionRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postUpdate(RequisitionRequest $request, $id)
    {
        $requisition_input = $request->except(['_token', 'submit', '_method']);

        RequisitionItem::where('requisition_id', $id)->delete();

        $requisitions = Requisition::find($id);

        $requisitions->voyage_planning_id = $requisition_input['voyage_planning'];
        $requisitions->date = $requisition_input['date'];
        $requisitions->total_amount = $requisition_input['total_amount'];
        $requisitions->vendor_id = $requisition_input['vendor'];
        $requisitions->approval_status = 0; //means it will reviewed
        $requisitions->requested_by = 1; //means current user who logged in

        $requisitions->save();

        foreach ($requisition_input['requisitionItem'] as $requisitionItem) {
            event(new RequisitionSaved($requisitionItem, $id));
        }

        return redirect('requisition')->with('success' , 'Data Updated!');
    }

}