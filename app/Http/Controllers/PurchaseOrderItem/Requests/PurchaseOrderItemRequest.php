<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 08/10/16
 * Time: 17:23
 */

namespace App\Http\Controllers\PurchaseOrderItem\Requests;


use Illuminate\Foundation\Http\FormRequest;

class PurchaseOrderItemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'purchase_order' => 'required',
            'ingredient' => 'required',
            'quantity' => 'required|numeric',
            'unit' => 'required|numeric',
            'price' => 'required|numeric',
        ];
    }
}