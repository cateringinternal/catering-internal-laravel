<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 08/10/16
 * Time: 16:45
 */

namespace App\Http\Controllers\PurchaseOrderItem;


use App\Http\Controllers\Controller;
use App\Http\Models\Ingredient;
use App\Http\Models\PurchaseOrder;
use App\Http\Models\PurchaseOrderItem;

class PurchaseOrderItemController extends Controller
{
    /**
     * @var \Illuminate\Database\Eloquent\Collection|static[]
     */
    private $ingredient, $purchase_order, $purchase_order_item;

    use Actions\Create, Actions\Delete, Actions\Update;

    /**
     * PurchaseOrderItemController constructor.
     */
    public function __construct()
    {
        $this->ingredient = Ingredient::all();
        $this->purchase_order = PurchaseOrder::all();
        $this->purchase_order_item = PurchaseOrderItem::all();
    }

    /**
     * show purchase order item data
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex()
    {
        $data = [
            'purchase_orders' => $this->purchase_order,
            'purchase_order_items' => $this->purchase_order_item,
            'ingredients' => $this->ingredient
        ];

        return view('purchase-order-items.index', $data);
    }
}