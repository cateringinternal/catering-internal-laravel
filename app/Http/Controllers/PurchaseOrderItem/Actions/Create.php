<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 08/10/16
 * Time: 17:19
 */

namespace App\Http\Controllers\PurchaseOrderItem\Actions;


use App\Http\Controllers\PurchaseOrderItem\Requests\PurchaseOrderItemRequest;
use App\Http\Models\PurchaseOrderItem;

trait Create
{
    /**
     * create PO item data
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCreate()
    {
        $data = [
            'purchase_orders' => $this->purchase_order,
            'ingredients' => $this->ingredient
        ];

        return view('purchase-order-items.actions.create', $data);
    }

    /**
     * post PO item data into database
     *
     * @param PurchaseOrderItemRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postCreate(PurchaseOrderItemRequest $request)
    {
        $purchase_order_item_input = $request->except(['_token', 'submit']);

        $purchase_order_item = new PurchaseOrderItem;

        $purchase_order_item->purchase_order_id = $purchase_order_item_input['purchase_order'];
        $purchase_order_item->ingredient_id = $purchase_order_item_input['ingredient'];
        $purchase_order_item->quantity = $purchase_order_item_input['quantity'];
        $purchase_order_item->unit = $purchase_order_item_input['unit'];
        $purchase_order_item->price = $purchase_order_item_input['price'];

        $purchase_order_item->save();

        return redirect('purchase-order-item')->with('success' , 'Data Recorded!');
    }
}