<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 08/10/16
 * Time: 17:27
 */

namespace App\Http\Controllers\PurchaseOrderItem\Actions;


use App\Http\Models\PurchaseOrderItem;
use Illuminate\Contracts\Encryption\DecryptException;

trait Delete
{
    /**
     * delete PO item data
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getDelete($id)
    {
        try {

            $decrypted = decrypt($id);

            if ($purchase_order_item = PurchaseOrderItem::find($decrypted)) {

                $purchase_order_item->delete();

                return redirect('purchase-order-item')->with('success' , 'Data Deleted!');

            }

            return view('errors.404');

        } catch (DecryptException $e) {

            return $e;

        }
    }
}