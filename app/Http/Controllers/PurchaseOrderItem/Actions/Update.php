<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 08/10/16
 * Time: 17:28
 */

namespace App\Http\Controllers\PurchaseOrderItem\Actions;


use App\Http\Controllers\PurchaseOrderItem\Requests\PurchaseOrderItemRequest;
use App\Http\Models\PurchaseOrderItem;
use Illuminate\Contracts\Encryption\DecryptException;

trait Update
{
    /**
     * update PO item data
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getUpdate($id)
    {
        try {

            $decrypted = decrypt($id);

            if ($purchase_order_item = $this->purchase_order_item->where('id', $decrypted)->first()) {

                $data = [
                    'purchase_order_item' => $purchase_order_item,
                    'purchase_orders' => $this->purchase_order,
                    'ingredients' => $this->ingredient
                ];

                return view('purchase-order-items.actions.update', $data);

            }

            return view('errors.404');

        } catch (DecryptException $e) {

            return $e;

        }
    }

    /**
     * post PO item update into database
     *
     * @param PurchaseOrderItemRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postUpdate(PurchaseOrderItemRequest $request, $id)
    {
        $purchase_order_item_input = $request->except(['_token', 'submit', '_method']);

        $purchase_order_item = PurchaseOrderItem::find($id);

        $purchase_order_item->purchase_order_id = $purchase_order_item_input['purchase_order'];
        $purchase_order_item->ingredient_id = $purchase_order_item_input['ingredient'];
        $purchase_order_item->quantity = $purchase_order_item_input['quantity'];
        $purchase_order_item->unit = $purchase_order_item_input['unit'];
        $purchase_order_item->price = $purchase_order_item_input['price'];

        $purchase_order_item->save();

        return redirect('purchase-order-item')->with('success' , 'Data Updated!');
    }
}