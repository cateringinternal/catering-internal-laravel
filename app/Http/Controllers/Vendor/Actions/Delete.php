<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 23/09/16
 * Time: 6:38
 */

namespace App\Http\Controllers\Vendor\Actions;


use App\Http\Models\Vendor;
use Illuminate\Contracts\Encryption\DecryptException;

trait Delete
{
    /**
     * delete vendor data by ID
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getDelete($id)
    {
        try {

            $decrypted = decrypt($id);

            if ($vendor = Vendor::find($decrypted)) {

                $vendor->delete();

                return redirect('vendor')->with('success' , 'Data Deleted!');

            }

            return view('errors.404');

        } catch (DecryptException $e) {

            return $e;

        }

    }
}