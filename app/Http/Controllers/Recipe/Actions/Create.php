<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 22/09/16
 * Time: 21:01
 */

namespace App\Http\Controllers\Recipe\Actions;


use App\Events\RecipeSaved;
use App\Http\Controllers\Recipe\Requests\RecipeRequest;
use App\Http\Models\Recipe;

trait Create
{
    /**
     * show create recipe data
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCreate(){

        $data = [
            'ingredients' => $this->ingredient,
            'uoms' => $this->uom
        ];

        return view('recipes.actions.create', $data);
    }

    public function postCreate(RecipeRequest $request){

        $recipe_input = $request->except(['_token', 'submit']);

        $path = '';

        if ($request->hasFile('picture'))
        {
            $request->file('picture')->storeAs('recipes', $request->file('picture')->getClientOriginalName(),
                'public');

            $path = '/recipes/' . $request->file('picture')->getClientOriginalName();
        }

        $recipe = new Recipe;

        $recipe->title = $recipe_input['title'];
        $recipe->picture = $path;
        $recipe->description = $recipe_input['description'];
        $recipe->direction = $recipe_input['direction'];
        $recipe->menu_type = $recipe_input['menu_type'];

        $recipe->save();

        foreach ($recipe_input['ingredientRecipe'] as $ingredientRecipe) {
            event(new RecipeSaved($ingredientRecipe, $recipe->id));
        }

        return redirect('recipe')->with('success', 'Data Recorded!');

    }
}