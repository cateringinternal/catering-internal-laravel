<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 23/09/16
 * Time: 4:16
 */

namespace App\Http\Controllers\Recipe\Actions;


use App\Events\RecipeSaved;
use App\Http\Controllers\Recipe\Requests\RecipeRequest;
use App\Http\Models\IngredientRecipe;
use App\Http\Models\Recipe;
use Illuminate\Contracts\Encryption\DecryptException;

trait Update
{
    /**
     * update recipe data by ID
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getUpdate($id){

        try {

            $decrypted = decrypt($id);

            if ($recipe = $this->recipe->where('id', $decrypted)->first()) {

                $data = [
                    'recipe' => $recipe,
                    'ingredients' => $this->ingredient,
                    'uoms' => $this->uom
                ];

                return view('recipes.actions.update', $data);

            }

            return view('errors.404');

        } catch (DecryptException $e) {

            return $e;

        }

    }

    /**
     * post recipe data updated into database
     *
     * @param RecipeRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postUpdate(RecipeRequest $request, $id){

        $recipe_input = $request->except(['_token', 'submit', '_method']);

        $path = $recipe_input['previous_picture'];

        if ($request->hasFile('picture'))
        {
            $request->file('picture')->storeAs('recipes', $request->file('picture')->getClientOriginalName(),
                'public');

            $path = '/recipes/' . $request->file('picture')->getClientOriginalName();
        }

        IngredientRecipe::where('recipe_id', $id)->delete();

        $recipe = Recipe::find($id);

        $recipe->title = $recipe_input['title'];
        $recipe->picture = $path;
        $recipe->description = $recipe_input['description'];
        $recipe->direction = $recipe_input['direction'];
        $recipe->menu_type = $recipe_input['menu_type'];

        $recipe->save();

        foreach ($recipe_input['ingredientRecipe'] as $ingredientRecipe) {
            event(new RecipeSaved($ingredientRecipe, $recipe->id));
        }

        return redirect('recipe')->with('success', 'Data Updated!');

    }
}