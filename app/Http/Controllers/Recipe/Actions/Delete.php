<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 23/09/16
 * Time: 4:14
 */

namespace App\Http\Controllers\Recipe\Actions;


use App\Http\Models\Recipe;
use Illuminate\Contracts\Encryption\DecryptException;

trait Delete
{
    /**
     * delete recipe data by ID
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getDelete($id){

        try {

            $decrypted = decrypt($id);

            if ($recipe = Recipe::find($decrypted)) {

                $recipe->delete();

                return redirect('recipe')->with('success', 'Data Deleted!');

            }

            return view('errors.404');

        } catch (DecryptException $e) {

            return $e;

        }

    }
}