<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 15/10/16
 * Time: 13:41
 */

namespace App\Http\Controllers\InventoryOut\Requests;


use Illuminate\Foundation\Http\FormRequest;

class InventoryOutRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'from' => 'required',
            'from' => 'required',
            'quantity' => 'required|numeric',
            'unit' => 'required',
            'is_mutation' => 'required',
            'request_inventory_out' => 'required',
        ];
    }
}