<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 15/10/16
 * Time: 13:28
 */

namespace App\Http\Controllers\InventoryOut;


use App\Http\Controllers\Controller;
use App\Http\Models\Inventory;
use App\Http\Models\InventoryOut;

class InventoryOutController extends Controller
{
    /**
     * @var \Illuminate\Database\Eloquent\Collection|static[]
     */
    private $inventory, $inventory_out;

    use Actions\Create, Actions\Delete, Actions\Update;

    /**
     * InventoryOutController constructor.
     */
    public function __construct()
    {
        $this->inventory = Inventory::all();
        $this->inventory_out = InventoryOut::all();
    }

    /**
     * show inventory out data
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex()
    {
        $data = [
            'inventories' => $this->inventory,
            'inventories_out' => $this->inventory_out
        ];

        return view('inventories-out.index', $data);
    }
}