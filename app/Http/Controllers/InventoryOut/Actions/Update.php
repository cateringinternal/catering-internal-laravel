<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 15/10/16
 * Time: 13:46
 */

namespace App\Http\Controllers\InventoryOut\Actions;


use App\Http\Controllers\InventoryOut\Requests\InventoryOutRequest;
use App\Http\Models\InventoryOut;
use Illuminate\Contracts\Encryption\DecryptException;

trait Update
{
    /**
     * update inventory data by ID
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getUpdate($id)
    {
        try {

            $decrypted = decrypt($id);

            if ($inventory_out = $this->inventory_out->where('id', $decrypted)->first()) {

                $data = [
                    'inventory_out' => $inventory_out,
                    'inventories' => $this->inventory
                ];

                return view('inventories-out.actions.update', $data);

            }

            return view('errors.404');

        } catch (DecryptException $e) {

            return $e;

        }
    }

    /**
     * post inventory data updated into database
     *
     * @param InventoryOutRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postUpdate(InventoryOutRequest $request, $id)
    {
        $inventory_out_input = $request->except(['_token', 'submit', '_method']);

        $inventories_out = InventoryOut::find($id);

        $inventories_out->from = $inventory_out_input['from'];
        $inventories_out->quantity = $inventory_out_input['quantity'];
        $inventories_out->unit = $inventory_out_input['unit'];
        $inventories_out->is_mutation = $inventory_out_input['is_mutation'];
        $inventories_out->request_inventory_out = $inventory_out_input['request_inventory_out'];

        $inventories_out->save();

        return redirect('inventory-out')->with('success' , 'Data Updated!');
    }
}