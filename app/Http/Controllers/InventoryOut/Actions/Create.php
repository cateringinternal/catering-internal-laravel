<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 15/10/16
 * Time: 13:36
 */

namespace App\Http\Controllers\InventoryOut\Actions;


use App\Http\Controllers\InventoryOut\Requests\InventoryOutRequest;
use App\Http\Models\InventoryOut;

trait Create
{
    /**
     * create inventory out data
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCreate()
    {
        $data = [
            'inventories' => $this->inventory,
        ];

        return view('inventories-out.actions.create', $data);
    }

    /**
     * post inventory data created into database
     *
     * @param InventoryOutRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postCreate(InventoryOutRequest $request)
    {
        $inventory_out_input = $request->except(['_token', 'submit']);

        $inventories_out = new InventoryOut;

        $inventories_out->from = $inventory_out_input['from'];
        $inventories_out->quantity = $inventory_out_input['quantity'];
        $inventories_out->unit = $inventory_out_input['unit'];
        $inventories_out->is_mutation = $inventory_out_input['is_mutation'];
        $inventories_out->request_inventory_out = $inventory_out_input['request_inventory_out'];

        $inventories_out->save();

        return redirect('inventory-out')->with('success' , 'Data Recorded!');
    }
}