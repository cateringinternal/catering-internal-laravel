<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 15/10/16
 * Time: 13:44
 */

namespace App\Http\Controllers\InventoryOut\Actions;


use App\Http\Models\InventoryOut;
use Illuminate\Contracts\Encryption\DecryptException;

trait Delete
{
    /**
     * delete inventory out data by ID
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getDelete($id)
    {
        try {

            $decrypted = decrypt($id);

            if ($inventory_out = InventoryOut::find($decrypted)) {

                $inventory_out->delete();

                return redirect('inventory-out')->with('success' , 'Data Deleted!');

            }

            return view('errors.404');

        } catch (DecryptException $e) {

            return $e;

        }
    }
}