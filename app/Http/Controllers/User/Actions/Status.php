<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 16/10/16
 * Time: 21:05
 */

namespace App\Http\Controllers\User\Actions;


use App\Http\Models\User;
use Illuminate\Contracts\Encryption\DecryptException;

trait Status
{
    /**
     * update user status
     *
     * @param $status
     * @param $id
     * @return bool|\Illuminate\Http\RedirectResponse
     */
    public function updateStatus($status, $id)
    {

        try {

            $decrypted = decrypt($id);

            $status_update = 0;

            if ($user = User::find($decrypted))
            {
                if ($status == 0)
                {
                    $status_update = 1;
                }

                $user->is_active = $status_update;

                $user->save();

                return redirect('user')->with('success', 'Data Updated!');
            }

            return view('errors.404');

        } catch (DecryptException $e) {

            return $e;

        }
    }
}