<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 22/09/16
 * Time: 1:37
 */

namespace App\Http\Controllers\Cruise\Actions;


use App\Http\Models\Cruise;
use Illuminate\Contracts\Encryption\DecryptException;

trait Delete
{
    /**
     * delete cruise data by ID
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getDelete($id)
    {

        try {

            $decrypted = decrypt($id);

            if ($cruise = Cruise::find($decrypted)) {

                $cruise->delete();

                return redirect('cruise')->with('success', 'Data Deleted');

            }

            return view('errors.404');

        } catch (DecryptException $e) {

            return $e;

        }
    }
}