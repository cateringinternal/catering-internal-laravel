<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 20/10/16
 * Time: 17:53
 */

namespace App\Http\Controllers\Port\Requests;


use Illuminate\Foundation\Http\FormRequest;

class PortRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'name' => 'required',
            'address' => 'required',
            'city' => 'required',
            'phone' => 'required|numeric',
        ];
    }
}