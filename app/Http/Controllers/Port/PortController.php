<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 20/10/16
 * Time: 17:47
 */

namespace App\Http\Controllers\Port;


use App\Http\Controllers\Controller;
use App\Http\Models\Port;

class PortController extends Controller
{
    /**
     * @var \Illuminate\Database\Eloquent\Collection|static[]
     */
    private $port;

    use Actions\Create, Actions\Delete, Actions\Update;

    /**
     * PortController constructor.
     */
    public function __construct()
    {
        $this->port = Port::all();
    }

    /**
     * show port data
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex()
    {
        $data = [
            'ports' => $this->port
        ];

        return view('ports.index', $data);
    }
}