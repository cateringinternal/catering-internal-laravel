<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 20/10/16
 * Time: 18:02
 */

namespace App\Http\Controllers\Port\Actions;


use App\Http\Controllers\Port\Requests\PortRequest;
use App\Http\Models\Port;
use Illuminate\Contracts\Encryption\DecryptException;

trait Update
{
    /**
     * update port data by ID
     *
     * @param $id
     * @return DecryptException|bool|\Exception|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getUpdate($id)
    {
        try {

            $decrypted = decrypt($id);

            if ($port = $this->port->where('id', $decrypted)->first()) {

                $data = [
                    'port' => $port
                ];

                return view('ports.actions.update', $data);

            }

            return view('errors.404');

        } catch (DecryptException $e) {

            return $e;

        }
    }

    /**
     * post port data updated into database
     *
     * @param PortRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postUpdate(PortRequest $request, $id)
    {
        $port_input = $request->except(['_token', 'submit']);

        $ports = Port::find($id);

        $ports->name = $port_input['name'];
        $ports->address = $port_input['address'];
        $ports->city = $port_input['city'];
        $ports->phone = $port_input['phone'];

        $ports->save();

        return redirect('port')->with('success' , 'Data Updated!');
    }
}