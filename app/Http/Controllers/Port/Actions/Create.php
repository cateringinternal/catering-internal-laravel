<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 20/10/16
 * Time: 17:52
 */

namespace App\Http\Controllers\Port\Actions;


use App\Http\Controllers\Port\Requests\PortRequest;
use App\Http\Models\Port;

trait Create
{
    /**
     * create port data
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCreate()
    {
        return view('ports.actions.create');
    }

    /**
     * post port data created into database
     *
     * @param PortRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postCreate(PortRequest $request)
    {
        $port_input = $request->except(['_token', 'submit']);

        $ports = new Port;

        $ports->name = $port_input['name'];
        $ports->address = $port_input['address'];
        $ports->city = $port_input['city'];
        $ports->phone = $port_input['phone'];

        $ports->save();

        return redirect('port')->with('success' , 'Data Recorded!');
    }
}