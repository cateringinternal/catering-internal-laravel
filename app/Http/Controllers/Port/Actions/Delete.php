<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 20/10/16
 * Time: 18:01
 */

namespace App\Http\Controllers\Port\Actions;


use App\Http\Models\Port;
use Illuminate\Contracts\Encryption\DecryptException;

trait Delete
{
    /**
     * delete port data by ID
     *
     * @param $id
     * @return bool|\Exception|DecryptException|\Illuminate\Http\RedirectResponse
     */
    public function getDelete($id)
    {
        try {

            $decrypted = decrypt($id);

            if ($port = Port::find($decrypted)) {

                $port->delete();

                return redirect('port')->with('success', 'Data Deleted');

            }

            return view('errors.404');

        } catch (DecryptException $e) {

            return $e;

        }
    }
}