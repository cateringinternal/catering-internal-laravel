<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 28/09/16
 * Time: 21:13
 */

namespace App\Http\Controllers\VoyagePlanning\Actions;


use App\Http\Models\VoyagePlanning;
use Illuminate\Contracts\Encryption\DecryptException;

trait Delete
{
    /**
     * delete voyage planning data by ID
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getDelete($id)
    {
        try {

            $decrypted = decrypt($id);

            if ($voyage_planning = VoyagePlanning::find($decrypted)) {

                $voyage_planning->delete();

                return redirect('voyage-planning')->with('success' , 'Data Deleted!');

            }

            return view('errors.404');

        } catch (DecryptException $e) {

            return $e;

        }
    }
}