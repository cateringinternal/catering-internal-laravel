<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 18/10/16
 * Time: 6:58
 */

namespace App\Http\Controllers\Role\Requests;


use Illuminate\Foundation\Http\FormRequest;

class RoleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'name' => 'required',
            'display_name' => 'required',
            'description_name' => 'required',
        ];
    }
}