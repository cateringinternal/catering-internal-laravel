<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 18/10/16
 * Time: 7:05
 */

namespace App\Http\Controllers\Role\Actions;


use App\Http\Controllers\Role\Requests\RoleRequest;
use App\Http\Models\Role;
use Illuminate\Contracts\Encryption\DecryptException;

trait Update
{
    /**
     * update role data by ID
     *
     * @param $id
     * @return bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getUpdate($id)
    {
        try {

            $decrypted = decrypt($id);

            if ($role = $this->role->where('id', $decrypted)->first()) {

                $data = [
                    'role' => $role
                ];

                return view('roles.actions.update', $data);
            }

            return view('errors.404');

        } catch (DecryptException $e) {

            return $e;

        }
    }

    /**
     * post role data updated into database
     *
     * @param RoleRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postUpdate(RoleRequest $request, $id)
    {
        $roles_input = $request->except(['_token', 'submit', '_method']);

        $roles = Role::find($id);

        $roles->name = $roles_input['name'];
        $roles->display_name = $roles_input['display_name'];
        $roles->description_name = $roles_input['description_name'];

        $roles->save();

        return redirect('role')->with('success' , 'Data Updated!');
    }
}