<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 18/10/16
 * Time: 6:55
 */

namespace App\Http\Controllers\Role\Actions;


use App\Http\Controllers\Role\Requests\RoleRequest;
use App\Http\Models\Role;

trait Create
{
    /**
     * create role data
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCreate()
    {
        return view('roles.actions.create');
    }

    /**
     * post role data created into database
     *
     * @param RoleRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postCreate(RoleRequest $request)
    {
        $roles_input = $request->except(['_token', 'submit']);

        $roles = new Role;

        $roles->name = $roles_input['name'];
        $roles->display_name = $roles_input['display_name'];
        $roles->description_name = $roles_input['description_name'];

        $roles->save();

        return redirect('role')->with('success' , 'Data Recorded!');
    }
}