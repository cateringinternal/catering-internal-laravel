<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 18/10/16
 * Time: 7:03
 */

namespace App\Http\Controllers\Role\Actions;


use App\Http\Models\Role;
use Illuminate\Contracts\Encryption\DecryptException;

trait Delete
{
    /**
     * delete role data by ID
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getDelete($id)
    {
        try {

            $decrypted = decrypt($id);

            if ($role = Role::find($decrypted)) {

                $role->delete();

                return redirect('role')->with('success' , 'Data Deleted!');

            }

            return view('errors.404');

        } catch (DecryptException $e) {

            return $e;

        }
    }
}