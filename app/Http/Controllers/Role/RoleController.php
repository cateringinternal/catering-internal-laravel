<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 17/10/16
 * Time: 18:07
 */

namespace App\Http\Controllers\Role;


use App\Http\Controllers\Controller;
use App\Http\Models\Role;

class RoleController extends Controller
{
    /**
     * @var \Illuminate\Database\Eloquent\Collection|static[]
     */
    private $role;

    use Actions\Create, Actions\Delete, Actions\Update;

    /**
     * RoleController constructor.
     */
    public function __construct()
    {
        $this->role = Role::all();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex()
    {
        $data = [
            'roles' => $this->role
        ];

        return view('roles.index', $data);
    }
}