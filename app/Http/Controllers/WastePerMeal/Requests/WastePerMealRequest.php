<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 16/10/16
 * Time: 14:31
 */

namespace App\Http\Controllers\WastePerMeal\Requests;


use Illuminate\Foundation\Http\FormRequest;

class WastePerMealRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'waste' => 'required',
            'meal_planning' => 'required',
            'weight' => 'required|numeric',
        ];
    }
}