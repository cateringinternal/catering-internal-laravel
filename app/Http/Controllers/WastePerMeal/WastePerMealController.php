<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 16/10/16
 * Time: 14:21
 */

namespace App\Http\Controllers\WastePerMeal;


use App\Http\Controllers\Controller;
use App\Http\Models\MealPlanning;
use App\Http\Models\Waste;
use App\Http\Models\WastePerMeal;

class WastePerMealController extends Controller
{
    /**
     * @var \Illuminate\Database\Eloquent\Collection|static[]
     */
    private $waste_per_meal, $waste, $meal_planning;

    use Actions\Create, Actions\Delete, Actions\Update;

    /**
     * WastePerMealController constructor.
     */
    public function __construct()
    {
        $this->waste_per_meal = WastePerMeal::all();
        $this->waste = Waste::all();
        $this->meal_planning = MealPlanning::all();
    }

    /**
     * show waste per meal data
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex()
    {
        $data = [
            'waste_per_meals' => $this->waste_per_meal,
            'wastes' => $this->waste,
            'meal_plannings' => $this->meal_planning
        ];

        return view('waste-per-meals.index', $data);
    }
}