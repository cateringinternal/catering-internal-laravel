<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 16/10/16
 * Time: 14:27
 */

namespace App\Http\Controllers\WastePerMeal\Actions;


use App\Http\Controllers\WastePerMeal\Requests\WastePerMealRequest;
use App\Http\Models\WastePerMeal;

trait Create
{
    /**
     * create waste per meal data
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCreate()
    {
        $data = [
            'wastes' => $this->waste,
            'meal_plannings' => $this->meal_planning
        ];

        return view('waste-per-meals.actions.create', $data);
    }

    /**
     * post waste per meal data created into database
     *
     * @param WastePerMealRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postCreate(WastePerMealRequest $request)
    {
        $waste_per_meal_input = $request->except(['_token', 'submit']);

        $waste_per_meals = new WastePerMeal;

        $waste_per_meals->waste_id = $waste_per_meal_input['waste'];
        $waste_per_meals->meals_planning_id = $waste_per_meal_input['meal_planning'];
        $waste_per_meals->weight = $waste_per_meal_input['weight'];

        $waste_per_meals->save();

        return redirect('waste-per-meal')->with('success' , 'Data Recorded!');
    }

}