<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 16/10/16
 * Time: 14:34
 */

namespace App\Http\Controllers\WastePerMeal\Actions;


use App\Http\Models\WastePerMeal;
use Illuminate\Contracts\Encryption\DecryptException;

trait Delete
{
    /**
     * delete waste per meal data by ID
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getDelete($id)
    {
        try {

            $decrypted = decrypt($id);

            if ($waste_per_meal = WastePerMeal::find($decrypted)) {

                $waste_per_meal->delete();

                return redirect('waste-per-meal')->with('success' , 'Data Deleted!');

            }

            return view('errors.404');

        } catch (DecryptException $e) {

            return $e;

        }
    }
}