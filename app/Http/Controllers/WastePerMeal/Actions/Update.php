<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 16/10/16
 * Time: 14:36
 */

namespace App\Http\Controllers\WastePerMeal\Actions;


use App\Http\Controllers\WastePerMeal\Requests\WastePerMealRequest;
use App\Http\Models\WastePerMeal;
use Illuminate\Contracts\Encryption\DecryptException;

trait Update
{
    /**
     * update waste per meal data by ID
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getUpdate($id)
    {
        try {

            $decrypted = decrypt($id);

            if ($waste_per_meal = $this->waste_per_meal->where('id', $decrypted)->first()) {

                $data = [
                    'waste_per_meal' => $waste_per_meal,
                    'wastes' => $this->waste,
                    'meal_plannings' => $this->meal_planning
                ];

                return view('waste-per-meals.actions.update', $data);
            }

            return view('errors.404');

        } catch (DecryptException $e) {

            return $e;

        }
    }

    /**
     * post waste per meal data updated into database
     *
     * @param WastePerMealRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postUpdate(WastePerMealRequest $request, $id)
    {
        $waste_per_meal_input = $request->except(['_token', 'submit', '_method']);

        $waste_per_meals = WastePerMeal::find($id);

        $waste_per_meals->waste_id = $waste_per_meal_input['waste'];
        $waste_per_meals->meals_planning_id = $waste_per_meal_input['meal_planning'];
        $waste_per_meals->weight = $waste_per_meal_input['weight'];

        $waste_per_meals->save();

        return redirect('waste-per-meal')->with('success' , 'Data Updated!');
    }

}