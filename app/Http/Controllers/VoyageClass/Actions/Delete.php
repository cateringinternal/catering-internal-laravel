<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 28/09/16
 * Time: 20:14
 */

namespace App\Http\Controllers\VoyageClass\Actions;


use App\Http\Models\VoyageClass;
use Illuminate\Contracts\Encryption\DecryptException;

trait Delete
{
    /**
     * delete voyage class data by ID
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getDelete($id)
    {
        try {

            $decrypted = decrypt($id);

            if ($voyage_class = VoyageClass::find($decrypted)) {

                $voyage_class->delete();

                return redirect('voyage-class')->with('success' , 'Data Deleted!');

            }

            return view('errors.404');

        } catch (DecryptException $e) {

            return $e;

        }

    }
}