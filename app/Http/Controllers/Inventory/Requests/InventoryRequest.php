<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 15/10/16
 * Time: 12:43
 */

namespace App\Http\Controllers\Inventory\Requests;


use Illuminate\Foundation\Http\FormRequest;

class InventoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'ingredient' => 'required',
            'quantity' => 'required|numeric',
            'unit' => 'required',
            'expiry_date' => 'required',
            'nfc_label' => 'required',
            'remark' => 'required',
            'warehouse' => 'required',
            'receiving_item' => 'required',
            'mutation_from' => 'required',
        ];
    }
}