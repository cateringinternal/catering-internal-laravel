<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 15/10/16
 * Time: 11:53
 */

namespace App\Http\Controllers\Inventory;


use App\Http\Controllers\Controller;
use App\Http\Models\Ingredient;
use App\Http\Models\Inventory;
use App\Http\Models\ReceivingItem;
use App\Http\Models\Warehouse;

class InventoryController extends Controller
{
    /**
     * @var \Illuminate\Database\Eloquent\Collection|static[]
     */
    private $inventory, $ingredient, $warehouse, $receiving_item;

    use Actions\Create, Actions\Delete, Actions\Update;

    /**
     * InventoryController constructor.
     */
    public function __construct()
    {
        $this->inventory = Inventory::all();
        $this->ingredient = Ingredient::all();
        $this->warehouse = Warehouse::all();
        $this->receiving_item = ReceivingItem::all();
    }

    /**
     * show inventory data
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex()
    {
        $data = [
            'inventories' => $this->inventory,
            'ingredients' => $this->ingredient,
            'warehouses' => $this->warehouse,
            'receiving_items' => $this->receiving_item
        ];

        return view('inventories.index', $data);
    }
}