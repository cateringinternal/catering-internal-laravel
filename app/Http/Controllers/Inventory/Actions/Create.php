<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 15/10/16
 * Time: 12:32
 */

namespace App\Http\Controllers\Inventory\Actions;


use App\Http\Controllers\Inventory\Requests\InventoryRequest;
use App\Http\Models\Inventory;

trait Create
{
    /**
     * create inventory data
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCreate()
    {
        $data = [
            'ingredients' => $this->ingredient,
            'warehouses' => $this->warehouse,
            'receiving_items' => $this->receiving_item
        ];

        return view('inventories.actions.create', $data);
    }

    /**
     * post inventory data into database
     *
     * @param InventoryRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postCreate(InventoryRequest $request)
    {
        $inventory_input = $request->except(['_token', 'submit']);

        $inventories = new Inventory();

        $inventories->ingredient_id = $inventory_input['ingredient'];
        $inventories->quantity = $inventory_input['quantity'];
        $inventories->unit = $inventory_input['unit'];
        $inventories->expiry_date = $inventory_input['expiry_date'];
        $inventories->nfc_label = $inventory_input['nfc_label'];
        $inventories->remark = $inventory_input['remark'];
        $inventories->warehouse_id = $inventory_input['warehouse'];
        $inventories->receiving_item_id = $inventory_input['receiving_item'];
        $inventories->mutation_from = $inventory_input['mutation_from'];

        $inventories->save();

        return redirect('inventory')->with('success' , 'Data Recorded!');
    }
}