<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 15/10/16
 * Time: 13:12
 */

namespace App\Http\Controllers\Inventory\Actions;


use App\Http\Models\Inventory;
use Illuminate\Contracts\Encryption\DecryptException;

trait Delete
{
    /**
     * delete inventory data by ID
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getDelete($id)
    {
        try {

            $decrypted = decrypt($id);

            if ($inventory = Inventory::find($decrypted)) {

                $inventory->delete();

                return redirect('inventory')->with('success' , 'Data Deleted!');

            }

            return view('errors.404');

        } catch (DecryptException $e) {

            return $e;

        }
    }

}