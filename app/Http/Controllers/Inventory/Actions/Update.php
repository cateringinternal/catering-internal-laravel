<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 15/10/16
 * Time: 13:14
 */

namespace App\Http\Controllers\Inventory\Actions;


use App\Http\Controllers\Inventory\Requests\InventoryRequest;
use App\Http\Models\Inventory;
use Illuminate\Contracts\Encryption\DecryptException;

trait Update
{
    /**
     * update inventory data
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getUpdate($id)
    {
        try {

            $decrypted = decrypt($id);

            if ($inventory = $this->inventory->where('id', $decrypted)->first()) {

                $data = [
                    'inventory' => $inventory,
                    'ingredients' => $this->ingredient,
                    'warehouses' => $this->warehouse,
                    'receiving_items' => $this->receiving_item
                ];

                return view('inventories.actions.update', $data);

            }

            return view('errors.404');

        } catch (DecryptException $e) {

            return $e;

        }
    }

    /**
     * post update inventory data into database
     *
     * @param InventoryRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postUpdate(InventoryRequest $request, $id)
    {
        $inventory_input = $request->except(['_token', 'submit', '_method']);

        $inventories = Inventory::find($id);

        $inventories->ingredient_id = $inventory_input['ingredient'];
        $inventories->quantity = $inventory_input['quantity'];
        $inventories->unit = $inventory_input['unit'];
        $inventories->expiry_date = $inventory_input['expiry_date'];
        $inventories->nfc_label = $inventory_input['nfc_label'];
        $inventories->remark = $inventory_input['remark'];
        $inventories->warehouse_id = $inventory_input['warehouse'];
        $inventories->receiving_item_id = $inventory_input['receiving_item'];
        $inventories->mutation_from = $inventory_input['mutation_from'];

        $inventories->save();

        return redirect('inventory')->with('success' , 'Data Updated!');
    }
}