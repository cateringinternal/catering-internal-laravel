<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 20/09/16
 * Time: 6:49
 */

namespace App\Http\Controllers\Route\Actions;

use App\Events\RouteSaved;
use App\Http\Controllers\Route\Requests\RouteRequest;
use App\Http\Models\Route;
use App\Http\Models\RouteStop;
use Illuminate\Contracts\Encryption\DecryptException;

trait Update
{
    /**
     * update data route by ID
     *
     */
    public function getUpdate($id){

        try {

            $decrypted = decrypt($id);

            if ($route = $this->route->where('id', $decrypted)->first()) {

                $data = [
                    'ports' => $this->port,
                    'routes' => $route
                ];

                return view('routes.actions.update', $data);
            }

            return view('errors.404');

        } catch (DecryptException $e) {

            return $e;

        }

    }

    /**
     * post data update into database
     *
     * @param RouteRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postUpdate(RouteRequest $request, $id){

        $routes_input = $request->except(['_token', 'submit', '_method']);

        RouteStop::where('route_id', $id)->delete();

        $routes = Route::find($id);

        $routes->port_origin = $routes_input['port_origin'];
        $routes->port_destination = $routes_input['port_destination'];
        $routes->eta = $routes_input['eta'];

        $routes->save();

        foreach ($routes_input['routeStop'] as $routeStop) {
            event(new RouteSaved($routeStop, $routes->id));
        }

        return redirect('route')->with('success' , 'Data Updated!');
    }

}