<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 20/09/16
 * Time: 6:34
 */

namespace App\Http\Controllers\Route\Actions;

use App\Http\Models\Route;
use Illuminate\Contracts\Encryption\DecryptException;

trait Delete
{
    /**
     * delete data by ID
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getDelete($id){

        try {

            $decrypted = decrypt($id);

            if ($route = Route::find($decrypted)) {

                $route->delete();

                return redirect('route')->with('success', 'Data Deleted');

            }

            return view('errors.404');

        } catch (DecryptException $e) {

            return $e;

        }

    }
}