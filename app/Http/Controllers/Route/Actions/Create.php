<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 20/09/16
 * Time: 5:35
 */

namespace App\Http\Controllers\Route\Actions;

use App\Events\RouteSaved;
use App\Http\Controllers\Route\Requests\RouteRequest;
use App\Http\Models\Route;

trait Create
{
    /**
     * create data route
     *
     */
    public function getCreate(){

        $data = [
            'ports' => $this->port
        ];

        return view('routes.actions.create', $data);

    }

    /**
     * post data request into database
     *
     * @param RouteRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postCreate(RouteRequest $request){

        $routes_input = $request->except(['_token', 'submit']);

        $routes = new Route;

        $routes->port_origin = $routes_input['port_origin'];
        $routes->port_destination = $routes_input['port_destination'];
        $routes->eta = $routes_input['eta'];

        $routes->save();

        foreach ($routes_input['routeStop'] as $routeStop) {
            event(new RouteSaved($routeStop, $routes->id));
        }

        return redirect('route')->with('success' , 'Data Recorded!');


    }
}