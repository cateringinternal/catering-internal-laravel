<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 24/10/16
 * Time: 6:53
 */

namespace App\Observers;


use App\Http\Models\Route;
use App\Http\Models\RouteStop;

class RouteObserver
{
    /**
     * delete route stop when route deleted
     *
     * @param Route $route
     */
    public function deleted(Route $route)
    {
        if ($routeStop = RouteStop::where('route_id', $route->id)) {
            $routeStop->delete();
        }
    }
}