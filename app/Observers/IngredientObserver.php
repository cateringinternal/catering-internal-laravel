<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 23/10/16
 * Time: 4:19
 */

namespace App\Observers;


use App\Http\Models\Ingredient;
use App\Http\Models\IngredientRecipe;
use App\Http\Models\Variant;

class IngredientObserver
{
    /**
     * delete variant and ingredient recipe when ingredient deleted
     *
     * @param Ingredient $ingredient
     */
    public function deleted(Ingredient $ingredient)
    {

        if ($variant = Variant::where('ingredient_id', $ingredient->id)) {
            $variant->delete();
        }

        if ($ingredientRecipe = IngredientRecipe::where('ingredient_id', $ingredient->id)) {
            $ingredientRecipe->delete();
        }

    }
}