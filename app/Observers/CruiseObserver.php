<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 24/10/16
 * Time: 3:34
 */

namespace App\Observers;


use App\Http\Models\Cruise;
use App\Http\Models\Warehouse;

class CruiseObserver
{
    /**
     * delete warehouse where cruise deleted
     *
     * @param Cruise $cruise
     */
    public function deleted(Cruise $cruise)
    {
        if ($warehouse = Warehouse::where('cruise_id', $cruise->id)) {
            $warehouse->delete();
        }
    }
}