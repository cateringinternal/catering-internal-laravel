<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 25/10/16
 * Time: 20:14
 */

namespace App\Observers;


use App\Http\Models\Requisition;
use App\Http\Models\RequisitionItem;

class RequisitionObserver
{
    /**
     * delete requisition item when requisition data deleted by ID
     *
     * @param Requisition $requisition
     */
    public function deleted(Requisition $requisition)
    {
        if ($requisitionItem = RequisitionItem::where('requisition_id', $requisition->id)) {
            $requisitionItem->delete();
        }
    }
}