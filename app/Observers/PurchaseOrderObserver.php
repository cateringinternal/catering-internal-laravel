<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 26/10/16
 * Time: 19:59
 */

namespace App\Observers;


use App\Http\Models\PurchaseOrder;
use App\Http\Models\PurchaseOrderItem;

class PurchaseOrderObserver
{
    /**
     * @param PurchaseOrder $purchaseOrder
     */
    public function deleted(PurchaseOrder $purchaseOrder)
    {
        if ($purchaseOrderItem = PurchaseOrderItem::where('purchase_order_id', $purchaseOrder->id)) {
            $purchaseOrderItem->delete();
        }
    }
}