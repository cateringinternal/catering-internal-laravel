<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 24/10/16
 * Time: 2:29
 */

namespace App\Observers;


use App\Http\Models\IngredientRecipe;
use App\Http\Models\Recipe;

class RecipeObserver
{
    /**
     * delete ingredient recipe when recipe deleted
     *
     * @param Recipe $recipe
     */
    public function deleted(Recipe $recipe)
    {
        if ($ingredientRecipe = IngredientRecipe::where('recipe_id', $recipe->id)) {
            $ingredientRecipe->delete();
        }
    }
}