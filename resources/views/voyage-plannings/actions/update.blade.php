@include('header')
        <!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Voyage Planning
                    </header>
                    <div class="panel-body">
                        @if (count($errors) > 0)
                            <div class="alert alert-block alert-danger fade in">
                                <button data-dismiss="alert" class="close close-sm" type="button">
                                    <i class="fa fa-times"></i>
                                </button>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="form">
                            <form class="cmxform form-horizontal tasi-form" method="post" action="{{route('voyage-planning.update', ['id' => $voyage_planning->id])}}">
                                <input type="hidden" name="_method" value="PUT">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Departure</label>
                                    <div class="col-lg-10">
                                        <input class="form-control" type="date" name="departure" value="{{date_format(date_create($voyage_planning->departure), 'Y-m-d')}}"/>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Arrival</label>
                                    <div class="col-lg-10">
                                        <input class="form-control" type="date" name="arrival" value="{{date_format(date_create($voyage_planning->arrival), 'Y-m-d')}}"/>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Safety Factor</label>
                                    <div class="col-lg-10">
                                        <input class="form-control" type="text" name="safety_factor" value="{{ $voyage_planning->safety_factor }}"/>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Crew Qty</label>
                                    <div class="col-lg-10">
                                        <input class="form-control" type="text" name="crew_qty" value="{{ $voyage_planning->crew_qty }}"/>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Route</label>
                                    <div class="col-lg-10">
                                        <select class="form-control" name="route_id">
                                            @foreach($routes as $route)
                                                <option value="{{$route->id}}" @if($voyage_planning->route_id == $route->id) selected @endif>{{$route->eta}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Cruise</label>
                                    <div class="col-lg-10">
                                        <select class="form-control" name="cruise_id">
                                            @foreach($cruises as $cruise)
                                                <option value="{{$cruise->id}}" @if($voyage_planning->cruise_id == $cruise->id) selected @endif>{{$cruise->cruise_type}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">
                                        <input class="btn btn-danger" type="submit" name="Submit"/>
                                        <a href="{{route('voyage-planning')}}" class="btn btn-info">Cancel</a>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                </section>
            </div>
        </div>
    </section>
</section>
@include('footer')