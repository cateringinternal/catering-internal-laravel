@include('header')
        <!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Voyage Planning
                    </header>
                    <div class="panel-body">
                        <div class="adv-table">
                            @if (session('success'))
                                <div class="alert alert-block alert-success fade in">
                                    <button data-dismiss="alert" class="close close-sm" type="button">
                                        <i class="fa fa-times"></i>
                                    </button>
                                    <strong>{{session('success')}}</strong>
                                </div>
                            @endif
                            <div class="btn-group pull-right">
                                <button class="btn dropdown-toggle" data-toggle="dropdown">Action <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="{{route('voyage-planning.create')}}">Tambah Data</a></li>
                                </ul>
                            </div>
                            <table  class="display table table-bordered table-striped" id="example">
                                <thead>
                                <th>Code</th>
                                <th>Keberangkatan</th>
                                <th>Kedatangan</th>
                                <th>Safety Factor</th>
                                <th>Jumlah Crew</th>
                                <th>Route</th>
                                <th>Cruise</th>
                                <th>Action</th>
                                </thead>
                                <tbody>
                                @foreach($voyage_plannings as $voyage_planning)
                                    <tr>
                                        <td>{{$voyage_planning->code}}</td>
                                        <td>
                                            {{date_format(date_create($voyage_planning->departure), 'd-m-Y')}}
                                        </td>
                                        <td>
                                            {{date_format(date_create($voyage_planning->arrival), 'd-m-Y')}}
                                        </td>
                                        <td>{{$voyage_planning->safety_factor}}</td>
                                        <td>{{$voyage_planning->crew_qty}}</td>
                                        <td>
                                            @foreach($routes as $route)
                                                @if($route->id == $voyage_planning->route_id)
                                                    {{$route->eta}}
                                                @endif
                                            @endforeach
                                        </td>
                                        <td>
                                            @foreach($cruises as $cruise)
                                                @if($cruise->id == $voyage_planning->cruise_id)
                                                    {{$cruise->cruise_type}}
                                                @endif
                                            @endforeach
                                        </td>
                                        <td>
                                            <a class="btn btn-success" href="{{route('voyage-planning.update', ['id' => encrypt($voyage_planning->id)])}}">Update</a>
                                            <a class="btn btn-danger" href="{{route('voyage-planning.delete', ['id' => encrypt($voyage_planning->id)])}}">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>
<!--main content end-->
@include('footer')