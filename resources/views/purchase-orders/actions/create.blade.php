@include('header')
        <!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Purchase Order
                    </header>
                    <div class="panel-body">
                        @if (count($errors) > 0)
                            <div class="alert alert-block alert-danger fade in">
                                <button data-dismiss="alert" class="close close-sm" type="button">
                                    <i class="fa fa-times"></i>
                                </button>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="form">
                            <form class="cmxform form-horizontal tasi-form" ng-controller="PurchaseOrderFormController" ng-init="init({{json_encode($requisitions)}},{{json_encode($vendors)}}, {{json_encode($ingredients)}},{{json_encode($uoms)}}, {{json_encode($requisitionItems)}}, '')" method="post" action="{{route('purchase-order.create')}}">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="form-group">
                                    <label class="control-label col-lg-2">Requisition</label>
                                    <div class="col-lg-10">
                                        <select class="form-control" ng-model="requisition" ng-change="setPurchaseOrderItem()" ng-options="requisition.code for requisition in requisitions">
                                            <option value="">-- Pilih Requisition --</option>
                                        </select>
                                        <input class="form-control" type="hidden" name="requisition" ng-value="requisition.id"/>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Date</label>
                                    <div class="col-lg-10">
                                        <input class="form-control" type="date" name="date" value="{{ old('date') }}"/>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="ccomment" class="control-label col-lg-2">Total Amount</label>
                                    <div class="col-lg-10">
                                        <input class="form-control" type="number" name="total_amount" value="{{ old('total_amount') }}"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-lg-2">Vendor</label>
                                    <div class="col-lg-10">
                                        <select class="form-control" ng-model="vendor" name="vendor" ng-options="vendor.name for vendor in vendors">
                                            <option value="">-- Pilih Vendor --</option>
                                        </select>
                                        <input class="form-control" type="hidden" name="vendor" ng-value="vendor.id"/>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Payment Method</label>
                                    <div class="col-lg-10">
                                        <input class="form-control" type="text" name="payment_method" value="{{ old('payment_method') }}"/>
                                    </div>
                                </div>
                                <table class="display table table-bordered">
                                    <thead>
                                    <th>Bahan</th>
                                    <th>Jumlah</th>
                                    <th>Satuan</th>
                                    <th>Harga</th>
                                    </thead>
                                    <tbody>
                                    <tr ng-repeat="purchaseOrderItem in purchaseOrderItems">
                                        <td>
                                            <% purchaseOrderItem.ingredient.name %>
                                            <input class="form-control" type="hidden" name="purchaseOrderItem[<%$index%>][ingredient]" value="<% purchaseOrderItem.ingredient.id %>"/>
                                        </td>
                                        <td>
                                            <% purchaseOrderItem.quantity %>
                                            <input class="form-control" type="hidden" name="purchaseOrderItem[<%$index%>][quantity]" value="<% purchaseOrderItem.quantity %>"/>
                                        </td>
                                        <td>
                                            <% purchaseOrderItem.uom.uom %>
                                            <input class="form-control" type="hidden" name="purchaseOrderItem[<%$index%>][uom]" value="<% purchaseOrderItem.uom.id %>"/>
                                        </td>
                                        <td>
                                            <% purchaseOrderItem.price %>
                                            <input class="form-control" type="hidden" name="purchaseOrderItem[<%$index%>][price]" value="<% purchaseOrderItem.price %>"/>
                                        </td>
                                    </tr>
                                    <tr ng-if="purchaseOrderItems.length !== 0">
                                         <td colspan="3">Total</td>
                                        <td><% totalPrice %></td>
                                    </tr>
                                    </tbody>
                                </table>
                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">
                                        <input class="btn btn-danger" type="submit" name="submit"/>
                                        <a href="{{route('purchase-order')}}" class="btn btn-info">Cancel</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </section>
</section>
@include('footer')