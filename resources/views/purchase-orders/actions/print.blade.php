<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<head>
    <title>Print Purchase Order</title>
</head>
<body>
<h3>Purchase Order</h3>
<table border="0">
    <tr>
        <td>Date</td>
        <td>Total Amount</td>
        <td>Number</td>
        <td>Issued By</td>
        <td>Vendor</td>
        <td>Payment Method</td>
    </tr>
    @if(!empty($purchase_order))
        <tr>
            <td>{{date_format(date_create($purchase_order->date), 'd-m-Y')}}</td>
            <td>{{$purchase_order->total_amount}}</td>
            <td>{{$purchase_order->code}}</td>
            <td>{{$purchase_order->issued_by}}</td>
            <td>{{$purchase_order->vendor}}</td>
            <td>{{$purchase_order->payment_method}}</td>
        </tr>
    @endif
</table>
<br>
<h3>Item</h3>
<table border="0">
    <tr>
        <td>Bahan</td>
        <td>Qty</td>
        <td>Satuan</td>
        <td>Harga</td>
    </tr>
    @if(!empty($purchase_order_items))
        @foreach($purchase_order_items as $purchase_order_item)
            <tr>
                @foreach($ingredients as $ingredient)
                    @if($ingredient->id == $purchase_order_item->ingredient_id)
                        <td>{{$ingredient->name}}</td>
                    @endif
                @endforeach
                <td>{{$purchase_order_item->quantity}}</td>
                @foreach($uoms as $uom)
                    @if($uom->id == $purchase_order_item->uom_id)
                        <td>{{$uom->uom}}</td>
                    @endif
                @endforeach
                <td>{{$purchase_order_item->price}}</td>
            </tr>
        @endforeach
    @endif
</table>
</body>
</html>