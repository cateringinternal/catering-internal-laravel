@include('header')
        <!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Purchase Order
                    </header>
                    <div class="panel-body">
                        <div class="adv-table">
                            @if (session('success'))
                                <div class="alert alert-block alert-success fade in">
                                    <button data-dismiss="alert" class="close close-sm" type="button">
                                        <i class="fa fa-times"></i>
                                    </button>
                                    <strong>{{session('success')}}</strong>
                                </div>
                            @endif
                            <div class="btn-group pull-right">
                                <button class="btn dropdown-toggle" data-toggle="dropdown">Action <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="{{route('purchase-order.create')}}">Tambah Data</a></li>
                                </ul>
                            </div>
                            <table  class="display table table-bordered table-striped" id="example">
                                <thead>
                                <th>Code</th>
                                <th>Requisition</th>
                                <th>Date</th>
                                <th>Total Amount</th>
                                <th>Issued By</th>
                                <th>Vendor</th>
                                <th>Payment Method</th>
                                <th>Action</th>
                                </thead>
                                <tbody>
                                @foreach($purchase_orders as $purchase_order)
                                    <tr>
                                        <td>{{$purchase_order->code}}</td>
                                        <td>
                                            @foreach($requisitions as $requisition)
                                                @if($requisition->id == $purchase_order->requisition_id)
                                                    {{$requisition->code}}
                                                @endif
                                            @endforeach
                                        </td>
                                        <td>{{date_format(date_create($purchase_order->date), 'd-m-Y')}}</td>
                                        <td>
                                            {{$purchase_order->total_amount}}
                                        </td>
                                        <td>
                                            {{$purchase_order->issued_by}}
                                        </td>
                                        <td>
                                            @foreach($vendors as $vendor)
                                                @if($vendor->id == $purchase_order->vendor_id)
                                                    {{$vendor->name}}
                                                @endif
                                            @endforeach
                                        </td>
                                        <td>
                                            {{$purchase_order->payment_method}}
                                        </td>
                                        <td>
                                            <a class="btn btn-info" href="{{route('purchase-order.print', ['id' => encrypt($purchase_order->id)])}}">Print</a>
                                            <a class="btn btn-success" href="{{route('purchase-order.update', ['id' => encrypt($purchase_order->id)])}}">Update</a>
                                            <a class="btn btn-danger" href="{{route('purchase-order.delete', ['id' => encrypt($purchase_order->id)])}}">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>
<!--main content end-->
@include('footer')