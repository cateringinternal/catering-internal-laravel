@include('header')
        <!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Employee
                    </header>
                    <div class="panel-body">
                        <div class="adv-table">
                            @if (session('success'))
                                <div class="alert alert-block alert-success fade in">
                                    <button data-dismiss="alert" class="close close-sm" type="button">
                                        <i class="fa fa-times"></i>
                                    </button>
                                    <strong>{{session('success')}}</strong>
                                </div>
                            @endif
                            <div class="btn-group pull-right">
                                <button class="btn dropdown-toggle" data-toggle="dropdown">Action <i
                                            class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="{{route('employee.create')}}">Tambah Data</a></li>
                                </ul>
                            </div>
                            <table cellpadding="0" cellspacing="0" class="display table table-bordered table-striped" id="hidden-table-info">
                                <thead>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>User</th>
                                <th>Action</th>
                                </thead>
                                <tbody>
                                @foreach($employees as $employee)
                                    <tr>
                                        <td>
                                            {{$employee->name}}
                                        </td>
                                        <td>{{$employee->email}}</td>
                                        <td>{{$employee->phone}}</td>
                                        <td>
                                            @foreach($users as $user)
                                                    @if($user->id == $employee->user_id)
                                                        {{$user->username}}
                                                    @endif
                                            @endforeach
                                        </td>
                                        <td>
                                            <a class="btn btn-success"
                                               href="{{route('employee.update', ['id' => encrypt($employee->id)])}}">Update</a>
                                            <a class="btn btn-danger"
                                               href="{{route('employee.delete', ['id' => encrypt($employee->id)])}}">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>
<!--main content end-->
@include('footer')