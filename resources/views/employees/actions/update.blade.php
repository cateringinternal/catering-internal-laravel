@include('header')
        <!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Employee
                    </header>
                    <div class="panel-body">
                        @if (count($errors) > 0)
                            <div class="alert alert-block alert-danger fade in">
                                <button data-dismiss="alert" class="close close-sm" type="button">
                                    <i class="fa fa-times"></i>
                                </button>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="form">
                            <form class="cmxform form-horizontal tasi-form" method="post" action="{{route('employee.update', ['id' => $employee->id])}}">
                                <input type="hidden" name="_method" value="PUT">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Name</label>
                                    <div class="col-lg-10">
                                        <input type="text" class="form-control" name="name" value="{{ $employee->name }}"/>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Email</label>
                                    <div class="col-lg-10">
                                        <input type="text" class="form-control" name="email" value="{{ $employee->email }}"/>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Phone</label>
                                    <div class="col-lg-10">
                                        <input type="number" class="form-control" name="phone" value="{{ $employee->phone }}"/>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">No. SIJIL</label>
                                    <div class="col-lg-10">
                                        <input type="number" class="form-control" name="sijil_number" value="{{ $employee->sijil_number }}"/>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">No. NRP</label>
                                    <div class="col-lg-10">
                                        <input type="number" class="form-control" name="nrp_number" value="{{ $employee->nrp_number }}"/>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Sertifikat</label>
                                    <div class="col-lg-10">
                                        <input type="text" class="form-control" name="certificate" value="{{ $employee->certificate }}"/>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">No. BK Crew</label>
                                    <div class="col-lg-10">
                                        <input type="text" class="form-control" name="bk_number_crew" value="{{ $employee->bk_number_crew }}"/>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Tanggal Efektif</label>
                                    <div class="col-lg-10">
                                        <input type="date" class="form-control" name="effective_date" value="{{ $employee->effective_date }}"/>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">User</label>
                                    <div class="col-lg-10">
                                        <select class="form-control" name="user">
                                            @foreach($users as $user)
                                                <option value="{{$user->id}}" @if($user->id == $employee->user_id) selected @endif>{{$user->username}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">
                                        <input class="btn btn-danger" type="submit" name="Submit"/>
                                        <a href="{{route('employee')}}" class="btn btn-info">Cancel</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </section>
</section>
@include('footer')