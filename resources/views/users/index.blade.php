@include('header')
        <!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        User
                    </header>
                    <div class="panel-body">
                        <div class="adv-table">
                            @if (session('success'))
                                <div class="alert alert-block alert-success fade in">
                                    <button data-dismiss="alert" class="close close-sm" type="button">
                                        <i class="fa fa-times"></i>
                                    </button>
                                    <strong>{{session('success')}}</strong>
                                </div>
                            @endif
                            <div class="btn-group pull-right">
                                <button class="btn dropdown-toggle" data-toggle="dropdown">Action <i
                                            class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="{{route('user.create')}}">Tambah Data</a></li>
                                </ul>
                            </div>
                            <table class="display table table-bordered table-striped" id="example">
                                <thead>
                                <th>Username</th>
                                <th>Role</th>
                                <th>Dibuat Tanggal</th>
                                <th>Status</th>
                                <th>Action</th>
                                </thead>
                                <tbody>
                                @foreach($users as $user)
                                    <tr>
                                        <td>
                                            {{$user->username}}
                                        </td>
                                        <td>
                                            @foreach($role_users as $role_user)
                                                @foreach($roles as $role)
                                                    @if($user->id == $role_user->user_id && $role->id == $role_user->role_id)
                                                        {{$role->name}}
                                                    @endif
                                                @endforeach
                                            @endforeach
                                        </td>
                                        <td>{{date_format(date_create($user->created_at),'d-m-Y')}}</td>
                                        <td>@if($user->is_active == 1)
                                                Aktif
                                        @else
                                                Tidak Aktif
                                        @endif</td>
                                        <td>
                                            <a class="btn btn-warning"
                                               href="{{route('user.status', ['status' => $user->is_active, 'id' => encrypt($user->id)])}}">@if($user->is_active == 0)
                                                    Aktifkan @else Non Aktifkan @endif</a>
                                            <a class="btn btn-success"
                                               href="{{route('user.update', ['id' => encrypt($user->id)])}}">Update</a>
                                            <a class="btn btn-danger"
                                               href="{{route('user.delete', ['id' => encrypt($user->id)])}}">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>
<!--main content end-->
@include('footer')