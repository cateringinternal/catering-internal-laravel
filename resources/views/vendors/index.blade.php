@include('header')
        <!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Vendor
                    </header>
                    <div class="panel-body">
                        <div class="adv-table">
                            @if (session('success'))
                                <div class="alert alert-block alert-success fade in">
                                    <button data-dismiss="alert" class="close close-sm" type="button">
                                        <i class="fa fa-times"></i>
                                    </button>
                                    <strong>{{session('success')}}</strong>
                                </div>
                            @endif
                            <div class="btn-group pull-right">
                                <button class="btn dropdown-toggle" data-toggle="dropdown">Action <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="{{route('vendor.create')}}">Tambah Data</a></li>
                                </ul>
                            </div>
                            <table  class="display table table-bordered table-striped" id="example">
                                <thead>
                                <th>Name</th>
                                <th>Payment Method</th>
                                <th>Bank Account</th>
                                <th>Address</th>
                                <th>City</th>
                                <th>Zip Code</th>
                                <th>Phone</th>
                                <th>Contact Name</th>
                                <th>Action</th>
                                </thead>
                                <tbody>
                                @foreach($vendors as $vendor)
                                    <tr>
                                        <td>{{$vendor->name}}</td>
                                        <td>{{$vendor->payment_method}}</td>
                                        <td>{{$vendor->bank_account}}</td>
                                        <td>{{$vendor->address}}</td>
                                        <td>{{$vendor->city}}</td>
                                        <td>{{$vendor->zip_code}}</td>
                                        <td>{{$vendor->phone}}</td>
                                        <td>{{$vendor->contact_name}}</td>
                                        <td>
                                            <a class="btn btn-success" href="{{route('vendor.update', ['id' => encrypt($vendor->id)])}}">Update</a>
                                            <a class="btn btn-danger" href="{{route('vendor.delete', ['id' => encrypt($vendor->id)])}}">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>
<!--main content end-->
@include('footer')