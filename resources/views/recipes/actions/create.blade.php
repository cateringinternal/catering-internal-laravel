@include('header')
        <!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Recipe
                    </header>
                    <div class="panel-body">
                        @if (count($errors) > 0)
                            <div class="alert alert-block alert-danger fade in">
                                <button data-dismiss="alert" class="close close-sm" type="button">
                                    <i class="fa fa-times"></i>
                                </button>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="form">
                            <form class="cmxform form-horizontal tasi-form" ng-controller="RecipeFormController" ng-init="init({{json_encode($ingredients)}}, {{json_encode($uoms)}}, '')" method="post" action="{{route('recipe.create')}}" enctype="multipart/form-data">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Title</label>
                                    <div class="col-lg-10">
                                        <input class="form-control" type="text" name="title" value="{{ old('title') }}"/>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Picture</label>
                                    <div class="col-lg-10">
                                        <input class="form-control" type="file" name="picture" />
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Date</label>
                                    <div class="col-lg-10">
                                        <input class="form-control" type="date" name="date" value="{{ old('date') }}"/>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="ccomment" class="control-label col-lg-2">Description</label>
                                    <div class="col-lg-10">
                                        <textarea class="form-control" style="resize: none" name="description"></textarea>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Direction</label>
                                    <div class="col-lg-10">
                                        <input class="form-control" type="text" name="direction" value="{{ old('direction') }}"/>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Menu Type</label>
                                    <div class="col-lg-10">
                                        <input class="form-control" type="text" name="menu_type" value="{{ old('menu_type') }}"/>
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <label class="control-label col-md-4">Bahan</label>
                                    <div class="col-md-8">
                                        <select class="form-control" ng-model="bahan" ng-options="ingredient.name for ingredient in ingredients"></select>
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <label class="control-label col-md-4">Jumlah</label>
                                    <div class="col-md-8">
                                        <input class="form-control" type="number" ng-model="jumlah" value="{{ old('jumlah') }}"/>
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <label class="control-label col-md-4">Satuan</label>
                                    <div class="col-md-8">
                                        <select class="form-control" ng-model="satuan" ng-options="uom.uom for uom in uoms"></select>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-4"></label>
                                    <div class="col-md-8">
                                        <a href="" class="btn btn-success" ng-click="addIngredientRecipe()">Add</a>
                                    </div>
                                </div>
                                <table class="display table table-bordered">
                                    <thead>
                                    <th>Bahan</th>
                                    <th>Jumlah</th>
                                    <th>Satuan</th>
                                    <th>Action</th>
                                    </thead>
                                    <tbody>
                                    <tr ng-repeat="ingredientRecipe in ingredientRecipes">
                                        <td>
                                            <% ingredientRecipe.bahan.name %>
                                            <input class="form-control" type="hidden" name="ingredientRecipe[<%$index%>][bahan]" value="<% ingredientRecipe.bahan.id %>"/>
                                        </td>
                                        <td>
                                            <% ingredientRecipe.jumlah %>
                                            <input class="form-control" type="hidden" name="ingredientRecipe[<%$index%>][jumlah]" value="<% ingredientRecipe.jumlah %>"/>
                                        </td>
                                        <td>
                                            <% ingredientRecipe.satuan.uom %>
                                            <input class="form-control" type="hidden" name="ingredientRecipe[<%$index%>][satuan]" value="<% ingredientRecipe.satuan.id %>"/>
                                        </td>
                                        <td>
                                            <a href="" class="btn btn-success" ng-click="removeIngredientRecipe($index)">Remove</a>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">
                                        <input class="btn btn-danger" type="submit" name="submit"/>
                                        <a href="{{route('recipe')}}" class="btn btn-info">Cancel</a>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                </section>
            </div>
        </div>
    </section>
</section>
@include('footer')