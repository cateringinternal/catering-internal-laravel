@include('header')
        <!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Recipe
                    </header>
                    <div class="panel-body">
                        <div class="adv-table">
                            @if (session('success'))
                                <div class="alert alert-block alert-success fade in">
                                    <button data-dismiss="alert" class="close close-sm" type="button">
                                        <i class="fa fa-times"></i>
                                    </button>
                                    <strong>{{session('success')}}</strong>
                                </div>
                            @endif
                            <div class="btn-group pull-right">
                                <button class="btn dropdown-toggle" data-toggle="dropdown">Action <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="{{route('recipe.create')}}">Tambah Data</a></li>
                                </ul>
                            </div>
                            <table  class="display table table-bordered table-striped" id="example">
                                <thead>
                                <th>Judul</th>
                                <th>Gambar</th>
                                <th>Deskripsi</th>
                                <th>Direction</th>
                                <th>Tipe Menu</th>
                                <th>Action</th>
                                </thead>
                                <tbody>
                                @foreach($recipes as $recipe)
                                    <tr>
                                        <td>{{$recipe->title}}</td>
                                        <td>
                                            @if(!empty($recipe->picture) || $recipe->picture != NULL)
                                                <img style="width:20%" src="/uploads{{$recipe->picture}}">
                                            @else
                                                No Image
                                            @endif
                                        </td>
                                        <td>{{$recipe->description}}</td>
                                        <td>{{$recipe->direction}}</td>
                                        <td>{{$recipe->menu_type}}</td>
                                        <td>
                                            <a class="btn btn-success" href="{{route('recipe.update', ['id' => encrypt($recipe->id)])}}">Update</a>
                                            <a class="btn btn-danger" href="{{route('recipe.delete', ['id' => encrypt($recipe->id)])}}">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>
<!--main content end-->
@include('footer')