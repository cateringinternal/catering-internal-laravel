@include('header')
        <!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Requisition Item
                    </header>
                    <div class="panel-body">
                        @if (count($errors) > 0)
                            <div class="alert alert-block alert-danger fade in">
                                <button data-dismiss="alert" class="close close-sm" type="button">
                                    <i class="fa fa-times"></i>
                                </button>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="form">
                            <form class="cmxform form-horizontal tasi-form" method="post"
                                  action="{{route('requisition-item.update', ['id' => $requisition_item->id])}}">
                                <input type="hidden" name="_method" value="PUT">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Ingredient</label>
                                    <div class="col-lg-10">
                                        <select class="form-control" name="ingredient">
                                            @foreach($ingredients as $ingredient)
                                                <option value="{{$ingredient->id}}"
                                                        @if($ingredient->id == $requisition_item->ingredient_id) selected @endif>{{$ingredient->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Quantity</label>
                                    <div class="col-lg-10">
                                        <input class="form-control" type="number" name="quantity"
                                               value="{{ $requisition_item->quantity }}"/>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Price</label>
                                    <div class="col-lg-10">
                                        <input class="form-control" type="number" name="price"
                                               value="{{ $requisition_item->price }}"/>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="ccomment" class="control-label col-lg-2">Requisition</label>
                                    <div class="col-lg-10">
                                        <select class="form-control" name="requisition">
                                            @foreach($requisitions as $requisition)
                                                <option value="{{$requisition->id}}"
                                                        @if($requisition->id == $requisition_item->requisition_id) selected @endif>{{$requisition->number}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Unit</label>
                                    <div class="col-lg-10">
                                        <input class="form-control" type="text" name="unit"
                                               value="{{ $requisition_item->unit }}"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">
                                        <input class="btn btn-danger" type="submit" name="Submit"/>
                                        <a href="{{route('requisition-item')}}" class="btn btn-info">Cancel</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </section>
</section>
@include('footer')