@include('header')
        <!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Requisition Item
                    </header>
                    <div class="panel-body">
                        <div class="adv-table">
                            @if (session('success'))
                                <div class="alert alert-block alert-success fade in">
                                    <button data-dismiss="alert" class="close close-sm" type="button">
                                        <i class="fa fa-times"></i>
                                    </button>
                                    <strong>{{session('success')}}</strong>
                                </div>
                            @endif
                            <div class="btn-group pull-right">
                                <button class="btn dropdown-toggle" data-toggle="dropdown">Action <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="{{route('requisition-item.create')}}">Tambah Data</a></li>
                                </ul>
                            </div>
                            <table  class="display table table-bordered table-striped" id="example">
                                <thead>
                                <th>Ingredient</th>
                                <th>Quantity</th>
                                <th>Price</th>
                                <th>Requisition</th>
                                <th>Unit</th>
                                <th>Action</th>
                                </thead>
                                <tbody>
                                @foreach($requisition_items as $requisition_item)
                                    <tr>
                                        <td>
                                            @foreach($ingredients as $ingredient)
                                                @if($ingredient->id == $requisition_item->ingredient_id)
                                                    {{$ingredient->name}}
                                                @endif
                                            @endforeach
                                        </td>
                                        <td>
                                            {{$requisition_item->quantity}}
                                        </td>
                                        <td>
                                            {{$requisition_item->price}}
                                        </td>
                                        <td>
                                            @foreach($requisitions as $requisition)
                                                @if($requisition->id == $requisition_item->requisition_id)
                                                    {{$requisition->number}}
                                                @endif
                                            @endforeach
                                        </td>
                                        <td>
                                            {{$requisition_item->unit}}
                                        </td>
                                        <td>
                                            <a class="btn btn-success" href="{{route('requisition-item.update', ['id' => encrypt($requisition_item->id)])}}">Update</a>
                                            <a class="btn btn-danger" href="{{route('requisition-item.delete', ['id' => encrypt($requisition_item->id)])}}">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>
<!--main content end-->
@include('footer')