@include('header')
        <!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Requisition
                    </header>
                    <div class="panel-body">
                        @if (count($errors) > 0)
                            <div class="alert alert-block alert-danger fade in">
                                <button data-dismiss="alert" class="close close-sm" type="button">
                                    <i class="fa fa-times"></i>
                                </button>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="form">
                            <form class="cmxform form-horizontal tasi-form" ng-controller="RequisitionFormController" method="post" ng-init="init({{json_encode($ingredients)}}, {{json_encode($uoms)}}, '')" action="{{route('requisition.create')}}">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Voyage Planning</label>
                                    <div class="col-lg-10">
                                        <select class="form-control" name="voyage_planning">
                                            @foreach($voyage_plannings as $voyage_planning)
                                                <option value="{{$voyage_planning->id}}">{{$voyage_planning->code}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Date</label>
                                    <div class="col-lg-10">
                                        <input class="form-control" type="date" name="date" value="{{ old('date') }}"/>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="ccomment" class="control-label col-lg-2">Total Amount</label>
                                    <div class="col-lg-10">
                                        <input class="form-control" type="number" name="total_amount" value="{{ old('total_amount') }}"/>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Vendor</label>
                                    <div class="col-lg-10">
                                        <select class="form-control" name="vendor">
                                            @foreach($vendors as $vendor)
                                                <option value="{{$vendor->id}}">{{$vendor->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <label class="control-label col-md-4">Bahan</label>
                                    <div class="col-md-8">
                                        <select class="form-control" ng-model="ingredient" ng-options="ingredient.name for ingredient in ingredients"></select>
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <label class="control-label col-md-4">Harga</label>
                                    <div class="col-md-8">
                                        <input class="form-control" type="number" ng-model="price"/>
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <label class="control-label col-md-4">Jumlah</label>
                                    <div class="col-md-8">
                                        <input class="form-control" type="number" ng-model="quantity"/>
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <label class="control-label col-md-4">Satuan</label>
                                    <div class="col-md-8">
                                        <select class="form-control" ng-model="uom" ng-options="uom.uom for uom in uoms"></select>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-4"></label>
                                    <div class="col-md-8">
                                        <a href="" class="btn btn-success" ng-click="addRequisitionItem()">Add</a>
                                    </div>
                                </div>
                                <table class="display table table-bordered">
                                    <thead>
                                    <th>Bahan</th>
                                    <th>Jumlah</th>
                                    <th>Satuan</th>
                                    <th>Harga</th>
                                    <th>Action</th>
                                    </thead>
                                    <tbody>
                                    <tr ng-repeat="requisitionItem in requisitionItems">
                                        <td>
                                            <% requisitionItem.ingredient.name %>
                                            <input class="form-control" type="hidden" name="requisitionItem[<%$index%>][ingredient]" value="<% requisitionItem.ingredient.id %>"/>
                                        </td>
                                        <td>
                                            <% requisitionItem.quantity %>
                                            <input class="form-control" type="hidden" name="requisitionItem[<%$index%>][quantity]" value="<% requisitionItem.quantity %>"/>
                                        </td>
                                        <td>
                                            <% requisitionItem.uom.uom %>
                                            <input class="form-control" type="hidden" name="requisitionItem[<%$index%>][uom]" value="<% requisitionItem.uom.id %>"/>
                                        </td>
                                        <td>
                                            <% requisitionItem.price %>
                                            <input class="form-control" type="hidden" name="requisitionItem[<%$index%>][price]" value="<% requisitionItem.price %>"/>
                                        </td>
                                        <td>
                                            <a href="" class="btn btn-success" ng-click="removeRequisitionItem($index)">Remove</a>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">
                                        <input class="btn btn-danger" type="submit" name="submit"/>
                                        <a href="{{route('requisition')}}" class="btn btn-info">Cancel</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </section>
</section>
@include('footer')