@include('header')
        <!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Requisition
                    </header>
                    <div class="panel-body">
                        <div class="adv-table">
                            @if (session('success'))
                                <div class="alert alert-block alert-success fade in">
                                    <button data-dismiss="alert" class="close close-sm" type="button">
                                        <i class="fa fa-times"></i>
                                    </button>
                                    <strong>{{session('success')}}</strong>
                                </div>
                            @endif
                            <div class="btn-group pull-right">
                                <button class="btn dropdown-toggle" data-toggle="dropdown">Action <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="{{route('requisition.create')}}">Tambah Data</a></li>
                                </ul>
                            </div>
                            <table  class="display table table-bordered table-striped" id="example">
                                <thead>
                                <th>Code</th>
                                <th>Voyage Planning</th>
                                <th>Date</th>
                                <th>Total Amount</th>
                                <th>Vendor</th>
                                <th>Requested By</th>
                                <th>Approval Status</th>
                                <th>Approval Date</th>
                                <th>Approved By</th>
                                <th>Action</th>
                                </thead>
                                <tbody>
                                @foreach($requisitions as $requisition)
                                    <tr>
                                        <td>
                                            {{$requisition->code}}
                                        </td>
                                        <td>
                                            @foreach($voyage_plannings as $voyage_planning)
                                                @if($voyage_planning->id == $requisition->voyage_planning_id)
                                                    {{$voyage_planning->crew_qty}}
                                                @endif
                                            @endforeach
                                        </td>
                                        <td>{{date_format(date_create($requisition->date), 'd-m-Y')}}</td>
                                        <td>
                                            {{$requisition->total_amount}}
                                        </td>
                                        <td>
                                            @foreach($vendors as $vendor)
                                                @if($vendor->id == $requisition->vendor_id)
                                                    {{$vendor->name}}
                                                @endif
                                            @endforeach
                                        </td>
                                        <td>
                                            {{$requisition->requested_by}}
                                        </td>
                                        <td>
                                            @if($requisition->approval_status == 0)
                                                Not Approved
                                            @elseif($requisition->approval_status == 1)
                                                Need Revised
                                            @elseif($requisition->approval_status == 2)
                                                Approved
                                            @else
                                                Status Error
                                            @endif
                                        </td>
                                        <td>
                                            @if(!empty($requisition->approval_date))
                                                {{date_format(date_create($requisition->approval_date), 'd-m-Y')}}
                                            @endif

                                        </td>
                                        <td>
                                            {{$requisition->approved_by}}
                                        </td>
                                        <td>
                                            @if($requisition->approval_status != 2)
                                                <a class="btn btn-warning" href="{{route('requisition.status', ['id' => encrypt($requisition->id), 'status' => 1])}}">Revised</a>
                                                <a class="btn btn-info" href="{{route('requisition.status', ['id' => encrypt($requisition->id), 'status' => 2])}}">Approval</a>
                                            @endif
                                            <a class="btn btn-success" href="{{route('requisition.update', ['id' => encrypt($requisition->id)])}}">Update</a>
                                            <a class="btn btn-danger" href="{{route('requisition.delete', ['id' => encrypt($requisition->id)])}}">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>
<!--main content end-->
@include('footer')