@include('header')
        <!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Inventory Out
                    </header>
                    <div class="panel-body">
                        <div class="adv-table">
                            @if (session('success'))
                                <div class="alert alert-block alert-success fade in">
                                    <button data-dismiss="alert" class="close close-sm" type="button">
                                        <i class="fa fa-times"></i>
                                    </button>
                                    <strong>{{session('success')}}</strong>
                                </div>
                            @endif
                            <div class="btn-group pull-right">
                                <button class="btn dropdown-toggle" data-toggle="dropdown">Action <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="{{route('inventory-out.create')}}">Tambah Data</a></li>
                                </ul>
                            </div>
                            <table  class="display table table-bordered table-striped" id="example">
                                <thead>
                                <th>From</th>
                                <th>Quantity</th>
                                <th>Unit</th>
                                <th>Is mutation</th>
                                <th>Request Inventory Out</th>
                                <th>Action</th>
                                </thead>
                                <tbody>
                                @foreach($inventories_out as $inventory_out)
                                    <tr>
                                        <td>
                                            @foreach($inventories as $inventory)
                                                @if($inventory->id == $inventory_out->from)
                                                    {{$inventory->quantity}}
                                                @endif
                                            @endforeach
                                        </td>
                                        <td>{{$inventory_out->quantity}}</td>
                                        <td>{{$inventory_out->unit}}</td>
                                        <td>{{$inventory_out->is_mutation}}</td>
                                        <td>{{$inventory_out->request_inventory_out}}</td>
                                        <td>
                                            <a class="btn btn-success" href="{{route('inventory-out.update', ['id' => encrypt($inventory_out->id)])}}">Update</a>
                                            <a class="btn btn-danger" href="{{route('inventory-out.delete', ['id' => encrypt($inventory_out->id)])}}">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>
<!--main content end-->
@include('footer')