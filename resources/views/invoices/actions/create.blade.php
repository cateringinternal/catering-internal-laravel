@include('header')
        <!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Invoice
                    </header>
                    <div class="panel-body">
                        @if (count($errors) > 0)
                            <div class="alert alert-block alert-danger fade in">
                                <button data-dismiss="alert" class="close close-sm" type="button">
                                    <i class="fa fa-times"></i>
                                </button>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="form">
                            <form class="cmxform form-horizontal tasi-form" ng-controller="InvoiceFormController" ng-init="init({{json_encode($receivings)}}, {{json_encode($vendors)}}, {{json_encode($ingredients)}}, {{json_encode($uoms)}}, {{json_encode($receivingItems)}}, '')" method="post" action="{{route('invoice.create')}}">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Receiving</label>
                                    <div class="col-lg-10">
                                        <select class="form-control" ng-model="receiving" ng-change="setReceivingItem()" ng-options="receiving.code for receiving in receivings">
                                            <option value="">-- Pilih Receiving --</option>
                                        </select>
                                        <input class="form-control" type="hidden" name="receiving" ng-value="receiving.id"/>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Date</label>
                                    <div class="col-lg-10">
                                        <input class="form-control" type="date" name="date" value="{{ old('date') }}"/>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Total Amount</label>
                                    <div class="col-lg-10">
                                        <input class="form-control" type="number" name="total_amount" value="{{ old('total_amount') }}"/>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Vendor</label>
                                    <div class="col-lg-10">
                                        <select class="form-control" name="vendor">
                                            @foreach($vendors as $vendor)
                                                <option value="{{$vendor->id}}">{{$vendor->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Payment Type</label>
                                    <div class="col-lg-10">
                                        <input class="form-control" type="text" name="payment_type" value="{{ old('payment_type') }}"/>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Bank Account</label>
                                    <div class="col-lg-10">
                                        <input class="form-control" type="text" name="bank_account" value="{{ old('bank_account') }}"/>
                                    </div>
                                </div>
                                <table class="display table table-bordered">
                                    <thead>
                                    <th>Bahan</th>
                                    <th>Jumlah</th>
                                    <th>Satuan</th>
                                    <th>Harga</th>
                                    <th>Remark</th>
                                    </thead>
                                    <tbody>
                                    <tr ng-repeat="receivingItem in receivingItems">
                                        <td>
                                            <% receivingItem.ingredient.name %>
                                            <input class="form-control" type="hidden" name="receivingItem[<%$index%>][ingredient]" value="<% receivingItem.ingredient.id %>"/>
                                        </td>
                                        <td>
                                            <% receivingItem.quantity %>
                                            <input class="form-control" type="hidden" name="receivingItem[<%$index%>][quantity]" value="<% receivingItem.quantity %>"/>
                                        </td>
                                        <td>
                                            <% receivingItem.uom.uom %>
                                            <input class="form-control" type="hidden" name="receivingItem[<%$index%>][uom]" value="<% receivingItem.uom.id %>"/>
                                        </td>
                                        <td>
                                            <% receivingItem.price %>
                                            <input class="form-control" type="hidden" name="receivingItem[<%$index%>][price]" value="<% receivingItem.price %>"/>
                                        </td>
                                        <td>
                                            <% receivingItem.remark %>
                                            <input class="form-control" type="hidden" name="receivingItem[<%$index%>][remark]" value="<% receivingItem.remark %>"/>
                                        </td>
                                    </tr>
                                    <tr ng-if="receivingItems.length !== 0">
                                        <td colspan="4">Total</td>
                                        <td><% totalPrice %></td>
                                    </tr>
                                    </tbody>
                                </table>
                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">
                                        <input class="btn btn-danger" type="submit" name="submit"/>
                                        <a href="{{route('invoice')}}" class="btn btn-info">Cancel</a>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                </section>
            </div>
        </div>
    </section>
</section>
@include('footer')