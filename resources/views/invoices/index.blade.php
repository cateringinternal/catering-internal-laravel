@include('header')
        <!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Invoice
                    </header>
                    <div class="panel-body">
                        <div class="adv-table">
                            @if (session('success'))
                                <div class="alert alert-block alert-success fade in">
                                    <button data-dismiss="alert" class="close close-sm" type="button">
                                        <i class="fa fa-times"></i>
                                    </button>
                                    <strong>{{session('success')}}</strong>
                                </div>
                            @endif
                            <div class="btn-group pull-right">
                                <button class="btn dropdown-toggle" data-toggle="dropdown">Action <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="{{route('invoice.create')}}">Tambah Data</a></li>
                                </ul>
                            </div>
                            <table  class="display table table-bordered table-striped" id="example">
                                <thead>
                                <th>Code</th>
                                <th>Date</th>
                                <th>Total Amount</th>
                                <th>Vendor</th>
                                <th>Payment Type</th>
                                <th>Payment Status</th>
                                <th>Bank Account</th>
                                <th>Issued By</th>
                                <th>Receiving</th>
                                <th>Approved By</th>
                                <th>Action</th>
                                </thead>
                                <tbody>
                                @foreach($invoices as $invoice)
                                    <tr>
                                        <td>
                                            {{$invoice->code}}
                                        </td>
                                        <td>
                                            {{date_format(date_create($invoice->date), 'd-m-Y')}}
                                        </td>
                                        <td>
                                            {{$invoice->total_amount}}
                                        </td>
                                        <td>
                                            @foreach($vendors as $vendor)
                                                @if($vendor->id == $invoice->vendor_id)
                                                    {{$vendor->name}}
                                                @endif
                                            @endforeach
                                        </td>
                                        <td>{{$invoice->payment_type}}</td>
                                        <td>{{$invoice->payment_status}}</td>
                                        <td>{{$invoice->bank_account}}</td>
                                        <td>{{$invoice->issued_by}}</td>
                                        <td>
                                            @foreach($receivings as $receiving)
                                                @if($receiving->id == $invoice->receiving_id)
                                                    {{$receiving->code}}
                                                @endif
                                            @endforeach
                                        </td>
                                        <td>{{$invoice->approved_by}}</td>
                                        <td>
                                            <a class="btn btn-success" href="{{route('invoice.update', ['id' => encrypt($invoice->id)])}}">Update</a>
                                            <a class="btn btn-danger" href="{{route('invoice.delete', ['id' => encrypt($invoice->id)])}}">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>
<!--main content end-->
@include('footer')