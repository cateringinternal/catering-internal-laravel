@include('header')
        <!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Purchase Order Item
                    </header>
                    <div class="panel-body">
                        @if (count($errors) > 0)
                            <div class="alert alert-block alert-danger fade in">
                                <button data-dismiss="alert" class="close close-sm" type="button">
                                    <i class="fa fa-times"></i>
                                </button>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="form">
                            <form class="cmxform form-horizontal tasi-form" method="post" action="{{route('purchase-order-item.update', ['id' => $purchase_order_item->id])}}">
                                <input type="hidden" name="_method" value="PUT">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Purchase Order</label>
                                    <div class="col-lg-10">
                                        <select class="form-control" name="purchase_order">
                                            @foreach($purchase_orders as $purchase_order)
                                                <option value="{{$purchase_order->id}}"
                                                        @if($purchase_order->id == $purchase_order_item->purchase_order_id) selected @endif>{{$purchase_order->number}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Ingredient</label>
                                    <div class="col-lg-10">
                                        <select class="form-control" name="ingredient">
                                            @foreach($ingredients as $ingredient)
                                                <option value="{{$ingredient->id}}" @if($ingredient->id == $purchase_order_item->ingredient_id) selected @endif>{{$ingredient->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Quantity</label>
                                    <div class="col-lg-10">
                                        <input class="form-control" type="number" name="quantity" value="{{ $purchase_order_item->quantity }}"/>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="ccomment" class="control-label col-lg-2">Unit</label>
                                    <div class="col-lg-10">
                                        <input class="form-control" type="number" name="unit" value="{{ $purchase_order_item->unit }}"/>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Price</label>
                                    <div class="col-lg-10">
                                        <input class="form-control" type="number" name="price" value="{{ $purchase_order_item->price }}"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">
                                        <input class="btn btn-danger" type="submit" name="Submit"/>
                                        <a href="{{route('purchase-order-item')}}" class="btn btn-info">Cancel</a>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                </section>
            </div>
        </div>
    </section>
</section>
@include('footer')