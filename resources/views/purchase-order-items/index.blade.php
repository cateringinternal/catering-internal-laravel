@include('header')
        <!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Purchase Order Item
                    </header>
                    <div class="panel-body">
                        <div class="adv-table">
                            @if (session('success'))
                                <div class="alert alert-block alert-success fade in">
                                    <button data-dismiss="alert" class="close close-sm" type="button">
                                        <i class="fa fa-times"></i>
                                    </button>
                                    <strong>{{session('success')}}</strong>
                                </div>
                            @endif
                            <div class="btn-group pull-right">
                                <button class="btn dropdown-toggle" data-toggle="dropdown">Action <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="{{route('purchase-order-item.create')}}">Tambah Data</a></li>
                                </ul>
                            </div>
                            <table  class="display table table-bordered table-striped" id="example">
                                <thead>
                                <th>Purchase Order</th>
                                <th>Ingredient</th>
                                <th>Quantity</th>
                                <th>Unit</th>
                                <th>Price</th>
                                <th>Action</th>
                                </thead>
                                <tbody>
                                @foreach($purchase_order_items as $purchase_order_item)
                                    <tr>
                                        <td>
                                            @foreach($purchase_orders as $purchase_order)
                                                @if($purchase_order->id == $purchase_order_item->purchase_order_id)
                                                    {{$purchase_order->number}}
                                                @endif
                                            @endforeach
                                        </td>
                                        <td>
                                            @foreach($ingredients as $ingredient)
                                                @if($ingredient->id == $purchase_order_item->ingredient_id)
                                                    {{$ingredient->name}}
                                                @endif
                                            @endforeach
                                        </td>
                                        <td>
                                            {{$purchase_order_item->quantity}}
                                        </td>
                                        <td>
                                            {{$purchase_order_item->unit}}
                                        </td>
                                        <td>
                                            {{$purchase_order_item->price}}
                                        </td>
                                        <td>
                                            <a class="btn btn-success" href="{{route('purchase-order-item.update', ['id' => encrypt($purchase_order_item->id)])}}">Update</a>
                                            <a class="btn btn-danger" href="{{route('purchase-order-item.delete', ['id' => encrypt($purchase_order_item->id)])}}">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>
<!--main content end-->
@include('footer')