@include('header')
        <!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Ingredient
                    </header>
                    <div class="panel-body">
                        <div class="adv-table">
                            @if (session('success'))
                                <div class="alert alert-block alert-success fade in">
                                    <button data-dismiss="alert" class="close close-sm" type="button">
                                        <i class="fa fa-times"></i>
                                    </button>
                                    <strong>{{session('success')}}</strong>
                                </div>
                            @endif
                            <div class="btn-group pull-right">
                                <button class="btn dropdown-toggle" data-toggle="dropdown">Action <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="{{route('ingredient.create')}}">Tambah Data</a></li>
                                </ul>
                            </div>
                            <table  class="display table table-bordered table-striped" id="example">
                                <thead>
                                <th>Nama</th>
                                <th>Kategori</th>
                                <th>Description</th>
                                <th>Action</th>
                                </thead>
                                <tbody>
                                @foreach($ingredients as $ingredient)
                                    <tr>
                                        <td>
                                            {{$ingredient->name}}
                                        </td>
                                        <td>
                                            @foreach($categories as $category)
                                                @if($category->id == $ingredient->category_id)
                                                    {{$category->category}}
                                                @endif
                                            @endforeach
                                        </td>
                                        <td>{{$ingredient->description}}</td>
                                        <td>
                                            <a class="btn btn-success" href="{{route('ingredient.update', ['id' => encrypt($ingredient->id)])}}">Update</a>
                                            <a class="btn btn-danger" href="{{route('ingredient.delete', ['id' => encrypt($ingredient->id)])}}">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>
<!--main content end-->
@include('footer')