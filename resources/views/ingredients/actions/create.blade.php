@include('header')
        <!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Ingredient
                    </header>
                    <div class="panel-body">
                        @if (count($errors) > 0)
                            <div class="alert alert-block alert-danger fade in">
                                <button data-dismiss="alert" class="close close-sm" type="button">
                                    <i class="fa fa-times"></i>
                                </button>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="form">
                            <form class="cmxform form-horizontal tasi-form" ng-controller="IngredientFormController" method="post" action="{{route('ingredient.create')}}">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Name</label>
                                    <div class="col-lg-10">
                                        <input class="form-control" type="text" ng-model="bahanUtama" name="name" value="{{ old('name') }}"/>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Kategori</label>
                                    <div class="col-lg-10">
                                        <select class="form-control" name="category_id">
                                            @foreach($categories as $category)
                                                <option value="{{$category->id}}">{{$category->category}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Satuan Resep</label>
                                    <div class="col-lg-10">
                                        <select class="form-control" name="satuan_resep">
                                            @foreach($uoms as $uom)
                                                <option value="{{$uom->id}}">{{$uom->uom}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Satuan Pembelian</label>
                                    <div class="col-lg-10">
                                        <select class="form-control" name="satuan_pembelian">
                                            @foreach($uoms as $uom)
                                                <option value="{{$uom->id}}">{{$uom->uom}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Description</label>
                                    <div class="col-lg-10">
                                        <textarea class="form-control" style="resize: none" name="description"></textarea>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-4">Variant Bahan</label>
                                    <div class="col-md-8">
                                        <input class="form-control" type="text" ng-model="variantName" name="variant_bahan" value="{{ old('variant_bahan') }}"/>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-4">Description</label>
                                    <div class="col-md-8">
                                        <input class="form-control" type="text" ng-model="description" name="variant_description" value="{{ old('variant_description') }}"/>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-4"></label>
                                    <div class="col-md-8">
                                        <a href="" class="btn btn-success" ng-click="addVariant()">Add</a>
                                    </div>
                                </div>
                                <table class="display table table-bordered">
                                    <thead>
                                    <th>Nama</th>
                                    <th>Deskripsi</th>
                                    <th>Bahan Utama</th>
                                    <th>Action</th>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat="variant in variants">
                                            <td>
                                                <% variant.variantName %>
                                                <input class="form-control" type="hidden" name="variant[<%$index%>][variantName]" value="<% variant.variantName %>"/>
                                            </td>
                                            <td>
                                                <% variant.description %>
                                                <input class="form-control" type="hidden" name="variant[<%$index%>][description]" value="<% variant.description %>"/>
                                            </td>
                                            <td>
                                                <% variant.bahanUtama %>
                                            </td>
                                            <td>
                                                <a href="" class="btn btn-success" ng-click="removeVariant($index)">Remove</a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">
                                        <input class="btn btn-danger" type="submit" name="submit"/>
                                        <a href="{{route('ingredient')}}" class="btn btn-info">Cancel</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </section>
</section>
@include('footer')