@include('header')
        <!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Waste
                    </header>
                    <div class="panel-body">
                        <div class="adv-table">
                            @if (session('success'))
                                <div class="alert alert-block alert-success fade in">
                                    <button data-dismiss="alert" class="close close-sm" type="button">
                                        <i class="fa fa-times"></i>
                                    </button>
                                    <strong>{{session('success')}}</strong>
                                </div>
                            @endif
                            <div class="btn-group pull-right">
                                <button class="btn dropdown-toggle" data-toggle="dropdown">Action <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="{{route('waste.create')}}">Tambah Data</a></li>
                                </ul>
                            </div>
                            <table  class="display table table-bordered table-striped" id="example">
                                <thead>
                                <th>Date</th>
                                <th>Voyage Planning</th>
                                <th>Weight</th>
                                <th>Waste Type</th>
                                <th>Action</th>
                                </thead>
                                <tbody>
                                @foreach($wastes as $waste)
                                    <tr>
                                        <td>
                                            {{date_format(date_create($waste->date), 'd-m-Y')}}
                                        </td>
                                        <td>
                                            @foreach($voyage_plannings as $voyage_planning)
                                                @if($voyage_planning->id == $voyage_planning->route_id)
                                                    {{$voyage_planning->crew_qty}}
                                                @endif
                                            @endforeach
                                        </td>
                                        <td>
                                            {{$waste->weight}}
                                        </td>
                                        <td>
                                            {{$waste->waste_type}}
                                        </td>
                                        <td>
                                            <a class="btn btn-success" href="{{route('waste.update', ['id' => encrypt($waste->id)])}}">Update</a>
                                            <a class="btn btn-danger" href="{{route('waste.delete', ['id' => encrypt($waste->id)])}}">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>
<!--main content end-->
@include('footer')