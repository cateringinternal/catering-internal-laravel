@include('header')
        <!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Route
                    </header>
                    <div class="panel-body">
                        @if (count($errors) > 0)
                            <div class="alert alert-block alert-danger fade in">
                                <button data-dismiss="alert" class="close close-sm" type="button">
                                    <i class="fa fa-times"></i>
                                </button>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="form">
                            <form class="cmxform form-horizontal tasi-form" ng-controller="RouteFormController" ng-init="init({{$ports}}, '')" method="post" action="{{route('route.create')}}">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Port Origin</label>
                                    <div class="col-lg-10">
                                        <select class="form-control" name="port_origin">
                                            @foreach($ports as $port)
                                                <option value="{{$port->id}}">{{$port->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Port Destination</label>
                                    <div class="col-lg-10">
                                        <select class="form-control" name="port_destination">
                                            @foreach($ports as $port)
                                                <option value="{{$port->id}}">{{$port->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Eta</label>
                                    <div class="col-lg-10">
                                        <input class="form-control" type="number" name="eta" value="{{ old('eta') }}"/>
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <label class="control-label col-md-4">Pemberhentian</label>
                                    <div class="col-md-8">
                                        <select class="form-control" ng-model="port" ng-options="port.name for port in ports"></select>
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <label class="control-label col-md-4">Estimasi</label>
                                    <div class="col-md-8">
                                        <input class="form-control" type="number" ng-model="estimasi" value="{{ old('estimasi') }}"/>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-4"></label>
                                    <div class="col-md-8">
                                        <a href="" class="btn btn-success" ng-click="addRouteStop()">Add</a>
                                    </div>
                                </div>
                                <table class="display table table-bordered">
                                    <thead>
                                    <th>Pemberhentian</th>
                                    <th>Estimasi</th>
                                    <th>Action</th>
                                    </thead>
                                    <tbody>
                                    <tr ng-repeat="routeStop in routeStops">
                                        <td>
                                            <% routeStop.port.name %>
                                            <input class="form-control" type="hidden" name="routeStop[<%$index%>][port]" value="<% routeStop.port.id %>"/>
                                        </td>
                                        <td>
                                            <% routeStop.estimasi %>
                                            <input class="form-control" type="hidden" name="routeStop[<%$index%>][estimasi]" value="<% routeStop.estimasi %>"/>
                                        </td>
                                        <td>
                                            <a href="" class="btn btn-success" ng-click="removeRouteStop($index)">Remove</a>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">
                                        <input class="btn btn-danger" type="submit" name="submit"/>
                                        <a href="{{route('route')}}" class="btn btn-info">Cancel</a>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                </section>
            </div>
        </div>
    </section>
</section>
@include('footer')