@include('header')
        <!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Meal Planning
                    </header>
                    <div class="panel-body">
                        @if (count($errors) > 0)
                            <div class="alert alert-block alert-danger fade in">
                                <button data-dismiss="alert" class="close close-sm" type="button">
                                    <i class="fa fa-times"></i>
                                </button>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="form">
                            <form class="cmxform form-horizontal tasi-form" method="post" action="{{route('meal-planning.create')}}">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Meal For</label>
                                    <div class="col-lg-10">
                                        <input class="form-control" type="text" name="meal_for" value="{{ old('meal_for') }}"/>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Voyage Class</label>
                                    <div class="col-lg-10">
                                        <select class="form-control" name="voyage_class_id">
                                            @foreach($voyage_classes as $voyage_class)
                                                <option value="{{$voyage_class->id}}">{{$voyage_class->class}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Dry Menu</label>
                                    <div class="col-lg-10">
                                        <select class="form-control" name="dry_menu">
                                            @foreach($menus as $menu)
                                                <option value="{{$menu->id}}">{{$menu->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="ccomment" class="control-label col-lg-2">Menu</label>
                                    <div class="col-lg-10">
                                        <select class="form-control" name="menu_id">
                                            @foreach($menus as $menu)
                                                <option value="{{$menu->id}}">{{$menu->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Voyage Planning</label>
                                    <div class="col-lg-10">
                                        <select class="form-control" name="voyage_planning_id">
                                            @foreach($voyage_plannings as $voyage_planning)
                                                <option value="{{$voyage_planning->id}}">{{$voyage_planning->crew_qty}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Meal Time</label>
                                    <div class="col-lg-10">
                                        <input class="form-control" type="text" name="meal_time" value="{{ old('meal_time') }}"/>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Jumlah Hari</label>
                                    <div class="col-lg-10">
                                        <input class="form-control" type="number" name="number_days" value="{{ old('number_days') }}"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">
                                        <input class="btn btn-danger" type="submit" name="Submit"/>
                                        <a href="{{route('meal-planning')}}" class="btn btn-info">Cancel</a>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                </section>
            </div>
        </div>
    </section>
</section>
@include('footer')