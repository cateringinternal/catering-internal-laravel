@include('header')
        <!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Meal Planning
                    </header>
                    <div class="panel-body">
                        <div class="adv-table">
                            @if (session('success'))
                                <div class="alert alert-block alert-success fade in">
                                    <button data-dismiss="alert" class="close close-sm" type="button">
                                        <i class="fa fa-times"></i>
                                    </button>
                                    <strong>{{session('success')}}</strong>
                                </div>
                            @endif
                            <div class="btn-group pull-right">
                                <button class="btn dropdown-toggle" data-toggle="dropdown">Action <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="{{route('meal-planning.create')}}">Tambah Data</a></li>
                                </ul>
                            </div>
                            <table  class="display table table-bordered table-striped" id="example">
                                <thead>
                                <th>Meal for</th>
                                <th>Voyage Class</th>
                                <th>Dry Menu</th>
                                <th>Menu</th>
                                <th>Voyage Planning</th>
                                <th>Meal Time</th>
                                <th>Jumlah Hari</th>
                                <th>Action</th>
                                </thead>
                                <tbody>
                                @foreach($meal_plannings as $meal_planning)
                                    <tr>
                                        <td>{{$meal_planning->meal_for}}</td>
                                        <td>
                                            @foreach($voyage_classes as $voyage_class)
                                                @if($voyage_class->id == $meal_planning->voyage_class_id)
                                                    {{$voyage_class->class}}
                                                @endif
                                            @endforeach
                                        </td>
                                        <td>
                                            @foreach($menus as $menu)
                                                @if($menu->id == $meal_planning->dry_menu)
                                                    {{$menu->name}}
                                                @endif
                                            @endforeach
                                        </td>
                                        <td>
                                            @foreach($menus as $menu)
                                                @if($menu->id == $meal_planning->menu_id)
                                                    {{$menu->name}}
                                                @endif
                                            @endforeach
                                        </td>
                                        <td>
                                            @foreach($voyage_plannings as $voyage_planning)
                                                @if($voyage_planning->id == $meal_planning->voyage_planning_id)
                                                    {{$voyage_planning->crew_qty}}
                                                @endif
                                            @endforeach
                                        </td>
                                        <td>{{$meal_planning->meal_time}}</td>
                                        <td>{{$meal_planning->number_days}}</td>
                                        <td>
                                            <a class="btn btn-success" href="{{route('meal-planning.update', ['id' => encrypt($meal_planning->id)])}}">Update</a>
                                            <a class="btn btn-danger" href="{{route('meal-planning.delete', ['id' => encrypt($meal_planning->id)])}}">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>
<!--main content end-->
@include('footer')