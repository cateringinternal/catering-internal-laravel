@include('header')
        <!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        UOM
                    </header>
                    <div class="panel-body">
                        <div class="adv-table">
                            @if (session('success'))
                                <div class="alert alert-block alert-success fade in">
                                    <button data-dismiss="alert" class="close close-sm" type="button">
                                        <i class="fa fa-times"></i>
                                    </button>
                                    <strong>{{session('success')}}</strong>
                                </div>
                            @endif
                            <div class="btn-group pull-right">
                                <button class="btn dropdown-toggle" data-toggle="dropdown">Action <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="{{route('uom.create')}}">Tambah Data</a></li>
                                </ul>
                            </div>
                            <table  class="display table table-bordered table-striped" id="example">
                                <thead>
                                <th>Name</th>
                                <th>Type</th>
                                <th>Description</th>
                                <th>Pengali</th>
                                <th>Action</th>
                                </thead>
                                <tbody>
                                @foreach($uoms as $uom)
                                    <tr>
                                        <td>{{$uom->uom}}</td>
                                        <td>{{$uom->type}}</td>
                                        <td>{{$uom->description}}</td>
                                        <td>{{$uom->multiplier}}</td>
                                        <td>
                                            <a class="btn btn-success" href="{{route('uom.update', ['id' => encrypt($uom->id)])}}">Update</a>
                                            <a class="btn btn-danger" href="{{route('uom.delete', ['id' => encrypt($uom->id)])}}">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>
<!--main content end-->
@include('footer')