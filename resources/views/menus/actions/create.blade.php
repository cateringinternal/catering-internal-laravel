@include('header')
        <!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Menu
                    </header>
                    <div class="panel-body">
                        @if (count($errors) > 0)
                            <div class="alert alert-block alert-danger fade in">
                                <button data-dismiss="alert" class="close close-sm" type="button">
                                    <i class="fa fa-times"></i>
                                </button>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="form">
                            <form class="cmxform form-horizontal tasi-form" method="post" action="{{route('menu.create')}}">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Nama</label>
                                    <div class="col-lg-10">
                                        <input class="form-control" type="text" name="name" value="{{ old('name') }}"/>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Appertizer</label>
                                    <div class="col-lg-10">
                                        <select class="form-control" name="appetizer">
                                            @foreach($recipes as $recipe)
                                                <option value="{{$recipe->id}}">{{$recipe->title}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Main Dish</label>
                                    <div class="col-lg-10">
                                        <select class="form-control" name="main_dish">
                                            @foreach($recipes as $recipe)
                                                <option value="{{$recipe->id}}">{{$recipe->title}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="ccomment" class="control-label col-lg-2">Dessert</label>
                                    <div class="col-lg-10">
                                        <select class="form-control" name="dessert">
                                            @foreach($recipes as $recipe)
                                                <option value="{{$recipe->id}}">{{$recipe->title}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Beverage</label>
                                    <div class="col-lg-10">
                                        <select class="form-control" name="beverage">
                                            @foreach($recipes as $recipe)
                                                <option value="{{$recipe->id}}">{{$recipe->title}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Type</label>
                                    <div class="col-lg-10">
                                        <input class="form-control" type="text" name="type" value="{{ old('type') }}"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">
                                        <input class="btn btn-danger" type="submit" name="Submit"/>
                                        <a href="{{route('menu')}}" class="btn btn-info">Cancel</a>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                </section>
            </div>
        </div>
    </section>
</section>
@include('footer')