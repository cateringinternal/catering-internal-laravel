@include('header')
        <!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Menu
                    </header>
                    <div class="panel-body">
                        <div class="adv-table">
                            @if (session('success'))
                                <div class="alert alert-block alert-success fade in">
                                    <button data-dismiss="alert" class="close close-sm" type="button">
                                        <i class="fa fa-times"></i>
                                    </button>
                                    <strong>{{session('success')}}</strong>
                                </div>
                            @endif
                            <div class="btn-group pull-right">
                                <button class="btn dropdown-toggle" data-toggle="dropdown">Action <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="{{route('menu.create')}}">Tambah Data</a></li>
                                </ul>
                            </div>
                            <table  class="display table table-bordered table-striped" id="example">
                                <thead>
                                <th>Nama</th>
                                <th>Appetizer</th>
                                <th>Main Dish</th>
                                <th>Dessert</th>
                                <th>Beverage</th>
                                <th>Type</th>
                                <th>Action</th>
                                </thead>
                                <tbody>
                                @foreach($menus as $menu)
                                    <tr>
                                        <td>
                                            {{$menu->name}}
                                        </td>
                                        <td>
                                            @foreach($recipes as $recipe)
                                                @if($recipe->id == $menu->appetizer)
                                                    {{$recipe->title}}
                                                @endif
                                            @endforeach
                                        </td>
                                        <td>
                                            @foreach($recipes as $recipe)
                                                @if($recipe->id == $menu->main_dish)
                                                    {{$recipe->title}}
                                                @endif
                                            @endforeach
                                        </td>
                                        <td>
                                            @foreach($recipes as $recipe)
                                                @if($recipe->id == $menu->dessert)
                                                    {{$recipe->title}}
                                                @endif
                                            @endforeach
                                        </td>
                                        <td>
                                            @foreach($recipes as $recipe)
                                                @if($recipe->id == $menu->beverage)
                                                    {{$recipe->title}}
                                                @endif
                                            @endforeach
                                        </td>
                                        <td>{{$menu->type}}</td>
                                        <td>
                                            <a class="btn btn-success" href="{{route('menu.update', ['id' => encrypt($menu->id)])}}">Update</a>
                                            <a class="btn btn-danger" href="{{route('menu.delete', ['id' => encrypt($menu->id)])}}">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>
<!--main content end-->
@include('footer')