@include('header')
        <!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Inventory
                    </header>
                    <div class="panel-body">
                        <div class="adv-table">
                            @if (session('success'))
                                <div class="alert alert-block alert-success fade in">
                                    <button data-dismiss="alert" class="close close-sm" type="button">
                                        <i class="fa fa-times"></i>
                                    </button>
                                    <strong>{{session('success')}}</strong>
                                </div>
                            @endif
                            <div class="btn-group pull-right">
                                <button class="btn dropdown-toggle" data-toggle="dropdown">Action <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="{{route('inventory.create')}}">Tambah Data</a></li>
                                </ul>
                            </div>
                            <table  class="display table table-bordered table-striped" id="example">
                                <thead>
                                <th>Ingredient</th>
                                <th>Quantity</th>
                                <th>Unit</th>
                                <th>Expiry Date</th>
                                <th>NFC Label</th>
                                <th>Remark</th>
                                <th>Warehouse</th>
                                <th>Receiving Item</th>
                                <th>Mutation From</th>
                                <th>Action</th>
                                </thead>
                                <tbody>
                                @foreach($inventories as $inventory)
                                    <tr>
                                        <td>
                                            @foreach($ingredients as $ingredient)
                                                @if($ingredient->id == $inventory->ingredient_id)
                                                    {{$ingredient->name}}
                                                @endif
                                            @endforeach
                                        </td>
                                        <td>{{$inventory->quantity}}</td>
                                        <td>{{$inventory->unit}}</td>
                                        <td>{{date_format(date_create($inventory->expiry_date), 'd-m-Y')}}</td>
                                        <td>{{$inventory->nfc_label}}</td>
                                        <td>{{$inventory->remark}}</td>
                                        <td>
                                            @foreach($warehouses as $warehouse)
                                                @if($warehouse->id == $inventory->warehouse_id)
                                                    {{$warehouse->name}}
                                                @endif
                                            @endforeach
                                        </td>
                                        <td>
                                            @foreach($receiving_items as $receiving_item)
                                                @if($receiving_item->id == $inventory->receiving_item_id)
                                                    {{$receiving_item->quantity}}
                                                @endif
                                            @endforeach
                                        </td>
                                        <td>{{$inventory->mutation_from}}</td>
                                        <td>
                                            <a class="btn btn-success" href="{{route('inventory.update', ['id' => encrypt($inventory->id)])}}">Update</a>
                                            <a class="btn btn-danger" href="{{route('inventory.delete', ['id' => encrypt($inventory->id)])}}">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>
<!--main content end-->
@include('footer')