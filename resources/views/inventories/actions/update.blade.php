@include('header')
        <!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Inventory
                    </header>
                    <div class="panel-body">
                        @if (count($errors) > 0)
                            <div class="alert alert-block alert-danger fade in">
                                <button data-dismiss="alert" class="close close-sm" type="button">
                                    <i class="fa fa-times"></i>
                                </button>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="form">
                            <form class="cmxform form-horizontal tasi-form" method="post" action="{{route('inventory.update', ['id' => $inventory->id])}}">
                                <input type="hidden" name="_method" value="PUT">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Ingredient</label>
                                    <div class="col-lg-10">
                                        <select class="form-control" name="ingredient">
                                            @foreach($ingredients as $ingredient)
                                                <option value="{{$ingredient->id}}" @if($ingredient->id == $inventory->ingredient_id) selected @endif>{{$ingredient->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Quantity</label>
                                    <div class="col-lg-10">
                                        <input class="form-control" type="number" name="quantity" value="{{ $inventory->quantity }}"/>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Unit</label>
                                    <div class="col-lg-10">
                                        <input class="form-control" type="text" name="unit" value="{{ $inventory->unit }}"/>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Expiry Date</label>
                                    <div class="col-lg-10">
                                        <input class="form-control" type="date" name="expiry_date" value="{{ date_format(date_create($inventory->expiry_date), 'Y-m-d')}}"/>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">NFC Label</label>
                                    <div class="col-lg-10">
                                        <input class="form-control" type="text" name="nfc_label" value="{{ $inventory->nfc_label }}"/>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Remark</label>
                                    <div class="col-lg-10">
                                        <input class="form-control" type="text" name="remark" value="{{ $inventory->remark }}"/>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Warehouse</label>
                                    <div class="col-lg-10">
                                        <select class="form-control" name="warehouse">
                                            @foreach($warehouses as $warehouse)
                                                <option value="{{$warehouse->id}}" @if($warehouse->id == $inventory->warehouse_id) selected @endif>{{$warehouse->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Receiving Item</label>
                                    <div class="col-lg-10">
                                        <select class="form-control" name="receiving_item">
                                            @foreach($receiving_items as $receiving_item)
                                                <option value="{{$receiving_item->id}}" @if($receiving_item->id == $inventory->receving_item_id) selected @endif>{{$receiving_item->quantity}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Mutation From</label>
                                    <div class="col-lg-10">
                                        <input class="form-control" type="number" name="mutation_from" value="{{ $inventory->mutation_from }}"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">
                                        <input class="btn btn-danger" type="submit" name="Submit"/>
                                        <a href="{{route('inventory')}}" class="btn btn-info">Cancel</a>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                </section>
            </div>
        </div>
    </section>
</section>
@include('footer')<?php
/**
 * Created by PhpStorm.
 * User: dmustt
 * Date: 15/10/16
 * Time: 13:16
 */