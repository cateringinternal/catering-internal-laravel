@include('header')
        <!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Waste Per Meal
                    </header>
                    <div class="panel-body">
                        <div class="adv-table">
                            @if (session('success'))
                                <div class="alert alert-block alert-success fade in">
                                    <button data-dismiss="alert" class="close close-sm" type="button">
                                        <i class="fa fa-times"></i>
                                    </button>
                                    <strong>{{session('success')}}</strong>
                                </div>
                            @endif
                            <div class="btn-group pull-right">
                                <button class="btn dropdown-toggle" data-toggle="dropdown">Action <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="{{route('waste-per-meal.create')}}">Tambah Data</a></li>
                                </ul>
                            </div>
                            <table  class="display table table-bordered table-striped" id="example">
                                <thead>
                                <th>Waste</th>
                                <th>Meal Planning</th>
                                <th>Weight</th>
                                <th>Action</th>
                                </thead>
                                <tbody>
                                @foreach($waste_per_meals as $waste_per_meal)
                                    <tr>
                                        <td>
                                            @foreach($wastes as $waste)
                                                @if($waste->id == $waste_per_meal->waste_id)
                                                    {{$waste->waste_type}}
                                                @endif
                                            @endforeach
                                        </td>
                                        <td>
                                            @foreach($meal_plannings as $meal_planning)
                                                @if($meal_planning->id == $waste_per_meal->meals_planning_id)
                                                    {{$meal_planning->meal_for}}
                                                @endif
                                            @endforeach
                                        </td>
                                        <td>
                                            {{$waste_per_meal->weight}}
                                        </td>
                                        <td>
                                            <a class="btn btn-success" href="{{route('waste-per-meal.update', ['id' => encrypt($waste_per_meal->id)])}}">Update</a>
                                            <a class="btn btn-danger" href="{{route('waste-per-meal.delete', ['id' => encrypt($waste_per_meal->id)])}}">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>
<!--main content end-->
@include('footer')