@include('header')
        <!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Waste Per Meal
                    </header>
                    <div class="panel-body">
                        @if (count($errors) > 0)
                            <div class="alert alert-block alert-danger fade in">
                                <button data-dismiss="alert" class="close close-sm" type="button">
                                    <i class="fa fa-times"></i>
                                </button>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="form">
                            <form class="cmxform form-horizontal tasi-form" method="post" action="{{route('waste-per-meal.update', ['id' => $waste_per_meal->id])}}">
                                <input type="hidden" name="_method" value="PUT">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Waste</label>
                                    <div class="col-lg-10">
                                        <select class="form-control" name="waste">
                                            @foreach($wastes as $waste)
                                                <option value="{{$waste->id}}" @if($waste->id == $waste_per_meal->waste_id) selected @endif>{{$waste->waste_type}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Meal Planning</label>
                                    <div class="col-lg-10">
                                        <select class="form-control" name="meal_planning">
                                            @foreach($meal_plannings as $meal_planning)
                                                <option value="{{$meal_planning->id}}" @if($meal_planning->id == $waste_per_meal->meal_planning_id) selected @endif>{{$meal_planning->meal_for}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Weight</label>
                                    <div class="col-lg-10">
                                        <input class="form-control" type="text" name="weight" value="{{ $waste_per_meal->weight }}"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">
                                        <input class="btn btn-danger" type="submit" name="Submit"/>
                                        <a href="{{route('waste-per-meal')}}" class="btn btn-info">Cancel</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </section>
</section>
@include('footer')