@include('header')
        <!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Cruise
                    </header>
                    <div class="panel-body">
                        @if (count($errors) > 0)
                            <div class="alert alert-block alert-danger fade in">
                                <button data-dismiss="alert" class="close close-sm" type="button">
                                    <i class="fa fa-times"></i>
                                </button>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="form">
                            <form class="cmxform form-horizontal tasi-form" ng-controller="CruiseFormController" ng-init="init({{json_encode($cruise->warehouse)}})" method="post" action="{{route('cruise.update', ['id' => $cruise->id])}}">
                                <input type="hidden" name="_method" value="PUT">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Voyage Start</label>
                                    <div class="col-lg-10">
                                        <input type="date" class=" form-control" name="voyage_number_start" value="{{date_format(date_create($cruise->voyage_number_start), 'Y-m-d')}}"/>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Voyage End</label>
                                    <div class="col-lg-10">
                                        <input type="date" class="form-control" name="voyage_number_end" value="{{date_format(date_create($cruise->voyage_number_end), 'Y-m-d')}}"/>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Capacity</label>
                                    <div class="col-lg-10">
                                        <input type="number" class="form-control" name="capacity" value="{{$cruise->capacity}}"/>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="ccomment" class="control-label col-lg-2">Tipe</label>
                                    <div class="col-lg-10">
                                        <input type="text" class="form-control" name="cruise_type" value="{{$cruise->cruise_type}}"/>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">No. IMO</label>
                                    <div class="col-lg-10">
                                        <input type="number" class="form-control" name="imo_number" value="{{$cruise->imo_number}}"/>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Total Capacity</label>
                                    <div class="col-lg-10">
                                        <input type="number" class="form-control" name="total_capacity" value="{{$cruise->total_capacity}}"/>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-4">Warehouse</label>
                                    <div class="col-md-8">
                                        <input class="form-control" type="text" ng-model="warehouseName" name="warehouseName" value="{{ old('warehouseName') }}"/>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-4">Warehouse Type</label>
                                    <div class="col-md-8">
                                        <input class="form-control" type="text" ng-model="warehouseType" name="warehouseType" value="{{ old('warehouseType') }}"/>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-4"></label>
                                    <div class="col-md-8">
                                        <a href="" class="btn btn-success" ng-click="addWarehouse()">Add</a>
                                    </div>
                                </div>
                                <table class="display table table-bordered">
                                    <thead>
                                    <th>Warehouse</th>
                                    <th>Warehouse Type</th>
                                    <th>Action</th>
                                    </thead>
                                    <tbody>
                                    <tr ng-repeat="warehouse in warehouses">
                                        <td>
                                            <% warehouse.warehouseName %>
                                            <input class="form-control" type="hidden" name="warehouse[<%$index%>][warehouseName]" value="<% warehouse.warehouseName %>"/>
                                        </td>
                                        <td>
                                            <% warehouse.warehouseType %>
                                            <input class="form-control" type="hidden" name="warehouse[<%$index%>][warehouseType]" value="<% warehouse.warehouseType %>"/>
                                        </td>
                                        <td>
                                            <a href="" class="btn btn-success" ng-click="removeWarehouse($index)">Remove</a>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">
                                        <input class="btn btn-danger" type="submit" name="submit"/>
                                        <a href="{{route('cruise')}}" class="btn btn-info">Cancel</a>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                </section>
            </div>
        </div>
    </section>
</section>
@include('footer')