@include('header')
        <!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Cruise
                    </header>
                    <div class="panel-body">
                        <div class="adv-table">
                            @if (session('success'))
                                <div class="alert alert-block alert-success fade in">
                                    <button data-dismiss="alert" class="close close-sm" type="button">
                                        <i class="fa fa-times"></i>
                                    </button>
                                    <strong>{{session('success')}}</strong>
                                </div>
                            @endif
                            <div class="btn-group pull-right">
                                <button class="btn dropdown-toggle" data-toggle="dropdown">Action <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="{{route('cruise.create')}}">Tambah Data</a></li>
                                </ul>
                            </div>
                            <table  class="display table table-bordered table-striped" id="example">
                                <thead>
                                <th>Voyage Start</th>
                                <th>Voyage End</th>
                                <th>Kapasitas</th>
                                <th>Tipe</th>
                                <th>No. IMO</th>
                                <th>Total Kapasitas</th>
                                <th>Action</th>
                                </thead>
                                <tbody>
                                @foreach($cruises as $cruise)
                                    <tr>
                                        <td>
                                            {{date_format(date_create($cruise->voyage_number_start), 'd-m-Y')}}
                                        </td>
                                        <td>
                                            {{date_format(date_create($cruise->voyage_number_end), 'd-m-Y')}}
                                        </td>
                                        <td>{{$cruise->capacity}}</td>
                                        <td>{{$cruise->cruise_type}}</td>
                                        <td>{{$cruise->imo_number}}</td>
                                        <td>{{$cruise->total_capacity}}</td>
                                        <td>
                                            <a class="btn btn-success" href="{{route('cruise.update', ['id' => encrypt($cruise->id)])}}">Update</a>
                                            <a class="btn btn-danger" href="{{route('cruise.delete', ['id' => encrypt($cruise->id)])}}">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>
<!--main content end-->
@include('footer')