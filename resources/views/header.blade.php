<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Mosaddek">
    <meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
    <link rel="shortcut icon" href="img/favicon.png">

    <title>Catering System</title>

    <!-- Bootstrap core CSS -->
    <link href="{{config('app.url')}}/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{config('app.url')}}/css/bootstrap-reset.css" rel="stylesheet">
    <!--external css-->
    <link href="{{config('app.url')}}/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href="{{config('app.url')}}/assets/advanced-datatable/media/css/demo_page.css" rel="stylesheet" />
    <link href="{{config('app.url')}}/assets/advanced-datatable/media/css/demo_table.css" rel="stylesheet" />
    <link rel="stylesheet" href="{{config('app.url')}}/assets/data-tables/DT_bootstrap.css" />
    <!-- Custom styles for this template -->
    <link href="{{config('app.url')}}/css/style.css" rel="stylesheet">
    <link href="{{config('app.url')}}/css/style-responsive.css" rel="stylesheet" />

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
</head>

<body ng-app="DmusttApps">

<section id="container" class="">
    <!--header start-->
    <header class="header white-bg">
        <div class="sidebar-toggle-box">
            <div data-original-title="Toggle Navigation" data-placement="right" class="fa fa-bars tooltips"></div>
        </div>
        <!--logo start-->
        <a href="index.html" class="logo" >Catering<span>System</span></a>
        <!--logo end-->
        <div class="nav notify-row" id="top_menu">
        </div>
        <div class="top-nav ">
            <ul class="nav pull-right top-menu">
                <li>
                    <input type="text" class="form-control search" placeholder="Search">
                </li>
                <!-- user login dropdown start-->
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <img alt="" src="{{config('app.url')}}/img/avatar1_small.jpg">
                        <span class="username">Jhon Doue</span>
                        <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu extended logout">
                        <div class="log-arrow-up"></div>
                        <li><a href="#"><i class=" fa fa-suitcase"></i>Profile</a></li>
                        <li><a href="#"><i class="fa fa-cog"></i> Settings</a></li>
                        <li><a href="#"><i class="fa fa-bell-o"></i> Notification</a></li>
                        <li><a href="login.html"><i class="fa fa-key"></i> Log Out</a></li>
                    </ul>
                </li>
                <!-- user login dropdown end -->
            </ul>
        </div>
    </header>
    <!--header end-->
    <!--sidebar start-->
    <aside>
        <div id="sidebar"  class="nav-collapse ">
            <!-- sidebar menu start-->
            <ul class="sidebar-menu" id="nav-accordion">
                <li>
                    <a href="{{route('cruise')}}">
                        <i class="fa fa-dashboard"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
                <li>
                    <a href="{{route('ingredient')}}">
                        <i class="fa fa-dashboard"></i>
                        <span>Ingredient</span>
                    </a>
                </li>
                <li>
                    <a href="{{route('recipe')}}">
                        <i class="fa fa-dashboard"></i>
                        <span>Recipe</span>
                    </a>
                </li>
                <li>
                    <a href="{{route('menu')}}">
                        <i class="fa fa-dashboard"></i>
                        <span>Menu</span>
                    </a>
                </li>
                <li>
                    <a href="{{route('user')}}">
                        <i class="fa fa-dashboard"></i>
                        <span>User</span>
                    </a>
                </li>
                <li>
                    <a href="{{route('vendor')}}">
                        <i class="fa fa-dashboard"></i>
                        <span>Vendor</span>
                    </a>
                </li>
                <li>
                    <a href="{{route('cruise')}}">
                        <i class="fa fa-dashboard"></i>
                        <span>Cruise</span>
                    </a>
                </li>
                <li>
                    <a href="{{route('route')}}">
                        <i class="fa fa-dashboard"></i>
                        <span>Route</span>
                    </a>
                </li>
                <li>
                    <a href="{{route('meal-planning')}}">
                        <i class="fa fa-dashboard"></i>
                        <span>Meal Planning</span>
                    </a>
                </li>
                <li class="sub-menu">
                    <a href="javascript:;">
                        <i class="fa fa-laptop"></i>
                        <span>Voyage</span>
                    </a>
                    <ul class="sub">
                        <li><a  href="{{route('voyage-class')}}">Voyage Class</a></li>
                        <li><a  href="{{route('voyage-planning')}}">Voyage Planning</a></li>
                    </ul>
                </li>
                <li class="sub-menu">
                    <a href="javascript:;">
                        <i class="fa fa-laptop"></i>
                        <span>Requisition</span>
                    </a>
                    <ul class="sub">
                        <li><a  href="{{route('requisition')}}">Requisition</a></li>
                        <li><a  href="{{route('requisition-item')}}">Requisition Item</a></li>
                    </ul>
                </li>
                <li class="sub-menu">
                    <a href="javascript:;">
                        <i class="fa fa-laptop"></i>
                        <span>Purchase Order</span>
                    </a>
                    <ul class="sub">
                        <li><a  href="{{route('purchase-order')}}">Purchase Order</a></li>
                        <li><a  href="{{route('purchase-order-item')}}">Purchase Order Item</a></li>
                    </ul>
                </li>
                <li>
                    <a href="{{route('invoice')}}">
                        <i class="fa fa-dashboard"></i>
                        <span>Invoice</span>
                    </a>
                </li>
            </ul>
            <!-- sidebar menu end-->
        </div>
    </aside>
    <!--sidebar end-->