/**
 * Created by dmustt on 22/10/16.
 */

"use strict";

dmustt.controller('IngredientFormController', [ '$scope', function ($scope) {

    $scope.variantName = '';

    $scope.bahanUtama = '';

    $scope.description = '';

    $scope.variants = [];

    $scope.init = function (ingredientName, data) {
        if (data === 'undefined') {
            return;
        }
        angular.forEach(data, function(value) {
            $scope.variants.push({
                variantName : value.variation,
                description : value.description,
                bahanUtama : ingredientName
            });
        });

        $scope.bahanUtama = ingredientName;
    }

    $scope.removeVariant = function (index) {
        $scope.variants.splice(index, 1);
    };

    $scope.addVariant = function () {
        $scope.variants.push({
            variantName : $scope.variantName,
            description : $scope.description,
            bahanUtama : $scope.bahanUtama
        });
    }
}]);
