/**
 * Created by dmustt on 23/10/16.
 */

dmustt.controller('RecipeFormController', [ '$scope', function ($scope) {

    $scope.bahan = '';

    $scope.ingredients = [];

    $scope.uoms = [];

    $scope.jumlah = '';

    $scope.satuan = '';

    $scope.ingredientRecipes = [];

    $scope.init = function (ingredient, uom, ingredientRecipe) {
        if (ingredient === 'undefined' || uom === 'undefined') {
            return;
        }

        $scope.ingredients = ingredient;

        $scope.uoms = uom;

        var bahan_temp = [],
            uom_temp = [];

        bahan_temp.push(ingredient);
        uom_temp.push(uom);

        $scope.bahan = bahan_temp[0][0];
        $scope.satuan = uom_temp[0][0];

        if (ingredientRecipe !== '') {
            var ingredientInserted = [],
                uomInserted = [];
            angular.forEach(ingredientRecipe, function(value) {
                angular.forEach(ingredient, function(ingredientValue) {
                    if (ingredientValue.id == value.ingredient_id) {
                        ingredientInserted = ingredientValue;
                    }
                });
                angular.forEach(uom, function(uomValue) {
                    if (uomValue.id == value.uom_id) {
                        uomInserted = uomValue;
                    }
                });
                $scope.ingredientRecipes.push({
                    bahan : ingredientInserted,
                    jumlah : value.ingredient_recipe_qty,
                    satuan : uomInserted
                });
            });
        }
    }

    $scope.removeIngredientRecipe = function (index) {
        $scope.ingredientRecipes.splice(index, 1);
    };

    $scope.addIngredientRecipe = function () {
        $scope.ingredientRecipes.push({
            bahan : $scope.bahan,
            jumlah : $scope.jumlah,
            satuan : $scope.satuan
        });
    }
}]);
