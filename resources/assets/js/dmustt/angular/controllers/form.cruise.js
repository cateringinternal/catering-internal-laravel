/**
 * Created by dmustt on 24/10/16.
 */

dmustt.controller('CruiseFormController', [ '$scope', function ($scope) {

    $scope.warehouseName = '';

    $scope.warehouseType = '';

    $scope.warehouses = [];

    $scope.init = function (data) {
        if (data === 'undefined') {
            return;
        }
        angular.forEach(data, function(value) {
            $scope.warehouses.push({
                warehouseName : value.name,
                warehouseType : value.type,
            });
        });
    }

    $scope.removeWarehouse = function (index) {
        $scope.warehouses.splice(index, 1);
    };

    $scope.addWarehouse = function () {
        $scope.warehouses.push({
            warehouseName : $scope.warehouseName,
            warehouseType : $scope.warehouseType,
        });
    }
}]);