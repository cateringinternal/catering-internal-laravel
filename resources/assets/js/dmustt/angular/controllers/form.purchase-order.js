/**
 * Created by dmustt on 25/10/16.
 */

dmustt.controller('PurchaseOrderFormController', ['$scope', function ($scope) {

    $scope.requisition = '';
    $scope.vendor = '';
    $scope.totalPrice = 0;
    $scope.requisitions = [];
    $scope.vendors = [];
    $scope.ingredients = [];
    $scope.uoms = [];
    $scope.requisitionItems = [];
    $scope.purchaseOrderItems = [];

    $scope.init = function (requisition, vendor, ingredient, uom, requisitionItem, purchaseOrderItem) {
        if (requisition === 'undefined' || vendor === 'undefined' ||
            ingredient === 'undefined' || uom === 'undefined') {
            return;
        }

        $scope.requisitions = requisition;
        $scope.vendors = vendor;
        $scope.requisitionItems = requisitionItem;
        $scope.ingredients = ingredient;
        $scope.uoms = uom;

        if ($scope.totalPrice !== 0) {
            $scope.setTotalPriceZero();
        }

        if (purchaseOrderItem !== '') {
            var ingredientInserted = [],
                uomInserted = [];
            angular.forEach(purchaseOrderItem, function (value) {
                angular.forEach($scope.ingredients, function (ingredientValue) {
                    if (ingredientValue.id == value.ingredient_id) {
                        ingredientInserted = ingredientValue;
                    }
                });
                angular.forEach($scope.uoms, function (uomValue) {
                    if (uomValue.id == value.uom_id) {
                        uomInserted = uomValue;
                    }
                });
                $scope.purchaseOrderItems.push({
                    ingredient: ingredientInserted,
                    quantity: value.quantity,
                    price: value.price,
                    uom: uomInserted,
                });

                $scope.totalPrice = $scope.totalPrice + parseInt(value.price);
            });
        }

    }

    $scope.setPurchaseOrderItemsEmpty = function () {
        $scope.purchaseOrderItems = [];
    }

    $scope.setTotalPriceZero = function () {
        $scope.totalPrice = 0;
    }

    $scope.setPurchaseOrderItem = function () {

        $scope.setPurchaseOrderItemsEmpty();

        if ($scope.requisition === '') {
            return;
        }

        if ($scope.totalPrice !== 0) {
            $scope.setTotalPriceZero();
        }

        var ingredientInserted = [],
            uomInserted = [];
        angular.forEach($scope.requisitionItems, function (value) {
            angular.forEach($scope.ingredients, function (ingredientValue) {
                if (ingredientValue.id == value.ingredient_id) {
                    ingredientInserted = ingredientValue;
                }
            });
            angular.forEach($scope.uoms, function (uomValue) {
                if (uomValue.id == value.uom_id) {
                    uomInserted = uomValue;
                }
            });
            if (value.requisition_id == $scope.requisition.id) {
                $scope.purchaseOrderItems.push({
                    ingredient: ingredientInserted,
                    quantity: value.quantity,
                    price: value.price,
                    uom: uomInserted,
                });
                $scope.totalPrice = $scope.totalPrice + parseInt(value.price);
            }
        });
    }
}]);
