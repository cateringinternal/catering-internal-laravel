/**
 * Created by dmustt on 24/10/16.
 */

dmustt.controller('RouteFormController', [ '$scope', function ($scope) {

    $scope.port = '';

    $scope.ports = [];

    $scope.estimasi = '';

    $scope.routeStops = [];

    $scope.init = function (port, routeStop) {
        if (port === 'undefined') {
            return;
        }

        $scope.ports = port;

        var routeStop_temp = [];

        routeStop_temp.push(port);

        $scope.port = routeStop_temp[0][0];

        if (routeStop !== '') {
            var portInserted = [];
            angular.forEach(routeStop, function(value) {
                angular.forEach(port, function(portValue) {
                    if (portValue.id == value.port_id) {
                        portInserted = portValue;
                    }
                });
                $scope.routeStops.push({
                    port : portInserted,
                    estimasi : value.eta,
                });
            });
        }
    }

    $scope.removeRouteStop = function (index) {
        $scope.routeStops.splice(index, 1);
    };

    $scope.addRouteStop = function () {
        $scope.routeStops.push({
            port : $scope.port,
            estimasi : $scope.estimasi,
        });
    }
}]);
