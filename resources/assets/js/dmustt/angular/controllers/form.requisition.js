/**
 * Created by dmustt on 25/10/16.
 */

dmustt.controller('RequisitionFormController', [ '$scope', function ($scope) {

    $scope.ingredient = '';

    $scope.ingredients = [];

    $scope.uoms = [];

    $scope.quantity = '';

    $scope.price = '';

    $scope.uom = '';

    $scope.requisitionItems = [];

    $scope.init = function (ingredient, uom, requisitionItem) {
        if (ingredient === 'undefined' || uom === 'undefined') {
            return;
        }

        $scope.ingredients = ingredient;

        $scope.uoms = uom;

        var ingredientTemp = [],
            uomTemp = [];

        ingredientTemp.push(ingredient);
        uomTemp.push(uom);

        $scope.ingredient = ingredientTemp[0][0];
        $scope.uom = uomTemp[0][0];

        if (requisitionItem !== '') {
            var ingredientInserted = [],
                uomInserted = [];
            angular.forEach(requisitionItem, function(value) {
                angular.forEach(ingredient, function(ingredientValue) {
                    if (ingredientValue.id == value.ingredient_id) {
                        ingredientInserted = ingredientValue;
                    }
                });
                angular.forEach(uom, function(uomValue) {
                    if (uomValue.id == value.uom_id) {
                        uomInserted = uomValue;
                    }
                });
                $scope.requisitionItems.push({
                    ingredient : ingredientInserted,
                    quantity : value.quantity,
                    price : value.price,
                    uom : uomInserted
                });
            });
        }
    }

    $scope.removeRequisitionItem = function (index) {
        $scope.requisitionItems.splice(index, 1);
    };

    $scope.addRequisitionItem = function () {
        $scope.requisitionItems.push({
            ingredient : $scope.ingredient,
            quantity : $scope.quantity,
            price : $scope.price,
            uom : $scope.uom
        });
    }
}]);
