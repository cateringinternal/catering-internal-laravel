/**
 * Created by dmustt on 27/10/16.
 */

dmustt.controller('InvoiceFormController', ['$scope', function ($scope) {

    $scope.receiving = '';
    $scope.vendor = '';
    $scope.totalPrice = 0;
    $scope.receivings = [];
    $scope.vendors = [];
    $scope.ingredients = [];
    $scope.uoms = [];
    $scope.receivingItems = [];
    $scope.receivingItemListComplete = [];

    $scope.init = function (receiving, vendor, ingredient, uom, receivingItem, receivingItemListComplete) {
        if (receiving === 'undefined' || vendor === 'undefined' ||
            ingredient === 'undefined' || uom === 'undefined') {
            return;
        }

        $scope.receivings = receiving;
        $scope.vendors = vendor;
        $scope.ingredients = ingredient;
        $scope.uoms = uom;
        $scope.receivingItemListComplete =
            receivingItemListComplete === '' ? receivingItem : receivingItemListComplete;

        if ($scope.totalPrice !== 0) {
            $scope.setTotalPriceZero();
        }

        if (receivingItemListComplete !== '') {
            var ingredientInserted = [],
                uomInserted = [];
            angular.forEach(receivingItem, function (value) {
                angular.forEach($scope.ingredients, function (ingredientValue) {
                    if (ingredientValue.id == value.ingredient_id) {
                        ingredientInserted = ingredientValue;
                    }
                });
                angular.forEach($scope.uoms, function (uomValue) {
                    if (uomValue.id == value.uom_id) {
                        uomInserted = uomValue;
                    }
                });
                $scope.receivingItems.push({
                    ingredient: ingredientInserted,
                    quantity: value.quantity,
                    price: value.price,
                    uom: uomInserted,
                    remark: value.remark
                });

                $scope.totalPrice = $scope.totalPrice + parseInt(value.price);
            });
        }

    }

    $scope.setReceivingItemsEmpty = function () {
        $scope.receivingItems = [];
    }

    $scope.setTotalPriceZero = function () {
        $scope.totalPrice = 0;
    }

    $scope.setReceivingItem = function () {

        $scope.setReceivingItemsEmpty();

        if ($scope.receiving === '') {
            return;
        }

        if ($scope.totalPrice !== 0) {
            $scope.setTotalPriceZero();
        }

        var ingredientInserted = [],
            uomInserted = [];
        angular.forEach($scope.receivingItemListComplete, function (value) {
            angular.forEach($scope.ingredients, function (ingredientValue) {
                if (ingredientValue.id == value.ingredient_id) {
                    ingredientInserted = ingredientValue;
                }
            });
            angular.forEach($scope.uoms, function (uomValue) {
                if (uomValue.id == value.uom_id) {
                    uomInserted = uomValue;
                }
            });
            if (value.receiving_id == $scope.receiving.id) {
                $scope.receivingItems.push({
                    ingredient: ingredientInserted,
                    quantity: value.quantity,
                    price: value.price,
                    uom: uomInserted,
                    remark: value.remark
                });
                $scope.totalPrice = $scope.totalPrice + parseInt(value.price);
            }
        });
    }
}]);
