/**
 * Created by dmustt on 22/10/16.
 */

"use strict";

/**
 * JS root scopes
 * need to feed another js
 */
var _rootScope;


// initial dmustt apps module with config
var dmustt = angular.module('DmusttApps', []);

dmustt.config(function ($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
});
