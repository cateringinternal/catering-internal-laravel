/**
 * Created by dmustt on 22/10/16.
 */

"use strict";

/**
 * JS root scopes
 * need to feed another js
 */
var _rootScope;


// initial dmustt apps module with config
var dmustt = angular.module('DmusttApps', []);

dmustt.config(function ($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
});

/**
 * Created by dmustt on 24/10/16.
 */

dmustt.controller('CruiseFormController', [ '$scope', function ($scope) {

    $scope.warehouseName = '';

    $scope.warehouseType = '';

    $scope.warehouses = [];

    $scope.init = function (data) {
        if (data === 'undefined') {
            return;
        }
        angular.forEach(data, function(value) {
            $scope.warehouses.push({
                warehouseName : value.name,
                warehouseType : value.type,
            });
        });
    }

    $scope.removeWarehouse = function (index) {
        $scope.warehouses.splice(index, 1);
    };

    $scope.addWarehouse = function () {
        $scope.warehouses.push({
            warehouseName : $scope.warehouseName,
            warehouseType : $scope.warehouseType,
        });
    }
}]);
/**
 * Created by dmustt on 22/10/16.
 */

"use strict";

dmustt.controller('IngredientFormController', [ '$scope', function ($scope) {

    $scope.variantName = '';

    $scope.bahanUtama = '';

    $scope.description = '';

    $scope.variants = [];

    $scope.init = function (ingredientName, data) {
        if (data === 'undefined') {
            return;
        }
        angular.forEach(data, function(value) {
            $scope.variants.push({
                variantName : value.variation,
                description : value.description,
                bahanUtama : ingredientName
            });
        });

        $scope.bahanUtama = ingredientName;
    }

    $scope.removeVariant = function (index) {
        $scope.variants.splice(index, 1);
    };

    $scope.addVariant = function () {
        $scope.variants.push({
            variantName : $scope.variantName,
            description : $scope.description,
            bahanUtama : $scope.bahanUtama
        });
    }
}]);

/**
 * Created by dmustt on 27/10/16.
 */

dmustt.controller('InvoiceFormController', ['$scope', function ($scope) {

    $scope.receiving = '';
    $scope.vendor = '';
    $scope.totalPrice = 0;
    $scope.receivings = [];
    $scope.vendors = [];
    $scope.ingredients = [];
    $scope.uoms = [];
    $scope.receivingItems = [];
    $scope.receivingItemListComplete = [];

    $scope.init = function (receiving, vendor, ingredient, uom, receivingItem, receivingItemListComplete) {
        if (receiving === 'undefined' || vendor === 'undefined' ||
            ingredient === 'undefined' || uom === 'undefined') {
            return;
        }

        $scope.receivings = receiving;
        $scope.vendors = vendor;
        $scope.ingredients = ingredient;
        $scope.uoms = uom;
        $scope.receivingItemListComplete =
            receivingItemListComplete === '' ? receivingItem : receivingItemListComplete;

        if ($scope.totalPrice !== 0) {
            $scope.setTotalPriceZero();
        }

        if (receivingItemListComplete !== '') {
            var ingredientInserted = [],
                uomInserted = [];
            angular.forEach(receivingItem, function (value) {
                angular.forEach($scope.ingredients, function (ingredientValue) {
                    if (ingredientValue.id == value.ingredient_id) {
                        ingredientInserted = ingredientValue;
                    }
                });
                angular.forEach($scope.uoms, function (uomValue) {
                    if (uomValue.id == value.uom_id) {
                        uomInserted = uomValue;
                    }
                });
                $scope.receivingItems.push({
                    ingredient: ingredientInserted,
                    quantity: value.quantity,
                    price: value.price,
                    uom: uomInserted,
                    remark: value.remark
                });

                $scope.totalPrice = $scope.totalPrice + parseInt(value.price);
            });
        }

    }

    $scope.setReceivingItemsEmpty = function () {
        $scope.receivingItems = [];
    }

    $scope.setTotalPriceZero = function () {
        $scope.totalPrice = 0;
    }

    $scope.setReceivingItem = function () {

        $scope.setReceivingItemsEmpty();

        if ($scope.receiving === '') {
            return;
        }

        if ($scope.totalPrice !== 0) {
            $scope.setTotalPriceZero();
        }

        var ingredientInserted = [],
            uomInserted = [];
        angular.forEach($scope.receivingItemListComplete, function (value) {
            angular.forEach($scope.ingredients, function (ingredientValue) {
                if (ingredientValue.id == value.ingredient_id) {
                    ingredientInserted = ingredientValue;
                }
            });
            angular.forEach($scope.uoms, function (uomValue) {
                if (uomValue.id == value.uom_id) {
                    uomInserted = uomValue;
                }
            });
            if (value.receiving_id == $scope.receiving.id) {
                $scope.receivingItems.push({
                    ingredient: ingredientInserted,
                    quantity: value.quantity,
                    price: value.price,
                    uom: uomInserted,
                    remark: value.remark
                });
                $scope.totalPrice = $scope.totalPrice + parseInt(value.price);
            }
        });
    }
}]);

/**
 * Created by dmustt on 25/10/16.
 */

dmustt.controller('PurchaseOrderFormController', ['$scope', function ($scope) {

    $scope.requisition = '';
    $scope.vendor = '';
    $scope.totalPrice = 0;
    $scope.requisitions = [];
    $scope.vendors = [];
    $scope.ingredients = [];
    $scope.uoms = [];
    $scope.requisitionItems = [];
    $scope.purchaseOrderItems = [];

    $scope.init = function (requisition, vendor, ingredient, uom, requisitionItem, purchaseOrderItem) {
        if (requisition === 'undefined' || vendor === 'undefined' ||
            ingredient === 'undefined' || uom === 'undefined') {
            return;
        }

        $scope.requisitions = requisition;
        $scope.vendors = vendor;
        $scope.requisitionItems = requisitionItem;
        $scope.ingredients = ingredient;
        $scope.uoms = uom;

        if ($scope.totalPrice !== 0) {
            $scope.setTotalPriceZero();
        }

        if (purchaseOrderItem !== '') {
            var ingredientInserted = [],
                uomInserted = [];
            angular.forEach(purchaseOrderItem, function (value) {
                angular.forEach($scope.ingredients, function (ingredientValue) {
                    if (ingredientValue.id == value.ingredient_id) {
                        ingredientInserted = ingredientValue;
                    }
                });
                angular.forEach($scope.uoms, function (uomValue) {
                    if (uomValue.id == value.uom_id) {
                        uomInserted = uomValue;
                    }
                });
                $scope.purchaseOrderItems.push({
                    ingredient: ingredientInserted,
                    quantity: value.quantity,
                    price: value.price,
                    uom: uomInserted,
                });

                $scope.totalPrice = $scope.totalPrice + parseInt(value.price);
            });
        }

    }

    $scope.setPurchaseOrderItemsEmpty = function () {
        $scope.purchaseOrderItems = [];
    }

    $scope.setTotalPriceZero = function () {
        $scope.totalPrice = 0;
    }

    $scope.setPurchaseOrderItem = function () {

        $scope.setPurchaseOrderItemsEmpty();

        if ($scope.requisition === '') {
            return;
        }

        if ($scope.totalPrice !== 0) {
            $scope.setTotalPriceZero();
        }

        var ingredientInserted = [],
            uomInserted = [];
        angular.forEach($scope.requisitionItems, function (value) {
            angular.forEach($scope.ingredients, function (ingredientValue) {
                if (ingredientValue.id == value.ingredient_id) {
                    ingredientInserted = ingredientValue;
                }
            });
            angular.forEach($scope.uoms, function (uomValue) {
                if (uomValue.id == value.uom_id) {
                    uomInserted = uomValue;
                }
            });
            if (value.requisition_id == $scope.requisition.id) {
                $scope.purchaseOrderItems.push({
                    ingredient: ingredientInserted,
                    quantity: value.quantity,
                    price: value.price,
                    uom: uomInserted,
                });
                $scope.totalPrice = $scope.totalPrice + parseInt(value.price);
            }
        });
    }
}]);

/**
 * Created by dmustt on 23/10/16.
 */

dmustt.controller('RecipeFormController', [ '$scope', function ($scope) {

    $scope.bahan = '';

    $scope.ingredients = [];

    $scope.uoms = [];

    $scope.jumlah = '';

    $scope.satuan = '';

    $scope.ingredientRecipes = [];

    $scope.init = function (ingredient, uom, ingredientRecipe) {
        if (ingredient === 'undefined' || uom === 'undefined') {
            return;
        }

        $scope.ingredients = ingredient;

        $scope.uoms = uom;

        var bahan_temp = [],
            uom_temp = [];

        bahan_temp.push(ingredient);
        uom_temp.push(uom);

        $scope.bahan = bahan_temp[0][0];
        $scope.satuan = uom_temp[0][0];

        if (ingredientRecipe !== '') {
            var ingredientInserted = [],
                uomInserted = [];
            angular.forEach(ingredientRecipe, function(value) {
                angular.forEach(ingredient, function(ingredientValue) {
                    if (ingredientValue.id == value.ingredient_id) {
                        ingredientInserted = ingredientValue;
                    }
                });
                angular.forEach(uom, function(uomValue) {
                    if (uomValue.id == value.uom_id) {
                        uomInserted = uomValue;
                    }
                });
                $scope.ingredientRecipes.push({
                    bahan : ingredientInserted,
                    jumlah : value.ingredient_recipe_qty,
                    satuan : uomInserted
                });
            });
        }
    }

    $scope.removeIngredientRecipe = function (index) {
        $scope.ingredientRecipes.splice(index, 1);
    };

    $scope.addIngredientRecipe = function () {
        $scope.ingredientRecipes.push({
            bahan : $scope.bahan,
            jumlah : $scope.jumlah,
            satuan : $scope.satuan
        });
    }
}]);

/**
 * Created by dmustt on 25/10/16.
 */

dmustt.controller('RequisitionFormController', [ '$scope', function ($scope) {

    $scope.ingredient = '';

    $scope.ingredients = [];

    $scope.uoms = [];

    $scope.quantity = '';

    $scope.price = '';

    $scope.uom = '';

    $scope.requisitionItems = [];

    $scope.init = function (ingredient, uom, requisitionItem) {
        if (ingredient === 'undefined' || uom === 'undefined') {
            return;
        }

        $scope.ingredients = ingredient;

        $scope.uoms = uom;

        var ingredientTemp = [],
            uomTemp = [];

        ingredientTemp.push(ingredient);
        uomTemp.push(uom);

        $scope.ingredient = ingredientTemp[0][0];
        $scope.uom = uomTemp[0][0];

        if (requisitionItem !== '') {
            var ingredientInserted = [],
                uomInserted = [];
            angular.forEach(requisitionItem, function(value) {
                angular.forEach(ingredient, function(ingredientValue) {
                    if (ingredientValue.id == value.ingredient_id) {
                        ingredientInserted = ingredientValue;
                    }
                });
                angular.forEach(uom, function(uomValue) {
                    if (uomValue.id == value.uom_id) {
                        uomInserted = uomValue;
                    }
                });
                $scope.requisitionItems.push({
                    ingredient : ingredientInserted,
                    quantity : value.quantity,
                    price : value.price,
                    uom : uomInserted
                });
            });
        }
    }

    $scope.removeRequisitionItem = function (index) {
        $scope.requisitionItems.splice(index, 1);
    };

    $scope.addRequisitionItem = function () {
        $scope.requisitionItems.push({
            ingredient : $scope.ingredient,
            quantity : $scope.quantity,
            price : $scope.price,
            uom : $scope.uom
        });
    }
}]);

/**
 * Created by dmustt on 24/10/16.
 */

dmustt.controller('RouteFormController', [ '$scope', function ($scope) {

    $scope.port = '';

    $scope.ports = [];

    $scope.estimasi = '';

    $scope.routeStops = [];

    $scope.init = function (port, routeStop) {
        if (port === 'undefined') {
            return;
        }

        $scope.ports = port;

        var routeStop_temp = [];

        routeStop_temp.push(port);

        $scope.port = routeStop_temp[0][0];

        if (routeStop !== '') {
            var portInserted = [];
            angular.forEach(routeStop, function(value) {
                angular.forEach(port, function(portValue) {
                    if (portValue.id == value.port_id) {
                        portInserted = portValue;
                    }
                });
                $scope.routeStops.push({
                    port : portInserted,
                    estimasi : value.eta,
                });
            });
        }
    }

    $scope.removeRouteStop = function (index) {
        $scope.routeStops.splice(index, 1);
    };

    $scope.addRouteStop = function () {
        $scope.routeStops.push({
            port : $scope.port,
            estimasi : $scope.estimasi,
        });
    }
}]);

/**
 * Created by dmustt on 22/10/16.
 */

"use strict";

dmustt.factory('Ingredients', ['$rootScope', '$window', function ($rootScope, $window) {
    var Ingredients = function () {
    };

    return (Ingredients);
}]);

//# sourceMappingURL=dmustt.js.map
