<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
    | by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//route's route
Route::group(['prefix' => 'route'], function () {
    //get route's route
    Route::get('/', ['uses' => 'Route\routeController@getIndex', 'as' => 'route']);

    //get route's create
    Route::get('create', ['uses' => 'Route\routeController@getCreate', 'as' => 'route.create']);

    //post route's create
    Route::post('create', ['uses' => 'Route\routeController@postCreate', 'as' => 'route.create']);

    //get route's update
    Route::get('update/{id}', ['uses' => 'Route\routeController@getUpdate', 'as' => 'route.update']);

    //put route's route
    Route::put('update/{id}', ['uses' => 'Route\routeController@postUpdate', 'as' => 'route.update']);

    //delete route's route
    Route::get('delete/{id}', ['uses' => 'Route\routeController@getDelete', 'as' => 'route.delete']);


});

//cruise's route
Route::group(['prefix' => 'cruise'], function() {

    //get cruise's route
    Route::get('/', ['uses' => 'Cruise\cruiseController@getIndex', 'as' => 'cruise']);

    //get cruise's route
    Route::get('create', ['uses' => 'Cruise\cruiseController@getCreate', 'as' => 'cruise.create']);

    //post cruise's route
    Route::post('create', ['uses' => 'Cruise\cruiseController@postCreate', 'as' => 'cruise.create']);

    //get cruise's route
    Route::get('update/{id}', ['uses' => 'Cruise\cruiseController@getUpdate', 'as' => 'cruise.update']);

    //put cruise's route
    Route::put('update/{id}', ['uses' => 'Cruise\cruiseController@postUpdate', 'as' => 'cruise.update']);

    //delete cruise's route
    Route::get('delete/{id}', ['uses' => 'Cruise\cruiseController@getDelete', 'as' => 'cruise.delete']);

});

//user's route
Route::group(['prefix' => 'user'], function() {

    //get user's route
    Route::get('/', ['uses' => 'User\userController@getIndex', 'as' => 'user']);

    //get user's route
    Route::get('create', ['uses' => 'User\userController@getCreate', 'as' => 'user.create']);

    //post user's route
    Route::post('create', ['uses' => 'User\userController@postCreate', 'as' => 'user.create']);

    //get user's route
    Route::get('update/{id}', ['uses' => 'User\userController@getUpdate', 'as' => 'user.update']);

    //put user's route
    Route::put('update/{id}', ['uses' => 'User\userController@postUpdate', 'as' => 'user.update']);

    //delete user's route
    Route::get('delete/{id}', ['uses' => 'User\userController@getDelete', 'as' => 'user.delete']);

    //status user's route
    Route::get('status/{status}/{id}', ['uses' => 'User\userController@updateStatus', 'as' => 'user.status']);

});

//ingredient's route
Route::group(['prefix' => 'ingredient'], function() {

    //get ingredient's route
    Route::get('/', ['uses' => 'Ingredient\ingredientController@getIndex', 'as' => 'ingredient']);

    //get ingredient's route
    Route::get('create', ['uses' => 'Ingredient\ingredientController@getCreate', 'as' => 'ingredient.create']);

    //post ingredient's route
    Route::post('create', ['uses' => 'Ingredient\ingredientController@postCreate', 'as' => 'ingredient.create']);

    //get ingredient's route
    Route::get('update/{id}', ['uses' => 'Ingredient\ingredientController@getUpdate', 'as' => 'ingredient.update']);

    //put ingredient's route
    Route::put('update/{id}', ['uses' => 'Ingredient\ingredientController@postUpdate', 'as' => 'ingredient.update']);

    //delete ingredient's route
    Route::get('delete/{id}', ['uses' => 'Ingredient\ingredientController@getDelete', 'as' => 'ingredient.delete']);

});

//recipe's route
Route::group(['prefix' => 'recipe'], function() {

    //get recipe's route
    Route::get('/', ['uses' => 'Recipe\recipeController@getIndex', 'as' => 'recipe']);

    //get recipe's route
    Route::get('create', ['uses' => 'Recipe\recipeController@getCreate', 'as' => 'recipe.create']);

    //post recipe's route
    Route::post('create', ['uses' => 'Recipe\recipeController@postCreate', 'as' => 'recipe.create']);

    //get recipe's route
    Route::get('update/{id}', ['uses' => 'Recipe\recipeController@getUpdate', 'as' => 'recipe.update']);

    //put recipe's route
    Route::put('update/{id}', ['uses' => 'Recipe\recipeController@postUpdate', 'as' => 'recipe.update']);

    //delete recipe's route
    Route::get('delete/{id}', ['uses' => 'Recipe\recipeController@getDelete', 'as' => 'recipe.delete']);

});

//menu's route
Route::group(['prefix' => 'menu'], function() {

    //get menu's route
    Route::get('/', ['uses' => 'Menu\menuController@getIndex', 'as' => 'menu']);

    //get menu's route
    Route::get('create', ['uses' => 'Menu\menuController@getCreate', 'as' => 'menu.create']);

    //post menu's route
    Route::post('create', ['uses' => 'Menu\menuController@postCreate', 'as' => 'menu.create']);

    //get menu's route
    Route::get('update/{id}', ['uses' => 'Menu\menuController@getUpdate', 'as' => 'menu.update']);

    //put menu's route
    Route::put('update/{id}', ['uses' => 'Menu\menuController@postUpdate', 'as' => 'menu.update']);

    //delete menu's route
    Route::get('delete/{id}', ['uses' => 'Menu\menuController@getDelete', 'as' => 'menu.delete']);

});

//vendor's route
Route::group(['prefix' => 'vendor'], function() {

    //get vendor's route
    Route::get('/', ['uses' => 'Vendor\vendorController@getIndex', 'as' => 'vendor']);

    //get vendor's route
    Route::get('create', ['uses' => 'Vendor\vendorController@getCreate', 'as' => 'vendor.create']);

    //post vendor's route
    Route::post('create', ['uses' => 'Vendor\vendorController@postCreate', 'as' => 'vendor.create']);

    //get vendor's route
    Route::get('update/{id}', ['uses' => 'Vendor\vendorController@getUpdate', 'as' => 'vendor.update']);

    //put vendor's route
    Route::put('update/{id}', ['uses' => 'Vendor\vendorController@postUpdate', 'as' => 'vendor.update']);

    //delete vendor's route
    Route::get('delete/{id}', ['uses' => 'Vendor\vendorController@getDelete', 'as' => 'vendor.delete']);

});

//voyage class's route
Route::group(['prefix' => 'voyage-class'], function() {

    //get voyage class's route
    Route::get('/', ['uses' => 'VoyageClass\voyageClassController@getIndex', 'as' => 'voyage-class']);

    //get voyage class's route
    Route::get('create', ['uses' => 'VoyageClass\voyageClassController@getCreate', 'as' => 'voyage-class.create']);

    //post voyage class's route
    Route::post('create', ['uses' => 'VoyageClass\voyageClassController@postCreate', 'as' => 'voyage-class.create']);

    //get voyage class's route
    Route::get('update/{id}', ['uses' => 'VoyageClass\voyageClassController@getUpdate', 'as' => 'voyage-class.update']);

    //put voyage class's route
    Route::put('update/{id}', ['uses' => 'VoyageClass\voyageClassController@postUpdate', 'as' => 'voyage-class.update']);

    //delete voyage class's route
    Route::get('delete/{id}', ['uses' => 'VoyageClass\voyageClassController@getDelete', 'as' => 'voyage-class.delete']);

});

//voyage planning's route
Route::group(['prefix' => 'voyage-planning'], function() {

    //get voyage planning's route
    Route::get('/', ['uses' => 'VoyagePlanning\voyagePlanningController@getIndex', 'as' => 'voyage-planning']);

    //get voyage planning's route
    Route::get('create', ['uses' => 'VoyagePlanning\voyagePlanningController@getCreate', 'as' => 'voyage-planning.create']);

    //post voyage planning's route
    Route::post('create', ['uses' => 'VoyagePlanning\voyagePlanningController@postCreate', 'as' => 'voyage-planning.create']);

    //get voyage planning's route
    Route::get('update/{id}', ['uses' => 'VoyagePlanning\voyagePlanningController@getUpdate', 'as' => 'voyage-planning.update']);

    //put voyage planning's route
    Route::put('update/{id}', ['uses' => 'VoyagePlanning\voyagePlanningController@postUpdate', 'as' => 'voyage-planning.update']);

    //delete voyage planning's route
    Route::get('delete/{id}', ['uses' => 'VoyagePlanning\voyagePlanningController@getDelete', 'as' => 'voyage-planning.delete']);

});

//meal planning's route
Route::group(['prefix' => 'meal-planning'], function() {

    //get meal planning's route
    Route::get('/', ['uses' => 'MealPlanning\mealPlanningController@getIndex', 'as' => 'meal-planning']);

    //get meal planning's route
    Route::get('create', ['uses' => 'MealPlanning\mealPlanningController@getCreate', 'as' => 'meal-planning.create']);

    //post meal planning's route
    Route::post('create', ['uses' => 'MealPlanning\mealPlanningController@postCreate', 'as' => 'meal-planning.create']);

    //get meal planning's route
    Route::get('update/{id}', ['uses' => 'MealPlanning\mealPlanningController@getUpdate', 'as' => 'meal-planning.update']);

    //put meal planning's route
    Route::put('update/{id}', ['uses' => 'MealPlanning\mealPlanningController@postUpdate', 'as' => 'meal-planning.update']);

    //delete meal planning's route
    Route::get('delete/{id}', ['uses' => 'MealPlanning\mealPlanningController@getDelete', 'as' => 'meal-planning.delete']);

});

//requisition's route
Route::group(['prefix' => 'requisition'], function() {

    //get requisition's route
    Route::get('/', ['uses' => 'Requisition\RequisitionController@getIndex', 'as' => 'requisition']);

    //get requisitionm's route
    Route::get('create', ['uses' => 'Requisition\RequisitionController@getCreate', 'as' => 'requisition.create']);

    //post requisition's route
    Route::post('create', ['uses' => 'Requisition\RequisitionController@postCreate', 'as' => 'requisition.create']);

    //get requisition's route
    Route::get('update/{id}', ['uses' => 'Requisition\RequisitionController@getUpdate', 'as' => 'requisition.update']);

    //put requisition's route
    Route::put('update/{id}', ['uses' => 'Requisition\RequisitionController@postUpdate', 'as' => 'requisition.update']);

    //delete requisition's route
    Route::get('delete/{id}', ['uses' => 'Requisition\RequisitionController@getDelete', 'as' => 'requisition.delete']);

    //status of requisition's route
    Route::get('status/{id}/{status}', ['uses' => 'Requisition\RequisitionController@getStatus', 'as' => 'requisition.status']);

});

//requisition item's route
Route::group(['prefix' => 'requisition-item'], function() {

    //get requisition item's route
    Route::get('/', ['uses' => 'RequisitionItem\RequisitionItemController@getIndex', 'as' => 'requisition-item']);

    //get requisition item's route
    Route::get('create', ['uses' => 'RequisitionItem\RequisitionItemController@getCreate', 'as' => 'requisition-item.create']);

    //post requisition item's route
    Route::post('create', ['uses' => 'RequisitionItem\RequisitionItemController@postCreate', 'as' => 'requisition-item.create']);

    //get requisition item's route
    Route::get('update/{id}', ['uses' => 'RequisitionItem\RequisitionItemController@getUpdate', 'as' => 'requisition-item.update']);

    //put requisition item's route
    Route::put('update/{id}', ['uses' => 'RequisitionItem\RequisitionItemController@postUpdate', 'as' => 'requisition-item.update']);

    //delete requisition item's route
    Route::get('delete/{id}', ['uses' => 'RequisitionItem\RequisitionItemController@getDelete', 'as' => 'requisition-item.delete']);

});

//purchase order's route
Route::group(['prefix' => 'purchase-order'], function() {

    //get purchase order's route
    Route::get('/', ['uses' => 'PurchaseOrder\PurchaseOrderController@getIndex', 'as' => 'purchase-order']);

    //get purchase order's route
    Route::get('create', ['uses' => 'PurchaseOrder\PurchaseOrderController@getCreate', 'as' => 'purchase-order.create']);

    //post purchase order's route
    Route::post('create', ['uses' => 'PurchaseOrder\PurchaseOrderController@postCreate', 'as' => 'purchase-order.create']);

    //get purchase order's route
    Route::get('update/{id}', ['uses' => 'PurchaseOrder\PurchaseOrderController@getUpdate', 'as' => 'purchase-order.update']);

    //put purchase order's route
    Route::put('update/{id}', ['uses' => 'PurchaseOrder\PurchaseOrderController@postUpdate', 'as' => 'purchase-order.update']);

    //delete purchase order's route
    Route::get('delete/{id}', ['uses' => 'PurchaseOrder\PurchaseOrderController@getDelete', 'as' => 'purchase-order.delete']);

    //print purchase order's route
    Route::get('print/{id}', ['uses' => 'PurchaseOrder\PurchaseOrderController@getPrint', 'as' => 'purchase-order.print']);
});

//purchase order item's route
Route::group(['prefix' => 'purchase-order-item'], function() {

    //get purchase order item's route
    Route::get('/', ['uses' => 'PurchaseOrderItem\PurchaseOrderItemController@getIndex', 'as' => 'purchase-order-item']);

    //get purchase order item's route
    Route::get('create', ['uses' => 'PurchaseOrderItem\PurchaseOrderItemController@getCreate', 'as' => 'purchase-order-item.create']);

    //post purchase order item's route
    Route::post('create', ['uses' => 'PurchaseOrderItem\PurchaseOrderItemController@postCreate', 'as' => 'purchase-order-item.create']);

    //get purchase order item's route
    Route::get('update/{id}', ['uses' => 'PurchaseOrderItem\PurchaseOrderItemController@getUpdate', 'as' => 'purchase-order-item.update']);

    //put purchase order item's route
    Route::put('update/{id}', ['uses' => 'PurchaseOrderItem\PurchaseOrderItemController@postUpdate', 'as' => 'purchase-order-item.update']);

    //delete purchase order item's route
    Route::get('delete/{id}', ['uses' => 'PurchaseOrderItem\PurchaseOrderItemController@getDelete', 'as' => 'purchase-order-item.delete']);

});

//invoice's route
Route::group(['prefix' => 'invoice'], function() {

    //get invoice's route
    Route::get('/', ['uses' => 'Invoice\InvoiceController@getIndex', 'as' => 'invoice']);

    //get invoice's route
    Route::get('create', ['uses' => 'Invoice\InvoiceController@getCreate', 'as' => 'invoice.create']);

    //post invoice's route
    Route::post('create', ['uses' => 'Invoice\InvoiceController@postCreate', 'as' => 'invoice.create']);

    //get invoice's route
    Route::get('update/{id}', ['uses' => 'Invoice\InvoiceController@getUpdate', 'as' => 'invoice.update']);

    //put invoice's route
    Route::put('update/{id}', ['uses' => 'Invoice\InvoiceController@postUpdate', 'as' => 'invoice.update']);

    //delete invoice's route
    Route::get('delete/{id}', ['uses' => 'Invoice\InvoiceController@getDelete', 'as' => 'invoice.delete']);

});

//inventory's route
Route::group(['prefix' => 'inventory'], function() {

    //get inventory's route
    Route::get('/', ['uses' => 'Inventory\InventoryController@getIndex', 'as' => 'inventory']);

    //get inventory's route
    Route::get('create', ['uses' => 'Inventory\InventoryController@getCreate', 'as' => 'inventory.create']);

    //post inventory's route
    Route::post('create', ['uses' => 'Inventory\InventoryController@postCreate', 'as' => 'inventory.create']);

    //get inventory's route
    Route::get('update/{id}', ['uses' => 'Inventory\InventoryController@getUpdate', 'as' => 'inventory.update']);

    //put inventory's route
    Route::put('update/{id}', ['uses' => 'Inventory\InventoryController@postUpdate', 'as' => 'inventory.update']);

    //delete inventory's route
    Route::get('delete/{id}', ['uses' => 'Inventory\InventoryController@getDelete', 'as' => 'inventory.delete']);

});

//inventory out's route
Route::group(['prefix' => 'inventory-out'], function() {

    //get inventory out's route
    Route::get('/', ['uses' => 'InventoryOut\InventoryOutController@getIndex', 'as' => 'inventory-out']);

    //get inventory out's route
    Route::get('create', ['uses' => 'InventoryOut\InventoryOutController@getCreate', 'as' => 'inventory-out.create']);

    //post inventory out's route
    Route::post('create', ['uses' => 'InventoryOut\InventoryOutController@postCreate', 'as' => 'inventory-out.create']);

    //get inventory out's route
    Route::get('update/{id}', ['uses' => 'InventoryOut\InventoryOutController@getUpdate', 'as' => 'inventory-out.update']);

    //put inventory out's route
    Route::put('update/{id}', ['uses' => 'InventoryOut\InventoryOutController@postUpdate', 'as' => 'inventory-out.update']);

    //delete inventory out's route
    Route::get('delete/{id}', ['uses' => 'InventoryOut\InventoryOutController@getDelete', 'as' => 'inventory-out.delete']);

});

//waste's route
Route::group(['prefix' => 'waste'], function() {

    //get waste's route
    Route::get('/', ['uses' => 'Waste\WasteController@getIndex', 'as' => 'waste']);

    //get waste's route
    Route::get('create', ['uses' => 'Waste\WasteController@getCreate', 'as' => 'waste.create']);

    //post waste's route
    Route::post('create', ['uses' => 'Waste\WasteController@postCreate', 'as' => 'waste.create']);

    //get waste's route
    Route::get('update/{id}', ['uses' => 'Waste\WasteController@getUpdate', 'as' => 'waste.update']);

    //put waste's route
    Route::put('update/{id}', ['uses' => 'Waste\WasteController@postUpdate', 'as' => 'waste.update']);

    //delete waste's route
    Route::get('delete/{id}', ['uses' => 'Waste\WasteController@getDelete', 'as' => 'waste.delete']);

});

//waste per meal's route
Route::group(['prefix' => 'waste-per-meal'], function() {

    //get waste per meal's route
    Route::get('/', ['uses' => 'WastePerMeal\WastePerMealController@getIndex', 'as' => 'waste-per-meal']);

    //get waste per meal's route
    Route::get('create', ['uses' => 'WastePerMeal\WastePerMealController@getCreate', 'as' => 'waste-per-meal.create']);

    //post waste per meal's route
    Route::post('create', ['uses' => 'WastePerMeal\WastePerMealController@postCreate', 'as' => 'waste-per-meal.create']);

    //get waste per meal's route
    Route::get('update/{id}', ['uses' => 'WastePerMeal\WastePerMealController@getUpdate', 'as' => 'waste-per-meal.update']);

    //put waste per meal's route
    Route::put('update/{id}', ['uses' => 'WastePerMeal\WastePerMealController@postUpdate', 'as' => 'waste-per-meal.update']);

    //delete waste per meal's route
    Route::get('delete/{id}', ['uses' => 'WastePerMeal\WastePerMealController@getDelete', 'as' => 'waste-per-meal.delete']);

});

//employee's route
Route::group(['prefix' => 'employee'], function() {

    //get employee's route
    Route::get('/', ['uses' => 'Employee\EmployeeController@getIndex', 'as' => 'employee']);

    //get employee's route
    Route::get('create', ['uses' => 'Employee\EmployeeController@getCreate', 'as' => 'employee.create']);

    //post employee's route
    Route::post('create', ['uses' => 'Employee\EmployeeController@postCreate', 'as' => 'employee.create']);

    //get employee's route
    Route::get('update/{id}', ['uses' => 'Employee\EmployeeController@getUpdate', 'as' => 'employee.update']);

    //put employee's route
    Route::put('update/{id}', ['uses' => 'Employee\EmployeeController@postUpdate', 'as' => 'employee.update']);

    //delete employee's route
    Route::get('delete/{id}', ['uses' => 'Employee\EmployeeController@getDelete', 'as' => 'employee.delete']);

});

//role's route
Route::group(['prefix' => 'role'], function() {

    //get role's route
    Route::get('/', ['uses' => 'Role\RoleController@getIndex', 'as' => 'role']);

    //get role's route
    Route::get('create', ['uses' => 'Role\RoleController@getCreate', 'as' => 'role.create']);

    //post role's route
    Route::post('create', ['uses' => 'Role\RoleController@postCreate', 'as' => 'role.create']);

    //get role's route
    Route::get('update/{id}', ['uses' => 'Role\RoleController@getUpdate', 'as' => 'role.update']);

    //put role's route
    Route::put('update/{id}', ['uses' => 'Role\RoleController@postUpdate', 'as' => 'role.update']);

    //delete role's route
    Route::get('delete/{id}', ['uses' => 'Role\RoleController@getDelete', 'as' => 'role.delete']);

});

//uom's route
Route::group(['prefix' => 'uom'], function() {

    //get uom's route
    Route::get('/', ['uses' => 'Uom\UomController@getIndex', 'as' => 'uom']);

    //get uom's route
    Route::get('create', ['uses' => 'Uom\UomController@getCreate', 'as' => 'uom.create']);

    //post uom's route
    Route::post('create', ['uses' => 'Uom\UomController@postCreate', 'as' => 'uom.create']);

    //get uom's route
    Route::get('update/{id}', ['uses' => 'Uom\UomController@getUpdate', 'as' => 'uom.update']);

    //put uom's route
    Route::put('update/{id}', ['uses' => 'Uom\UomController@postUpdate', 'as' => 'uom.update']);

    //delete uom's route
    Route::get('delete/{id}', ['uses' => 'Uom\UomController@getDelete', 'as' => 'uom.delete']);

});

//port's route
Route::group(['prefix' => 'port'], function() {

    //get port's route
    Route::get('/', ['uses' => 'Port\PortController@getIndex', 'as' => 'port']);

    //get port's route
    Route::get('create', ['uses' => 'Port\PortController@getCreate', 'as' => 'port.create']);

    //post port's route
    Route::post('create', ['uses' => 'Port\PortController@postCreate', 'as' => 'port.create']);

    //get port's route
    Route::get('update/{id}', ['uses' => 'Port\PortController@getUpdate', 'as' => 'port.update']);

    //put port's route
    Route::put('update/{id}', ['uses' => 'Port\PortController@postUpdate', 'as' => 'port.update']);

    //delete port's route
    Route::get('delete/{id}', ['uses' => 'Port\PortController@getDelete', 'as' => 'port.delete']);

});